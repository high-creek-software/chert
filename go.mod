module gitlab.com/high-creek-software/chert

go 1.21.0

toolchain go1.22.1

require (
	fyne.io/fyne/v2 v2.4.4
	github.com/BurntSushi/toml v1.3.2
	github.com/abhinav/goldmark-wikilink v0.3.0
	github.com/alecthomas/chroma v0.10.0
	github.com/atotto/clipboard v0.1.4
	github.com/blevesearch/bleve/v2 v2.3.2
	github.com/client9/gospell v0.0.0-20160306015952-90dfc71015df
	github.com/diamondburned/gotk4-sourceview/pkg v0.0.0-20240312005410-8276faa7949c
	github.com/diamondburned/gotk4/pkg v0.2.2
	github.com/gammazero/deque v0.2.1
	github.com/high-creek-software/bento v0.1.2
	github.com/high-creek-software/tabman v0.1.2
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/stephenafamo/goldmark-pdf v0.1.0
	github.com/yuin/goldmark v1.5.6
	github.com/yuin/goldmark-highlighting v0.0.0-20220208100518-594be1970594
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9
	golang.org/x/image v0.12.0
	gopkg.in/errgo.v2 v2.1.0
	mvdan.cc/xurls/v2 v2.4.0
)

require (
	fyne.io/systray v1.10.1-0.20231115130155-104f5ef7839e // indirect
	github.com/KarpelesLab/weak v0.1.1 // indirect
	github.com/ReneKroon/ttlcache/v2 v2.11.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fredbi/uri v1.0.0 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/fyne-io/gl-js v0.0.0-20230506162202-1fdaa286a934 // indirect
	github.com/fyne-io/glfw-js v0.0.0-20220120001248-ee7290d23504 // indirect
	github.com/fyne-io/image v0.0.0-20220602074514-4956b0afb3d2 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/go-swiss/fonts v0.0.0-20220802120607-8a942eccc739 // indirect
	github.com/go-text/render v0.0.0-20230619120952-35bccb6164b8 // indirect
	github.com/go-text/typesetting v0.1.0 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jsummers/gobmp v0.0.0-20151104160322-e2ba15ffa76e // indirect
	github.com/phpdave11/gofpdf v1.4.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/srwiley/oksvg v0.0.0-20221011165216-be6e8873101c // indirect
	github.com/srwiley/rasterx v0.0.0-20220730225603-2ab79fcdd4ef // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	github.com/tevino/abool v1.2.0 // indirect
	golang.org/x/mobile v0.0.0-20230531173138-3c911d8e3eda // indirect
	golang.org/x/text v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	honnef.co/go/js/dom v0.0.0-20210725211120-f030747120f2 // indirect
)

require (
	github.com/RoaringBitmap/roaring v0.9.4 // indirect
	github.com/bits-and-blooms/bitset v1.2.2 // indirect
	github.com/blevesearch/bleve_index_api v1.0.1 // indirect
	github.com/blevesearch/go-porterstemmer v1.0.3 // indirect
	github.com/blevesearch/gtreap v0.1.1 // indirect
	github.com/blevesearch/mmap-go v1.0.3 // indirect
	github.com/blevesearch/scorch_segment_api/v2 v2.1.0 // indirect
	github.com/blevesearch/segment v0.9.0 // indirect
	github.com/blevesearch/snowballstem v0.9.0 // indirect
	github.com/blevesearch/upsidedown_store_api v1.0.1 // indirect
	github.com/blevesearch/vellum v1.0.7 // indirect
	github.com/blevesearch/zapx/v11 v11.3.3 // indirect
	github.com/blevesearch/zapx/v12 v12.3.3 // indirect
	github.com/blevesearch/zapx/v13 v13.3.3 // indirect
	github.com/blevesearch/zapx/v14 v14.3.3 // indirect
	github.com/blevesearch/zapx/v15 v15.3.3 // indirect
	//github.com/diamondburned/gotk4-adwaita/pkg v0.0.0-20220415035637-463213580048 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/mschoch/smat v0.2.0 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20231121144256-b99613f794b6 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)

// replace github.com/diamondburned/gotk4-sourceview/pkg => ../gotk4-sourceview/pkg
