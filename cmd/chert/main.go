package main

import (
	"flag"
	"github.com/diamondburned/gotk4/pkg/gio/v2"
	gotk "github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/gtk"
)

var debug bool

func main() {
	flag.BoolVar(&debug, "v", false, "Sets the debug flag.")
	flag.Parse()

	app := gotk.NewApplication("io.highcreeksoftware.chert", gio.ApplicationNonUnique)
	// app := adw.NewApplication("io.highcreeksoftware.chert", gio.ApplicationNonUnique)
	app.ConnectActivate(func() {
		chertApplication := gtk.NewChertApplication(app, debug)

		chertApplication.Window.Show()
	})

	app.Run(nil)
}
