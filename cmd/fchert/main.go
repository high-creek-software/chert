package main

import "gitlab.com/high-creek-software/chert/internal/fne"

func main() {
	cApp := fne.NewChertApplication()
	cApp.Start()
}
