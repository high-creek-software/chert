# Chert

A basic note taking app for gtk4, with tagging and linking.

## Setup
Because I'm using gtk4, it requires gtksourceview5 for a sourceview.  However, I'm not finding it packaged on my distro, arch.

So I've needed to build it, and then associate it properly to get the builds and run to work.

Find it here: https://github.com/GNOME/gtksourceview

Then I needed to set up the paths correctly to run:
- PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
- LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
- ASSUME_NO_MOVING_GC_UNSAFE_RISK_IT_WITH=go1.18