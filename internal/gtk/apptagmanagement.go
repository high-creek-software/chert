package gtk

import (
	"errors"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
)

func (ca *ChertApplication) TagSelected(tagName string, tagSource core.TagSource) {
	tag := ca.registry.ChertData.Tags[tagName]
	ca.pageView.UpdateColumn(tag.Name, tag.Emoji)
	ca.pageView.ClearPages()
	if pages, err := ca.registry.PagesForTag(tagName); err == nil && pages != nil {
		for _, page := range pages {
			page.Tags, _ = ca.registry.FindPageTags(page)
			ca.pageView.AddPage(page)
		}
	}

	ca.notebookView.UnselectAll()
	switch tagSource {
	case core.TagTree:
		for _, ec := range ca.openPages {
			ec.UnselectAllTags()
		}
	case core.TagPage:
		ca.tagsView.UnselectAll()
	}
}

func (ca *ChertApplication) RequestTagEmoji(tagName string, callback func(text string)) {
	x, y, _ := ca.tagsView.TranslateCoordinates(ca.Window, 0, 0)
	rect := gdk.NewRectangle(int(x+100), int(y+35), 1, 1)

	ec := gtk.NewEmojiChooser()
	ec.ConnectEmojiPicked(func(text string) {
		ca.registry.SetTagEmoji(tagName, text)
		callback(text)
	})
	ec.SetParent(ca.tagsView)
	ec.SetPointingTo(&rect)
	ec.Show()
}

func (ca *ChertApplication) RequestDeleteTag(tagName string) error {
	ca.ShowError("Not yet implemented", errors.New("Can not yet delete"))
	return nil
}
