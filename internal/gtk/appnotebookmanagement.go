package gtk

import (
	"fmt"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
)

func (ca *ChertApplication) NotebookSelected(notebook *core.Notebook) {
	ca.pageView.ClearPages()
	ca.selectedNotebook = notebook
	ca.pageView.UpdateColumn(ca.selectedNotebook.Name, ca.selectedNotebook.Emoji)
	if notebook == nil || notebook.Pages == nil {
		return
	}
	for _, page := range notebook.Pages {
		func(pg *core.Page) {
			pg.Tags, _ = ca.registry.FindPageTags(pg)
			ca.pageView.AddPage(page)
		}(page)
	}

	ca.tagsView.UnselectAll()
	for _, ec := range ca.openPages {
		ec.UnselectAllTags()
	}
}

func (ca *ChertApplication) RequestAddChild(notebook *core.Notebook, childName string) error {
	_, err := ca.registry.AddChild(notebook, childName)
	if err != nil {
		ca.ShowError("Error creating notebook", err)
	}
	return err
}

func (ca *ChertApplication) RequestRename(notebook *core.Notebook, rename string) error {
	err := ca.registry.RenameNotebook(notebook, rename)
	if err != nil {
		ca.ShowError("Error renaming notebook", err)
	}
	return err
}

func (ca *ChertApplication) RequestCreatePage(notebook *core.Notebook, pageName string) error {
	pg, err := ca.registry.AddPage(notebook, pageName)

	if pg != nil && notebook.RelativePath == ca.selectedNotebook.RelativePath {
		ca.pageView.AddPage(pg)
	}

	if err != nil {
		ca.ShowError("Could not create new note", err)
	}

	return err
}

func (ca *ChertApplication) RequestDelete(notebook *core.Notebook, p *gtk.TreePath) error {
	dialog := gtk.NewMessageDialog(&ca.Window.Window, gtk.DialogModal, gtk.MessageWarning, gtk.ButtonsOKCancel)
	dialog.SetMarkup(fmt.Sprintf("Delete: %s", notebook.RelativePath))

	dialog.ConnectResponse(func(responseId int) {
		dialog.Destroy()
		if responseId == int(gtk.ResponseOK) {
			err := ca.registry.DeleteNotebook(notebook)
			if err != nil {
				ca.ShowError("Error deleting notebook", err)
			} else {
				ca.notebookView.FinalizeDelete(p)
			}
		}
	})

	dialog.Show()
	return nil
}

func (ca *ChertApplication) RequestPaste(notebook *core.Notebook) error {
	err := ca.registry.CutPastePage(ca.pendingCut.Page, ca.pendingCut.ParentNotebook, notebook)
	if err != nil {
		ca.ShowError("Error pasting page", err)
	} else {
		// Update the UI, by adding the page to the notebook?
		ca.pendingCut = nil
		ca.notebookView.SetPendingCut(false)
	}
	return err
}

func (ca *ChertApplication) RequestEmoji(relativePath string, callback func(text string)) {
	x, y, _ := ca.outsidePane.TranslateCoordinates(ca.Window, 0, 0)
	rect := gdk.NewRectangle(int(x+100), int(y+35), 1, 1)

	ec := gtk.NewEmojiChooser()
	ec.ConnectEmojiPicked(func(text string) {
		ca.registry.SetEmoji(relativePath, text)
		callback(text)
	})
	ec.SetParent(ca.notebookView.GetView())
	ec.SetPointingTo(&rect)
	ec.Show()
}
