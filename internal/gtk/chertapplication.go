package gtk

import (
	"bytes"
	_ "embed"
	"errors"
	"fmt"
	wikilink "github.com/abhinav/goldmark-wikilink"
	fmthtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/client9/gospell"
	"github.com/diamondburned/gotk4-sourceview/pkg/gtksource/v5"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"github.com/yuin/goldmark"
	highlighting "github.com/yuin/goldmark-highlighting"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/editorcomponents"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/notebook"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/page"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/search"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/tags"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"text/template"
)

//go:embed chert.css.tpl
var cssTemplate string

type ChertApplication struct {
	app         *gtk.Application
	Window      *gtk.ApplicationWindow
	registry    *core.Registry
	cssProvider *gtk.CSSProvider

	titleLbl    *gtk.Label
	subtitleLbl *gtk.Label

	headerBar    *gtk.HeaderBar
	recentButton *gtk.Button
	outsidePane  *gtk.Paned
	insidePane   *gtk.Paned
	notebookView notebook.NotebookList
	pageView     page.PageList
	tagsView     *tags.TagsTreeView
	tabs         *gtk.Notebook
	mainStack    *gtk.Stack
	welcomeBox   *gtk.Box
	mainBox      *gtk.Box
	searchBox    *gtk.Box

	fileChooser *gtk.FileChooserNative

	selectedNotebook *core.Notebook

	cssTemplate *template.Template

	config        *core.Config
	configManager core.ConfigManager

	schemeManager   *gtksource.StyleSchemeManager
	scheme          *gtksource.StyleScheme
	languageManager *gtksource.LanguageManager
	language        *gtksource.Language
	fontDesc        *pango.FontDescription

	pendingCut *core.PendingCut

	markdown goldmark.Markdown

	spelling *gospell.GoSpell

	openPages     map[string]*editorcomponents.EditorComponents
	searchResults map[string]*search.SearchResults

	logger *core.Logger

	port int
}

func NewChertApplication(app *gtk.Application, debug bool) *ChertApplication {
	chertApp := &ChertApplication{
		app:           app,
		openPages:     make(map[string]*editorcomponents.EditorComponents),
		searchResults: make(map[string]*search.SearchResults),
		logger:        core.NewLogger(debug),
	}
	chertApp.initialize()
	chertApp.port = chertApp.config.Port

	go func() {
		for {
			err := http.ListenAndServe(fmt.Sprintf("localhost:%d", chertApp.port), chertApp.registry)
			if strings.Contains(err.Error(), "bind: address already in use") {
				chertApp.port++
			} else {
				chertApp.logger.Error("Failed starting web server:", err)
				break
			}
		}
	}()

	return chertApp
}

func (ca *ChertApplication) initialize() {
	ca.configManager = core.NewConfigManager()
	var err error
	ca.config, err = ca.configManager.LoadConfig()
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		ca.logger.Fatal("Error loading config:", err)
	}

	ca.markdown = goldmark.New(
		goldmark.WithRendererOptions(
			html.WithUnsafe(),
		),
		goldmark.WithExtensions(
			extension.GFM,
			extension.NewFootnote(),
			&wikilink.Extender{Resolver: core.WikiResolver{}},
			highlighting.NewHighlighting(
				highlighting.WithStyle(ca.config.HighlightStyle),
				highlighting.WithGuessLanguage(true),
				highlighting.WithFormatOptions(
					fmthtml.WithLineNumbers(true),
				),
			),
		),
	)

	spelling, spellErr := gospell.NewGoSpell(ca.config.DictionaryBasePath+".aff", ca.config.DictionaryBasePath+".dic")
	if spellErr != nil {
		log.Println("error loading spelling", spellErr)
	} else {
		ca.spelling = spelling
	}

	ca.schemeManager = gtksource.NewStyleSchemeManager()
	if ca.config.Scheme != "" {
		ca.scheme = ca.schemeManager.Scheme(ca.config.Scheme)
	}
	ca.languageManager = gtksource.NewLanguageManager()
	ca.language = ca.languageManager.Language("markdown")

	ca.Window = gtk.NewApplicationWindow(ca.app)
	ca.Window.SetDefaultSize(1500, 900)
	//ca.Window.SetTitle("Chert")

	ca.setupActions()
	ca.setupHeader()
	ca.setupMainBox()

	ca.cssTemplate, err = template.New("chertcss").Parse(cssTemplate)
	if err != nil {
		ca.logger.Fatal("error parsing css template", err)
	}
	ca.cssProvider = gtk.NewCSSProvider()
	display := gdk.DisplayGetDefault()
	gtk.StyleContextAddProviderForDisplay(display, ca.cssProvider, gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

	ca.setCSS(ca.config.Font)

	if ca.config.Recent != nil && len(ca.config.Recent) > 0 {
		ca.openSelection(ca.config.Recent[0])
	}
}

func (ca *ChertApplication) setCSS(font string) {
	data := make(map[string]any)
	ca.fontDesc = pango.FontDescriptionFromString(font)
	if ca.fontDesc != nil {
		data["FontFamily"] = ca.fontDesc.Family()
		data["FontSize"] = ca.fontDesc.Size() / pango.SCALE
	}

	data["EmojiTitle"] = ca.config.EmojiSize

	var buf bytes.Buffer
	err := ca.cssTemplate.Execute(&buf, data)
	if err == nil {
		css := string(buf.Bytes())
		ca.cssProvider.LoadFromData(css)
	} else {
		ca.ShowError("Css template error", err)
	}

	if ca.fontDesc != nil {
		for _, ec := range ca.openPages {
			ec.SetTagFontSize(ca.fontDesc.Size())
		}
	}
}

func (ca *ChertApplication) fileSelected(respId int) {
	if respId != int(gtk.ResponseAccept) {
		return
	}

	selection := ca.fileChooser.File().Path()
	ca.openSelection(selection)
}

func (ca *ChertApplication) openSelection(path string) {
	// Run this on background, then callback here
	ca.registry = core.NewRegistry(path, core.WikilinkRegex, ca.markdown)
	ca.titleLbl.SetText(ca.registry.RootPath)
	ca.config.PrependRecent(path)
	ca.configManager.SaveConfig(ca.config)

	ca.notebookView.SetNotebook(ca.registry.Root)
	ca.tagsView.Clear()

	clear(ca.openPages)
	for ca.tabs.NPages() > 0 {
		ca.tabs.RemovePage(0)
	}

	if tags := ca.registry.GetTags(); tags != nil {
		for _, tag := range tags {
			ca.tagsView.AddTag(tag)
		}
	}
}

func (ca *ChertApplication) ShowError(title string, err error) {
	dialog := gtk.NewMessageDialog(&(ca.Window.Window), gtk.DialogDestroyWithParent|gtk.DialogModal, gtk.MessageError, gtk.ButtonsOK)
	dialog.SetMarkup(title)
	msg := gtk.NewLabel(err.Error())
	msg.SetName("error-sub")
	area := dialog.MessageArea()
	if box, ok := area.(*gtk.Box); ok {
		box.Append(msg)
	}

	dialog.ConnectResponse(func(responseId int) {
		dialog.Hide()
		dialog.Destroy()
	})
	dialog.Show()
}

func (ca *ChertApplication) EditInternalLink(linkRelativePath string) {

	page := ca.registry.GetPageByPath(linkRelativePath)

	ca.PageSelected(page)
}

func (ca *ChertApplication) OpenInternalLink(path string) {
	url := fmt.Sprintf("http://localhost:%d/%s", ca.port, path)
	ca.openLink(url)
}

func (ca *ChertApplication) OpenExternalLink(link string) {
	ca.openLink(link)
}

func (ca *ChertApplication) openLink(link string) {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default:
		cmd = "xdg-open"
	}
	args = append(args, link)
	err := exec.Command(cmd, args...).Start()
	if err != nil {
		ca.ShowError("Could not open browser", err)
	}
}
