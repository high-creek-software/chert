package gtk

import (
	"errors"
	"fmt"
	"github.com/diamondburned/gotk4-sourceview/pkg/gtksource/v5"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gdkpixbuf/v2"
	"github.com/diamondburned/gotk4/pkg/gio/v2"
	"github.com/diamondburned/gotk4/pkg/glib/v2"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/config"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/margin"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/notebook"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/page"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/search"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/tags"
)

func (ca *ChertApplication) setupActions() {
	// GVariantType https://docs.gtk.org/glib/struct.VariantType.html
	recentAction := gio.NewSimpleAction("recent-selected", glib.NewVariantType("i"))
	recentAction.ConnectActivate(func(params *glib.Variant) {
		idx := params.Int32()
		if idx == 0 {
			return
		}
		ca.openSelection(ca.config.Recent[idx])
	})
	ca.Window.AddAction(recentAction)
}

func (ca *ChertApplication) setupHeader() {
	ca.headerBar = gtk.NewHeaderBar()

	titleBox := gtk.NewBox(gtk.OrientationVertical, 0)
	titleBox.SetVAlign(gtk.AlignCenter)
	ca.titleLbl = gtk.NewLabel("Chert")
	ca.titleLbl.SetName("title")
	titleBox.Append(ca.titleLbl)
	ca.subtitleLbl = gtk.NewLabel("")
	ca.subtitleLbl.SetName("subtitle")
	titleBox.Append(ca.subtitleLbl)

	ca.headerBar.SetTitleWidget(titleBox)

	openButton := gtk.NewButtonFromIconName("document-open")
	ca.headerBar.PackStart(openButton)
	openButton.ConnectClicked(func() {
		ca.fileChooser = gtk.NewFileChooserNative("Open Directory", &(ca.Window.Window), gtk.FileChooserActionSelectFolder, "Open", "Cancel")
		ca.fileChooser.ConnectResponse(ca.fileSelected)
		ca.fileChooser.Show()
	})

	ca.recentButton = gtk.NewButtonFromIconName("document-open-recent")
	ca.headerBar.PackStart(ca.recentButton)
	ca.recentButton.ConnectClicked(ca.showRecentPopover)

	searchButton := gtk.NewButtonFromIconName("system-search")
	searchButton.ConnectClicked(func() {
		ca.searchBox.Show()
	})
	ca.headerBar.PackStart(searchButton)

	themeButton := gtksource.NewStyleSchemeChooserButton()
	if ca.scheme != nil {
		themeButton.SetStyleScheme(ca.scheme)
	}
	ca.headerBar.PackEnd(themeButton)
	themeButton.Connect("notify::style-scheme", func() {
		sel := themeButton.StyleScheme()
		if sel != nil {
			for _, ec := range ca.openPages {
				ec.SetStyleScheme(sel)
			}

			ca.scheme = sel
			ca.config.Scheme = sel.ID()
			ca.configManager.SaveConfig(ca.config)
		}
	})

	fontButton := gtk.NewFontButton()
	if ca.config.Font != "" {
		fontButton.SetFont(ca.config.Font)
	}
	ca.headerBar.PackEnd(fontButton)
	fontButton.ConnectFontSet(func() {
		ca.setCSS(fontButton.Font())
		ca.config.Font = fontButton.Font()
		ca.configManager.SaveConfig(ca.config)
	})

	settingsButton := gtk.NewButtonFromIconName("emblem-system")
	ca.headerBar.PackEnd(settingsButton)
	settingsButton.ConnectClicked(func() {
		dlg := gtk.NewDialogWithFlags("Chert Settings", &ca.Window.Window, gtk.DialogModal)
		configBox := config.NewConfigBox(ca.config, ca)

		dlg.SetChild(configBox.Box)
		dlg.SetSizeRequest(800, 200)
		dlg.Show()
	})

	ca.Window.SetTitlebar(ca.headerBar)
}

func (ca *ChertApplication) showRecentPopover() {
	menuModel := gio.NewMenu()
	for i, elem := range ca.config.Recent {
		// https://docs.gtk.org/gtk4/actions.html -> search: detailed actions
		// Not quite sure why yet that I have to add the win. prefix
		detailedAction := fmt.Sprintf("win.recent-selected(%d)", i)
		menuModel.Insert(i, elem, detailedAction)
	}

	popover := gtk.NewPopoverMenuFromModel(menuModel)
	popover.SetParent(ca.Window)
	popover.SetPosition(gtk.PosBottom)

	x, y, _ := ca.recentButton.TranslateCoordinates(ca.Window, 0, 0)
	rect := gdk.NewRectangle(int(x+10), int(y+35), 1, 1)
	popover.SetPointingTo(&rect)

	popover.Show()
}

func (ca *ChertApplication) setupMainBox() {
	ca.outsidePane = gtk.NewPaned(gtk.OrientationHorizontal)
	ca.insidePane = gtk.NewPaned(gtk.OrientationHorizontal)

	// Create first column notebook tree view & tags tree view
	notebookTagsPane := gtk.NewPaned(gtk.OrientationVertical)
	notebookTagsPane.SetSizeRequest(250, -1)

	ca.notebookView = notebook.NewNotebookTreeView(ca, ca.config)
	notebookTagsPane.SetStartChild(ca.notebookView.GetView())

	addTagBtn := ca.setupAddTagButton()
	ca.tagsView = tags.NewTagsTreeView(ca, ca.config)

	tagBox := gtk.NewBox(gtk.OrientationVertical, 0)
	tagBox.Append(addTagBtn)
	tagBox.Append(ca.tagsView.GetView())

	notebookTagsPane.SetEndChild(tagBox)

	// Create page tree view
	ca.pageView = page.NewPageTreeView(ca)

	ca.insidePane.SetStartChild(ca.pageView.GetView())
	ca.insidePane.SetResizeStartChild(false)
	ca.insidePane.SetShrinkStartChild(false)

	// Setting up the main window/editor stack
	ca.mainStack = gtk.NewStack()

	ca.welcomeBox = gtk.NewBox(gtk.OrientationVertical, 0)
	margin.SetMarginAll(ca.welcomeBox, 10)

	loader := gdkpixbuf.NewPixbufLoader()
	loader.SetSize(256, 256)
	loader.WriteBytes(glib.NewBytes(core.IconBytes))
	loader.Close()
	welcomeIcon := gtk.NewImageFromPixbuf(loader.Pixbuf())
	welcomeIcon.SetSizeRequest(256, 256)
	ca.welcomeBox.Append(welcomeIcon)
	introLbl := gtk.NewLabel("Welcome to chert")
	introLbl.SetName("welcome-lbl")
	ca.welcomeBox.Append(introLbl)
	ca.welcomeBox.SetVAlign(gtk.AlignCenter)
	ca.mainStack.AddChild(ca.welcomeBox)

	ca.tabs = gtk.NewNotebook()
	ca.tabs.SetScrollable(true)
	ca.tabs.ConnectSwitchPage(func(widget gtk.Widgetter, obj uint) {
		lbl, ok := ca.tabs.TabLabel(widget).(*gtk.Box)
		if ok {
			pathLbl, plOK := lbl.FirstChild().(*gtk.Label)
			if plOK {
				ca.subtitleLbl.SetText(pathLbl.Text())
			}
		}
	})
	ca.mainStack.AddChild(ca.tabs)

	ca.insidePane.SetEndChild(ca.mainStack)
	ca.insidePane.SetResizeEndChild(true)
	ca.insidePane.SetShrinkEndChild(true)

	ca.outsidePane.SetStartChild(notebookTagsPane)
	ca.outsidePane.SetResizeStartChild(false)
	ca.outsidePane.SetShrinkStartChild(false)

	ca.outsidePane.SetEndChild(ca.insidePane)
	ca.outsidePane.SetResizeEndChild(true)
	ca.outsidePane.SetShrinkStartChild(true)

	ca.mainBox = gtk.NewBox(gtk.OrientationVertical, 0)
	ca.searchBox = gtk.NewBox(gtk.OrientationHorizontal, 0)

	/*** Search Entry ***/
	searchEntry := gtk.NewSearchEntry()
	searchEntry.ConnectActivate(func() {
		ca.handleSearchEntry(searchEntry.Text())
	})
	margin.SetMarginAll(searchEntry, 10)
	ca.searchBox.Append(searchEntry)

	closeTrigger := gtk.NewShortcutTriggerParseString("Escape")
	closeAction := gtk.NewCallbackAction(func(widget gtk.Widgetter, args *glib.Variant) bool {
		searchEntry.SetText("")
		ca.searchBox.Hide()
		return true
	})
	closeShortcut := gtk.NewShortcut(closeTrigger, closeAction)
	closeController := gtk.NewShortcutController()
	closeController.AddShortcut(closeShortcut)
	searchEntry.AddController(closeController)

	findTrigger := gtk.NewShortcutTriggerParseString("<Control><Shift>f")
	findAction := gtk.NewCallbackAction(func(widget gtk.Widgetter, ars *glib.Variant) bool {
		ca.searchBox.Show()
		searchEntry.GrabFocus()
		return true
	})
	findShortcut := gtk.NewShortcut(findTrigger, findAction)
	findController := gtk.NewShortcutController()
	findController.AddShortcut(findShortcut)
	ca.Window.AddController(findController)

	hideBtn := gtk.NewButtonWithLabel("Hide")
	hideBtn.ConnectClicked(func() {
		ca.searchBox.Hide()
	})
	hideBtn.SetVExpand(false)
	hideBtn.SetVAlign(gtk.AlignCenter)
	ca.searchBox.Append(hideBtn)
	ca.searchBox.Hide()

	ca.mainBox.Append(ca.searchBox)
	ca.mainBox.Append(ca.outsidePane)

	ca.Window.SetChild(ca.mainBox)
}

func (ca *ChertApplication) setupAddTagButton() *gtk.Button {
	addTagBtn := gtk.NewButtonWithLabel("Add Tag")
	addTagBtn.ConnectClicked(func() {
		popover := gtk.NewPopover()
		popover.SetParent(addTagBtn)

		newTagBox := gtk.NewBox(gtk.OrientationVertical, 10)

		nameEntry := gtk.NewEntry()
		nameEntry.SetPlaceholderText("Tag name")
		newTagBox.Append(nameEntry)

		addBtn := gtk.NewButtonWithLabel("Add Tag")
		addBtn.ConnectClicked(func() {
			popover.Hide()

			tag, err := ca.registry.NewTag(nameEntry.Text())
			if err != nil {
				ca.ShowError("Error creating tag", err)
			} else {
				ca.tagsView.AddTag(tag)

				for _, ec := range ca.openPages {
					ec.AddTag(tag)
				}
			}
		})
		newTagBox.Append(addBtn)

		popover.SetChild(newTagBox)

		popover.Show()
	})
	addTagBtn.SetMarginStart(10)
	addTagBtn.SetMarginTop(10)
	addTagBtn.SetMarginEnd(10)
	addTagBtn.SetMarginBottom(10)

	return addTagBtn
}

func (ca *ChertApplication) handleSearchEntry(query string) {
	found := ca.registry.Search(query)

	if len(found) == 0 {
		ca.ShowError("No results found", errors.New("Did not find any results"))
		return
	}

	if ca.tabs.NPages() == 0 {
		ca.mainStack.SetVisibleChild(ca.tabs)
	}

	sr := search.NewSearchResults(query, found, ca)
	ca.searchResults[query] = sr

	lbl := gtk.NewLabel(fmt.Sprintf("🔎 %s", query))
	closeBtn := gtk.NewButtonWithLabel("x")
	closeBtn.SetName("closebtn")
	closeBtn.ConnectClicked(func() {
		ca.tabs.RemovePage(ca.tabs.PageNum(sr.GetView()))
		delete(ca.searchResults, query)

		if ca.tabs.NPages() == 0 {
			ca.mainStack.SetVisibleChild(ca.welcomeBox)
		}
	})
	closeBtn.SetMarginStart(8)
	closeBtn.SetMarginTop(4)
	closeBtn.SetMarginEnd(4)
	closeBtn.SetMarginBottom(4)

	lblBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	lblBox.Append(lbl)
	lblBox.Append(closeBtn)

	ca.tabs.AppendPage(sr.GetView(), lblBox)

	ca.tabs.SetCurrentPage(ca.tabs.PageNum(sr.GetView()))
}

func (ca *ChertApplication) ResizeTagsChanged(resize bool) {
	ca.config.ResizeTitleTags = resize
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleResizeTagsChange(resize)
	}
}

func (ca *ChertApplication) HighlightRowChanged(highight bool) {
	ca.config.HighlightCurrentLine = highight
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleHighlightRow(highight)
	}
}

func (ca *ChertApplication) ShowLineNumbersChanged(show bool) {
	ca.config.ShowLineNumbers = show
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleShowLineNumbersChange(show)
	}
}
func (ca *ChertApplication) AutoIndentChanged(indent bool) {
	ca.config.AutoIndent = indent
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleAutoIndentChanged(indent)
	}
}

func (ca *ChertApplication) LinkColorChanged(color string) {
	ca.config.LinkColor = color
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleLinkColorChanged(color)
	}
}

func (ca *ChertApplication) TabSpaceChanged(val int) {
	ca.config.TabWidth = val
	ca.saveConfig()
	for _, ec := range ca.openPages {
		ec.HandleTabSpaceChanged(val)
	}
}

func (ca *ChertApplication) EmojiSizeChanged(val int) {
	ca.config.EmojiSize = val
	ca.saveConfig()
	ca.setCSS(ca.config.Font)
	ca.tagsView.HandleEmojiSizeChanged(val)
	ca.notebookView.HandleEmojiSizeChanged(val)

	for _, ec := range ca.openPages {
		ec.HandleEmojiSizeChanged(val)
	}
}

func (ca *ChertApplication) DictionaryChanged(path string) {
	ca.config.DictionaryBasePath = path
	ca.saveConfig()
}

func (ca *ChertApplication) saveConfig() {
	err := ca.configManager.SaveConfig(ca.config)
	if err != nil {
		ca.ShowError("Error saving config", err)
	}
}
