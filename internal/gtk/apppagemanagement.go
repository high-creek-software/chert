package gtk

import (
	"fmt"
	fmthtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/atotto/clipboard"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	pdf "github.com/stephenafamo/goldmark-pdf"
	"github.com/yuin/goldmark"
	highlighting "github.com/yuin/goldmark-highlighting"
	"github.com/yuin/goldmark/extension"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/config"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/editorcomponents"
	"gopkg.in/errgo.v2/fmt/errors"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
)

func (ca *ChertApplication) PageSelected(page *core.Page) {
	if ca.tabs.NPages() == 0 {
		ca.mainStack.SetVisibleChild(ca.tabs)
	}

	if ec, ok := ca.openPages[page.RelativePath]; ok {
		idx := ca.tabs.PageNum(ec.GetView())
		ca.tabs.SetCurrentPage(idx)
		return
	}

	dirtyLbl := gtk.NewLabel("")
	dirtyLbl.SetMarginStart(8)

	ec := editorcomponents.NewEditorComponents(ca.registry, ca.config, ca.logger, ca,
		ca.language, ca.scheme, page, ca.spelling, dirtyLbl, ca.fontDesc)

	ca.openPages[page.RelativePath] = ec

	lbl := gtk.NewLabel(page.RelativePath)

	closeBtn := gtk.NewButtonWithLabel("X")
	closeBtn.SetName("closebtn")
	closeBtn.ConnectClicked(func() {
		ca.tabs.RemovePage(ca.tabs.PageNum(ec.GetView()))
		delete(ca.openPages, page.RelativePath)

		if ca.tabs.NPages() == 0 {
			ca.mainStack.SetVisibleChild(ca.welcomeBox)
		}
	})
	closeBtn.SetMarginStart(8)
	closeBtn.SetMarginTop(4)
	closeBtn.SetMarginEnd(4)
	closeBtn.SetMarginBottom(4)

	lblBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	lblBox.Append(lbl)
	lblBox.Append(dirtyLbl)
	lblBox.Append(closeBtn)

	ca.tabs.AppendPage(ec.GetView(), lblBox)

	ca.tabs.SetCurrentPage(ca.tabs.PageNum(ec.GetView()))
}

func (ca *ChertApplication) RequestPageRename(page *core.Page, rename string) error {
	err := ca.registry.RenamePage(page, rename)
	if err != nil {
		ca.ShowError("Error renaming page", err)
	}
	return err
}

func (ca *ChertApplication) RequestCut(page *core.Page) error {
	ca.pendingCut = &core.PendingCut{Page: page, ParentNotebook: ca.selectedNotebook}
	ca.notebookView.SetPendingCut(true)
	return nil
}

func (ca *ChertApplication) RequestPageDelete(page *core.Page, p *gtk.TreePath) error {
	dialog := gtk.NewMessageDialog(&(ca.Window.Window), gtk.DialogModal, gtk.MessageWarning, gtk.ButtonsOKCancel)
	dialog.SetMarkup(fmt.Sprintf("Delete %s", page.RelativePath))

	dialog.ConnectResponse(func(responseId int) {
		dialog.Destroy()

		switch responseId {
		case int(gtk.ResponseCancel):
			// Just do nothing for a cancel.
		case int(gtk.ResponseOK):
			err := ca.registry.DeletePage(page)
			if err != nil {
				ca.ShowError("Error deleting page", err)
			} else {
				ca.pageView.FinalizeDelete(p)
			}
		}
	})

	dialog.Show()

	return nil
}

func (ca *ChertApplication) RequestCopyPageInternalLink(page *core.Page) {
	link := fmt.Sprintf("[[%s]]", page.RelativePath)
	err := clipboard.WriteAll(link)
	if err != nil {
		ca.ShowError("Error copying link", err)
	}
}

func (ca *ChertApplication) ToggleMetaDataExpander(relativePath string, isOpen bool) {
	for key, ec := range ca.openPages {
		if key != relativePath {
			ec.ToggleMeta(isOpen)
		}
	}

	ca.config.HidePageManagement = !isOpen
	ca.configManager.SaveConfig(ca.config)
}

func (ca *ChertApplication) ChooseImage(result func(path string)) {
	imageChooser := gtk.NewFileChooserNative("Add Image", &ca.Window.Window, gtk.FileChooserActionOpen, "Open", "Cancel")
	filter := gtk.NewFileFilter()
	filter.AddPattern("*.jpg")
	filter.AddPattern("*.png")
	imageChooser.AddFilter(filter)
	imageChooser.ConnectResponse(func(responseId int) {
		if responseId != int(gtk.ResponseAccept) {
			return
		}

		path := imageChooser.File().Path()
		result(path)
	})
	imageChooser.Show()
}

func (ca *ChertApplication) ExportPDF(page *core.Page) {
	saveChooser := gtk.NewFileChooserNative("Choose file", &ca.Window.Window, gtk.FileChooserActionSave, "Save", "Cancel")
	saveChooser.ConnectResponse(func(responseId int) {
		if responseId != int(gtk.ResponseAccept) {
			return
		}

		path := saveChooser.File().Path()

		ca.exportPDF(path, page)
	})
	saveChooser.Show()
}

func (ca *ChertApplication) exportPDF(path string, page *core.Page) {
	md, err := ioutil.ReadFile(page.Path)
	if err != nil {
		ca.ShowError("Could not load markdown", err)
		return
	}

	out, err := os.Create(path)
	if err != nil {
		ca.ShowError("Could not open output file", err)
		return
	}

	imageDir := strings.Replace(page.Path, page.Name, "", 1)
	markdownPDF := ca.constructPDFMarkdown(os.DirFS(imageDir))
	err = markdownPDF.Convert(md, out)

	if err != nil {
		ca.ShowError("Error parsing markdown", err)
	}
}

func (ca *ChertApplication) constructPDFMarkdown(imageFS fs.FS) goldmark.Markdown {

	linkColor, ok := config.GetColor(ca.config.LinkColor)
	if !ok {
		ca.ShowError("Color not found", errors.Newf("Error finding color %s", ca.config.LinkColor))
		return nil
	}

	return goldmark.New(
		goldmark.WithRenderer(pdf.New(
			pdf.WithImageFS(imageFS),
			pdf.WithLinkColor(linkColor),
		)),
		goldmark.WithExtensions(
			extension.GFM,
			extension.NewFootnote(),
			highlighting.NewHighlighting(
				highlighting.WithStyle(ca.config.HighlightStyle),
				highlighting.WithGuessLanguage(true),
				highlighting.WithFormatOptions(fmthtml.WithLineNumbers(true)),
			),
		),
	)
}
