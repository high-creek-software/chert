{{if .FontFamily}}
#contentview {
    font-family: {{.FontFamily}};
    font-size: {{.FontSize}}px;
}
{{end}}

#error-sub {
    font-size: 18px;
}

#closebtn {
    padding: 2px;
    background: transparent;
    border: none;
}

#welcome-lbl {
    font-size: 18px;
}

#search-result-query {
    font-size: 32px;
}

#search-result-page {
    font-size: 18px;
}

#config-subhead {
    font-size: 16px;
}

#emoji-title {
    font-size: {{.EmojiTitle}}px;
}