package notebook

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
)

type NotebookList interface {
	GetView() gtk.Widgetter
	SetNotebook(notebook *core.Notebook)
	AddNotebook(notebook *core.Notebook)
	SetPendingCut(pendingCut bool)
	UnselectAll()
	FinalizeDelete(p *gtk.TreePath)
	HandleEmojiSizeChanged(val int)
}
