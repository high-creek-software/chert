package notebook

import (
	"fmt"
	"github.com/diamondburned/gotk4/pkg/core/glib"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/chert/internal/core"
)

type NotebookManagement interface {
	NotebookSelected(notebook *core.Notebook)
	RequestAddChild(notebook *core.Notebook, childName string) error
	RequestRename(notebook *core.Notebook, rename string) error
	RequestCreatePage(notebook *core.Notebook, pageName string) error
	RequestDelete(notebook *core.Notebook, p *gtk.TreePath) error
	RequestPaste(notebook *core.Notebook) error
	RequestEmoji(relativePath string, callback func(text string))
}

type NotebookTreeView struct {
	scrolledWindow *gtk.ScrolledWindow
	*gtk.TreeView

	notebookCellRenderer *gtk.CellRendererText
	notebookColumn       *gtk.TreeViewColumn

	notebookListStore *notebookListStore

	notebookManagement NotebookManagement

	hasPendingCut bool

	emojiRender *gtk.CellRendererText
}

func NewNotebookTreeView(notebookManagement NotebookManagement, conf *core.Config) NotebookList {
	nls := newNotebookListStore()

	scrolledWindow := gtk.NewScrolledWindow()

	ncr := gtk.NewCellRendererText()
	column := gtk.NewTreeViewColumn()
	column.SetTitle("Notebook")

	emojiRender := gtk.NewCellRendererText()
	emojiRender.SetObjectProperty("size", conf.EmojiSize*pango.SCALE)

	column.PackStart(emojiRender, false)
	column.AddAttribute(emojiRender, "text", 0)

	column.PackEnd(ncr, true)
	column.AddAttribute(ncr, "text", 1)

	column.SetResizable(true)

	notebookTreeView := gtk.NewTreeView()
	scrolledWindow.SetChild(notebookTreeView)
	notebookTreeView.SetHExpand(true)
	notebookTreeView.SetVExpand(true)
	//notebookTreeView.SetSizeRequest(150, -1)

	notebookTreeView.AppendColumn(column)
	notebookTreeView.SetModel(nls)

	notebookTreeView.SetActivateOnSingleClick(true)

	ntv := &NotebookTreeView{
		scrolledWindow:       scrolledWindow,
		TreeView:             notebookTreeView,
		notebookCellRenderer: ncr,
		notebookColumn:       column,
		notebookListStore:    nls,
		notebookManagement:   notebookManagement,
		emojiRender:          emojiRender,
	}

	notebookTreeView.ConnectRowActivated(ntv.rowActivated)

	clickGesture := gtk.NewGestureClick()
	clickGesture.SetButton(gdk.BUTTON_SECONDARY)
	clickGesture.ConnectPressed(ntv.rightClick)
	notebookTreeView.AddController(clickGesture)

	return ntv
}

func (ntv *NotebookTreeView) SetNotebook(notebook *core.Notebook) {
	ntv.notebookListStore.Clear()
	ntv.notebookListStore.setNotebook(notebook)
	ntv.ExpandRow(gtk.NewTreePathFromString("0"), false)
	ntv.notebookManagement.NotebookSelected(notebook)
}

func (ntv *NotebookTreeView) AddNotebook(notebook *core.Notebook) {
	ntv.notebookListStore.addNotebook(notebook.Name)
}

func (ntv *NotebookTreeView) GetView() gtk.Widgetter {
	return ntv.scrolledWindow
}

func (ntv *NotebookTreeView) rowActivated(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	ntv.notebookManagement.NotebookSelected(ntv.notebookListStore.getNotebookAtPath(path))
}

func (ntv *NotebookTreeView) rightClick(nPress int, x, y float64) {
	// I need to convert coordinates here, as the row was one greater than expected when turning the path into a notebook
	path, column, _, _, _ := ntv.PathAtPos(ntv.ConvertWidgetToBinWindowCoords(int(x), int(y)))
	if path != nil {
		notebook := ntv.notebookListStore.getNotebookAtPath(path)

		rect := ntv.CellArea(path, column)
		popoverRect := gdk.NewRectangle(rect.X(), rect.Y()+35, rect.Width(), rect.Height())
		popover := gtk.NewPopover()
		popover.SetParent(ntv.TreeView)
		popover.SetPointingTo(&popoverRect)
		notebookMenu := NewNotebookMenu(notebook.Name, ntv.hasPendingCut, func() {
			popover.Hide()
			ntv.handleRequestPaste(notebook, path)
		}, func() {
			popover.Hide()
			ntv.handleEmojiRequested(notebook, path)
		}, func(newNote string) {
			popover.Hide()
			ntv.handleNewNoteRequest(notebook, newNote, path)
		}, func(childName string) {
			popover.Hide()
			ntv.handleAddChild(notebook, childName, path)
		}, func(rename string) {
			popover.Hide()
			ntv.handleRenameRequest(notebook, rename, path)
		}, func() {
			popover.Hide()
			ntv.handleDeleteRequest(notebook, path)
		})
		popover.SetChild(notebookMenu)
		popover.Show()
	}
}

func (ntv *NotebookTreeView) handleNewNoteRequest(notebook *core.Notebook, pageName string, p *gtk.TreePath) {
	if res := ntv.notebookManagement.RequestCreatePage(notebook, pageName); res == nil {

	}
}

func (ntv *NotebookTreeView) handleRenameRequest(notebook *core.Notebook, rename string, p *gtk.TreePath) {
	if res := ntv.notebookManagement.RequestRename(notebook, rename); res == nil {
		if iter, ok := ntv.notebookListStore.Iter(p); ok {
			ntv.notebookListStore.Set(iter, []int{1}, []glib.Value{*glib.NewValue(rename)})
		}
	}
}

func (ntv *NotebookTreeView) handleAddChild(notebook *core.Notebook, childName string, p *gtk.TreePath) {
	if res := ntv.notebookManagement.RequestAddChild(notebook, childName); res == nil {
		if iter, ok := ntv.notebookListStore.Iter(p); ok {
			appendIter := ntv.notebookListStore.Append(iter)
			ntv.notebookListStore.Set(appendIter, []int{1}, []glib.Value{*glib.NewValue(childName)})
		}
	}
}

func (ntv *NotebookTreeView) handleDeleteRequest(notebook *core.Notebook, p *gtk.TreePath) {
	if res := ntv.notebookManagement.RequestDelete(notebook, p); res == nil {

	}
}

func (ntv *NotebookTreeView) FinalizeDelete(p *gtk.TreePath) {
	if iter, ok := ntv.notebookListStore.Iter(p); ok {
		ntv.notebookListStore.Remove(iter)
	}
}

func (ntv *NotebookTreeView) handleRequestPaste(notebook *core.Notebook, p *gtk.TreePath) {
	if res := ntv.notebookManagement.RequestPaste(notebook); res == nil {
		// Do something?
	}
}

func (ntv *NotebookTreeView) handleEmojiRequested(notebook *core.Notebook, p *gtk.TreePath) {
	ntv.notebookManagement.RequestEmoji(notebook.RelativePath, func(text string) {
		if iter, ok := ntv.notebookListStore.Iter(p); ok {
			ntv.notebookListStore.Set(iter, []int{0}, []glib.Value{*glib.NewValue(text)})
		}
	})
}

func (ntv *NotebookTreeView) SetPendingCut(pendingCut bool) {
	ntv.hasPendingCut = pendingCut
}

func (ntv *NotebookTreeView) UnselectAll() {
	if selection := ntv.Selection(); selection != nil {
		selection.UnselectAll()
	}
}

func (ntv *NotebookTreeView) HandleEmojiSizeChanged(val int) {
	ntv.emojiRender.SetObjectProperty("size", val*pango.SCALE)
}

type notebookListStore struct {
	*gtk.TreeStore
	notebook *core.Notebook
}

func newNotebookListStore() *notebookListStore {
	ls := gtk.NewTreeStore([]glib.Type{glib.TypeString, glib.TypeString})
	ls.SetSortColumnID(1, gtk.SortAscending)
	return &notebookListStore{TreeStore: ls}
}

func (nls *notebookListStore) setNotebook(notebook *core.Notebook) {
	nls.notebook = notebook

	app := nls.Append(nil)
	nls.Set(app, []int{0, 1}, []glib.Value{*glib.NewValue(notebook.Emoji), *glib.NewValue(notebook.Name)})

	nls.addChildren(notebook, app)
}

func (nls *notebookListStore) addChildren(nb *core.Notebook, iter *gtk.TreeIter) {
	for _, child := range nb.Children {
		childIter := nls.Append(iter)
		nls.Set(childIter, []int{0, 1}, []glib.Value{*glib.NewValue(child.Emoji), *glib.NewValue(child.Name)})

		if len(child.Children) > 0 {
			nls.addChildren(child, childIter)
		}
	}
}

func (nls *notebookListStore) addNotebook(notebook string) {
	//nls.Set(nls.Append(), []int{0}, []glib.Value{*glib.NewValue(notebook)})
}

func (nls *notebookListStore) getNotebookAtPath(p *gtk.TreePath) *core.Notebook {

	parent := nls.notebook
	idxs := p.Indices()
	previous := fmt.Sprintf("%d", idxs[0])
	for _, idx := range idxs[1:] {
		previous = fmt.Sprintf("%s:%d", previous, idx)
		pPath := gtk.NewTreePathFromString(previous)
		iter, ok := nls.Iter(pPath)
		if !ok {
			break
		}
		v := nls.Value(iter, 1)
		name := v.String()
		parent = parent.Children[name]
	}

	return parent
}

type notebookMenu struct {
	*gtk.Box
}

func NewNotebookMenu(name string, hasPendingCut bool, pasteRequested func(), emojiRequested func(), newNoteRequested func(name string), newChildRequested func(name string), renameRequested func(name string), deleteRequested func()) *notebookMenu {
	box := gtk.NewBox(gtk.OrientationVertical, 10)

	nameLabel := gtk.NewLabel(name)
	box.Append(nameLabel)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	if hasPendingCut {
		pasteBtn := gtk.NewButtonWithLabel("Paste")
		pasteBtn.ConnectClicked(pasteRequested)
		box.Append(pasteBtn)

		sep = gtk.NewSeparator(gtk.OrientationHorizontal)
		box.Append(sep)
	}

	emojiButton := gtk.NewButtonWithLabel("Choose Emoji")
	box.Append(emojiButton)
	emojiButton.ConnectClicked(emojiRequested)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	newNoteLabel := gtk.NewLabel("Add note")
	box.Append(newNoteLabel)
	newNoteEntry := gtk.NewEntry()
	box.Append(newNoteEntry)
	newNoteBtn := gtk.NewButtonWithLabel("New Note")
	newNoteBtn.ConnectClicked(func() {
		if newNoteRequested != nil {
			newNoteRequested(newNoteEntry.Text())
		}
	})
	box.Append(newNoteBtn)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	childLabel := gtk.NewLabel("Add child notebook")
	box.Append(childLabel)
	childEntry := gtk.NewEntry()
	box.Append(childEntry)
	addChild := gtk.NewButtonWithLabel("Add Notebook")
	addChild.ConnectClicked(func() {
		if newChildRequested != nil {
			newChildRequested(childEntry.Text())
		}
	})
	box.Append(addChild)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	renameLabel := gtk.NewLabel("Rename Notebook")
	box.Append(renameLabel)
	renameEntry := gtk.NewEntry()
	renameEntry.SetPlaceholderText(name)
	box.Append(renameEntry)
	renameBtn := gtk.NewButtonWithLabel("Rename Notebook")
	renameBtn.ConnectClicked(func() {
		if renameRequested != nil {
			renameRequested(renameEntry.Text())
		}
	})
	box.Append(renameBtn)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	deleteLabel := gtk.NewLabel(fmt.Sprintf("Delete %s", name))
	box.Append(deleteLabel)
	deleteBtn := gtk.NewButtonFromIconName("edit-delete")
	deleteBtn.ConnectClicked(func() {
		if deleteRequested != nil {
			deleteRequested()
		}
	})
	box.Append(deleteBtn)

	return &notebookMenu{Box: box}
}
