package page

import (
	"fmt"
	"github.com/diamondburned/gotk4/pkg/core/glib"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
	"strings"
)

type PageManagement interface {
	PageSelected(page *core.Page)
	RequestPageRename(page *core.Page, rename string) error
	RequestCut(page *core.Page) error
	RequestPageDelete(page *core.Page, p *gtk.TreePath) error
	RequestCopyPageInternalLink(page *core.Page)
}

type PageList interface {
	GetView() gtk.Widgetter
	AddPage(page *core.Page)
	ClearPages()
	SelectFirst()
	UpdateColumn(text, emoji string)
	FinalizeDelete(p *gtk.TreePath)
}

type PageTreeView struct {
	scrolledWindow *gtk.ScrolledWindow
	*gtk.TreeView

	pageCellRenderer *gtk.CellRendererText
	tagsRender       *gtk.CellRendererText
	pageColumn       *gtk.TreeViewColumn

	pageListStore *pageListStore

	pageManagement PageManagement
}

func NewPageTreeView(pageManagement PageManagement) PageList {
	pls := newPageListStore()
	scrolledWindow := gtk.NewScrolledWindow()
	scrolledWindow.SetSizeRequest(250, -1)

	pcr := gtk.NewCellRendererText()
	tagsRender := gtk.NewCellRendererText()
	column := gtk.NewTreeViewColumn()
	column.SetTitle("Page")

	column.PackStart(pcr, true)
	column.AddAttribute(pcr, "text", 0)
	column.SetResizable(true)

	column.PackEnd(tagsRender, false)
	column.AddAttribute(tagsRender, "text", 1)

	pageTreeView := gtk.NewTreeView()
	scrolledWindow.SetChild(pageTreeView)
	pageTreeView.SetHExpand(true)
	pageTreeView.SetSizeRequest(150, -1)

	pageTreeView.AppendColumn(column)
	pageTreeView.SetModel(pls)

	pageTreeView.SetActivateOnSingleClick(true)

	ptv := &PageTreeView{
		scrolledWindow:   scrolledWindow,
		TreeView:         pageTreeView,
		pageCellRenderer: pcr,
		tagsRender:       tagsRender,
		pageColumn:       column,
		pageListStore:    pls,
		pageManagement:   pageManagement,
	}

	pageTreeView.ConnectRowActivated(ptv.rowActivated)

	clickGesture := gtk.NewGestureClick()
	clickGesture.SetButton(gdk.BUTTON_SECONDARY)
	clickGesture.ConnectPressed(ptv.rightClick)
	pageTreeView.AddController(clickGesture)

	return ptv
}

func (ptv *PageTreeView) UpdateColumn(text, emoji string) {

	box := gtk.NewBox(gtk.OrientationHorizontal, 0)
	emojiLbl := gtk.NewLabel(emoji)
	emojiLbl.SetName("emoji-title")
	box.Append(emojiLbl)

	txtLbl := gtk.NewLabel(text)
	box.Append(txtLbl)

	ptv.pageColumn.SetWidget(box)
}

func (ptv *PageTreeView) GetView() gtk.Widgetter {
	return ptv.scrolledWindow
}

func (ptv *PageTreeView) AddPage(page *core.Page) {
	ptv.pageListStore.addPage(page)
}

func (ptv *PageTreeView) ClearPages() {
	ptv.pageListStore.clearPages()
}

func (ptv *PageTreeView) rowActivated(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	ptv.pageManagement.PageSelected(ptv.pageListStore.getPageAtPath(path))
}

func (ptv *PageTreeView) SelectFirst() {
	if ptv.pageListStore.IterNChildren(nil) > 0 {
		path := gtk.NewTreePathFirst()
		if iter, ok := ptv.pageListStore.Iter(path); ok {
			ptv.Selection().SelectIter(iter)
			ptv.pageManagement.PageSelected(ptv.pageListStore.getPageAtPath(path))
		}
	}
}

func (ptv *PageTreeView) rightClick(nPress int, x, y float64) {
	path, col, _, _, _ := ptv.PathAtPos(ptv.ConvertWidgetToBinWindowCoords(int(x), int(y)))
	if path != nil {
		page := ptv.pageListStore.getPageAtPath(path)

		rect := ptv.CellArea(path, col)
		popoverRect := gdk.NewRectangle(rect.X(), rect.Y()+35, rect.Width(), rect.Height())
		popover := gtk.NewPopover()
		popover.SetParent(ptv.TreeView)
		popover.SetPointingTo(&popoverRect)

		pageMenu := NewPageMenu(page.Name, func(renameRequest string) {
			ptv.handleRenameRequest(page, renameRequest, path)
			popover.Hide()
		}, func() {
			ptv.handleCutRequest(page, path)
			popover.Hide()
		}, func() {
			popover.Hide()
			ptv.pageManagement.RequestCopyPageInternalLink(page)
		}, func() {
			popover.Hide()
			ptv.handleDeleteRequest(page, path)
		})

		popover.SetChild(pageMenu)
		popover.Show()
	}
}

func (ptv *PageTreeView) handleRenameRequest(page *core.Page, renameRequest string, p *gtk.TreePath) {
	if res := ptv.pageManagement.RequestPageRename(page, renameRequest); res == nil {
		if iter, ok := ptv.pageListStore.Iter(p); ok {
			ptv.pageListStore.Set(iter, []int{0}, []glib.Value{*glib.NewValue(renameRequest)})
		}
	}
}

func (ptv *PageTreeView) handleCutRequest(page *core.Page, p *gtk.TreePath) {
	ptv.pageManagement.RequestCut(page)
}

func (ptv *PageTreeView) handleDeleteRequest(page *core.Page, p *gtk.TreePath) {
	ptv.pageManagement.RequestPageDelete(page, p)
}

func (ptv *PageTreeView) FinalizeDelete(p *gtk.TreePath) {
	if iter, ok := ptv.pageListStore.Iter(p); ok {
		ptv.pageListStore.Remove(iter)
	}
}

type pageListStore struct {
	*gtk.ListStore
	backingStore map[string]*core.Page
}

func newPageListStore() *pageListStore {
	ls := gtk.NewListStore([]glib.Type{glib.TypeString, glib.TypeString})
	ls.SetSortColumnID(0, gtk.SortAscending)
	return &pageListStore{ListStore: ls, backingStore: make(map[string]*core.Page)}
}

func (pls *pageListStore) addPage(page *core.Page) {

	var sb strings.Builder
	if page.Tags != nil && len(page.Tags) > 0 {
		sb.WriteString(page.Tags[0].Emoji)

		if len(page.Tags) > 1 {
			for _, t := range page.Tags[1:] {
				sb.WriteString(" ")
				sb.WriteString(t.Emoji)
			}
		}
	}

	pls.Set(pls.Append(), []int{0, 1}, []glib.Value{*glib.NewValue(page.Name), *glib.NewValue(sb.String())})
	pls.backingStore[page.Name] = page
}

func (pls *pageListStore) getPageAtPath(p *gtk.TreePath) *core.Page {
	iter, _ := pls.Iter(p)
	valAtPath := pls.Value(iter, 0)

	return pls.backingStore[valAtPath.String()]
}

func (pls *pageListStore) clearPages() {
	pls.Clear()
	pls.backingStore = make(map[string]*core.Page)
}

type pageMenu struct {
	*gtk.Box
}

func NewPageMenu(name string, renameRequest func(rename string), cutRequest func(), copyRequest func(), deleteRequest func()) *pageMenu {
	box := gtk.NewBox(gtk.OrientationVertical, 10)

	nameLabel := gtk.NewLabel(name)
	box.Append(nameLabel)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	cutBtn := gtk.NewButtonWithLabel("Cut")
	cutBtn.ConnectClicked(cutRequest)
	box.Append(cutBtn)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	internalLabel := gtk.NewLabel("Copy internal link")
	box.Append(internalLabel)
	copyLink := gtk.NewButtonWithLabel("Copy")
	copyLink.ConnectClicked(func() {
		copyRequest()
	})
	box.Append(copyLink)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	renameLabel := gtk.NewLabel("Rename Page")
	box.Append(renameLabel)

	renameEntry := gtk.NewEntry()
	renameEntry.SetPlaceholderText(name)
	box.Append(renameEntry)
	renameBtn := gtk.NewButtonWithLabel("Rename")
	renameBtn.ConnectClicked(func() {
		renameRequest(renameEntry.Text())
	})
	box.Append(renameBtn)

	sep = gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	deleteLabel := gtk.NewLabel(fmt.Sprintf("Delete %s", name))
	box.Append(deleteLabel)
	deleteBtn := gtk.NewButtonFromIconName("edit-delete")
	deleteBtn.ConnectClicked(func() {
		deleteRequest()
	})
	box.Append(deleteBtn)

	return &pageMenu{Box: box}
}
