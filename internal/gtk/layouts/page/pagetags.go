package page

import (
	"github.com/diamondburned/gotk4/pkg/core/glib"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/chert/internal/core"
)

type PageTagManagement interface {
	AddTagToActivePage(tagName string) error
	RemoveTagFromActivePage(tagName string) error
	TagSelected(tagName string, tagSource core.TagSource)
}

type PageTags struct {
	*gtk.Box
	combo      *gtk.ComboBox
	comboModel *gtk.ListStore
	treeView   *gtk.TreeView
	treeModel  *gtk.ListStore

	pageTagManagement PageTagManagement

	comboEmoji  *gtk.CellRendererText
	emojiRender *gtk.CellRendererText
}

func NewPageTags(pageTagManagement PageTagManagement, conf *core.Config) *PageTags {
	box := gtk.NewBox(gtk.OrientationVertical, 0)
	box.SetHExpand(true)

	combo := gtk.NewComboBox()

	comboEmoji := gtk.NewCellRendererText()
	comboEmoji.SetObjectProperty("size", conf.EmojiSize*pango.SCALE)
	combo.PackStart(comboEmoji, false)
	combo.AddAttribute(comboEmoji, "text", 0)

	comboRender := gtk.NewCellRendererText()
	combo.PackStart(comboRender, true)
	combo.AddAttribute(comboRender, "text", 1)

	comboModel := gtk.NewListStore([]glib.Type{glib.TypeString, glib.TypeString})
	combo.SetModel(comboModel)
	comboModel.SetSortColumnID(1, gtk.SortAscending)

	box.Append(combo)

	treeView := gtk.NewTreeView()
	treeView.SetActivateOnSingleClick(true)
	treeView.SetMarginTop(10)
	treeView.SetVExpand(true)
	box.Append(treeView)

	col := gtk.NewTreeViewColumn()
	col.SetTitle("Page Tags")

	emojiRender := gtk.NewCellRendererText()
	emojiRender.SetObjectProperty("size", conf.EmojiSize*pango.SCALE)
	col.PackStart(emojiRender, false)
	col.AddAttribute(emojiRender, "text", 0)

	render := gtk.NewCellRendererText()
	col.PackEnd(render, true)
	col.AddAttribute(render, "text", 1)
	col.SetResizable(true)

	treeView.AppendColumn(col)

	treeModel := gtk.NewListStore([]glib.Type{glib.TypeString, glib.TypeString})
	treeModel.SetSortColumnID(1, gtk.SortAscending)
	treeView.SetModel(treeModel)

	pt := &PageTags{
		Box:               box,
		combo:             combo,
		comboModel:        comboModel,
		treeView:          treeView,
		treeModel:         treeModel,
		pageTagManagement: pageTagManagement,
		comboEmoji:        comboEmoji,
		emojiRender:       emojiRender,
	}

	combo.ConnectChanged(pt.comboChanged)
	treeView.ConnectRowActivated(pt.tagRowSelected)

	clickGesture := gtk.NewGestureClick()
	clickGesture.SetButton(gdk.BUTTON_SECONDARY)
	clickGesture.ConnectPressed(pt.rightClick)
	treeView.AddController(clickGesture)

	return pt
}

func (pt *PageTags) comboChanged() {
	if iter, ok := pt.combo.ActiveIter(); ok {
		emoji := pt.comboModel.Value(iter, 0)
		name := pt.comboModel.Value(iter, 1)
		pt.combo.SetActive(-1)

		if err := pt.pageTagManagement.AddTagToActivePage(name.String()); err == nil {
			pt.addPageTagName(emoji.String(), name.String())
		}
	}
}

func (pt *PageTags) UnselectAll() {
	if selection := pt.treeView.Selection(); selection != nil {
		selection.UnselectAll()
	}
}

func (pt *PageTags) GetView() gtk.Widgetter {
	return pt.Box
}

func (pt *PageTags) AddTag(tag *core.Tag) {
	pt.comboModel.Set(pt.comboModel.Append(), []int{0, 1}, []glib.Value{*glib.NewValue(tag.Emoji), *glib.NewValue(tag.Name)})
}

func (pt *PageTags) addPageTagName(emoji, name string) {
	pt.treeModel.Set(pt.treeModel.Append(), []int{0, 1}, []glib.Value{*glib.NewValue(emoji), *glib.NewValue(name)})
}

func (pt *PageTags) AddPageTag(tag *core.Tag) {
	pt.addPageTagName(tag.Emoji, tag.Name)
}

func (pt *PageTags) HandleEmojiSizeChanged(val int) {
	pt.emojiRender.SetObjectProperty("size", val*pango.SCALE)
	pt.comboEmoji.SetObjectProperty("size", val*pango.SCALE)
}

func (pt *PageTags) tagRowSelected(path *gtk.TreePath, col *gtk.TreeViewColumn) {
	iter, _ := pt.treeModel.Iter(path)
	valAtPath := pt.treeModel.Value(iter, 1)

	pt.pageTagManagement.TagSelected(valAtPath.String(), core.TagPage)
}

func (pt *PageTags) tagNameAtSelection(path *gtk.TreePath, col *gtk.TreeViewColumn) string {
	iter, _ := pt.treeModel.Iter(path)
	valAtPath := pt.treeModel.Value(iter, 1)
	return valAtPath.String()
}

func (pt *PageTags) rightClick(nPress int, x, y float64) {
	path, col, _, _, _ := pt.treeView.PathAtPos(pt.treeView.ConvertWidgetToBinWindowCoords(int(x), int(y)))

	if path != nil {
		tagName := pt.tagNameAtSelection(path, col)

		rect := pt.treeView.CellArea(path, col)
		popoverRect := gdk.NewRectangle(rect.X(), rect.Y()+35, rect.Width(), rect.Height())
		popover := gtk.NewPopover()
		popover.SetParent(pt.treeView)
		popover.SetPointingTo(&popoverRect)

		menuBox := gtk.NewBox(gtk.OrientationVertical, 10)

		removeBtn := gtk.NewButtonWithLabel("Remove Tag")
		removeBtn.ConnectClicked(func() {
			popover.Hide()
			if err := pt.pageTagManagement.RemoveTagFromActivePage(tagName); err == nil {
				if iter, ok := pt.treeModel.Iter(path); ok {
					pt.treeModel.Remove(iter)
				}
			}
		})
		menuBox.Prepend(removeBtn)

		popover.SetChild(menuBox)
		popover.Show()
	}
}
