package config

import (
	"fmt"
	"github.com/diamondburned/gotk4/pkg/glib/v2"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/margin"
	"slices"
)

type ConfigManagement interface {
	ResizeTagsChanged(resize bool)
	HighlightRowChanged(highight bool)
	ShowLineNumbersChanged(show bool)
	AutoIndentChanged(indent bool)
	LinkColorChanged(color string)
	TabSpaceChanged(val int)
	EmojiSizeChanged(val int)
	DictionaryChanged(path string)
}

type ConfigBox struct {
	*gtk.Box

	conf             *core.Config
	configManagement ConfigManagement

	resizeBox *gtk.Box

	linkColorBox     *gtk.Box
	linkColorListBox *gtk.ListBox

	highlightBox *gtk.Box

	showLineNumberBox *gtk.Box

	autoIndentBox *gtk.Box

	tabSpaceBox *gtk.Box

	emojiSizeBox *gtk.Box

	dictionaryBox *gtk.Box
}

func NewConfigBox(conf *core.Config, configManagement ConfigManagement) *ConfigBox {
	cb := &ConfigBox{
		conf:             conf,
		configManagement: configManagement,
	}
	cb.setupResizeOption()
	cb.setupLinkColorOptions()
	cb.setupHighlightCurrentLine()
	cb.setupLineNumbers()
	cb.setupAutoIndent()
	cb.setupTabSpace()
	cb.setupEmojiSize()
	cb.setupDictionaryBox()

	/*chromaColorBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	chromaColorBox.SetMarginStart(10)
	chromaColorBox.SetMarginTop(10)
	chromaColorBox.SetMarginEnd(10)
	chromaColorBox.SetMarginBottom(10)

	chromaColorLbl := gtk.NewLabel("Rendered markdown code scheme")
	chromaColorBox.Prepend(chromaColorLbl)
	chromaColorSpacer := gtk.NewLabel("")
	chromaColorSpacer.SetHExpand(true)

	chromaColorBox.Append(chromaColorSpacer)
	chromaColorCombo := gtk.NewComboBox()
	chromaColorStore := gtk.NewListStore([]glib.Type{glib.TypeString})
	chromaColorStore.SetSortColumnID(0, gtk.SortAscending)
	chromaColorCombo.SetModel(chromaColorStore)
	chromaColorRender := gtk.NewCellRendererText()
	chromaColorCombo.PackStart(chromaColorRender, true)
	chromaColorCombo.AddAttribute(chromaColorRender, "text", 0)
	for _, chromaColor := range chromaColors {
		chromaColorStore.Set(chromaColorStore.Append(), []int{0}, []glib.Value{*glib.NewValue(chromaColor)})
	}

	chromaColorBox.Append(chromaColorCombo)*/

	cb.Box = gtk.NewBox(gtk.OrientationHorizontal, 0)

	firstBox := gtk.NewListBox()
	firstBox.SetSizeRequest(150, -1)
	firstBox.Append(cb.resizeBox)
	firstBox.Append(cb.highlightBox)
	firstBox.Append(cb.showLineNumberBox)
	firstBox.Append(cb.autoIndentBox)
	firstBox.Append(cb.tabSpaceBox)
	firstBox.Append(cb.emojiSizeBox)
	firstBox.Append(cb.dictionaryBox)
	cb.Box.Append(firstBox)
	cb.Box.Append(cb.linkColorBox)
	//cb.Box.Append(linkColorBox)
	//cb.Box.Append(chromaColorBox)

	cb.Box.SetMarginStart(10)
	cb.Box.SetMarginTop(10)
	cb.Box.SetMarginEnd(10)
	cb.Box.SetMarginBottom(10)

	return cb
}

func (cb *ConfigBox) setupResizeOption() {

	cb.resizeBox = gtk.NewBox(gtk.OrientationHorizontal, 10)
	margin.SetMarginAll(cb.resizeBox, 10)
	resizeLabel := gtk.NewLabel("Resize Tags In Editor")
	resizeSpacer := gtk.NewLabel("")
	resizeSpacer.SetHExpand(true)
	resizeTagsToggle := gtk.NewSwitch()
	//margin.SetMarginAll(resizeTagsToggle, 10)

	resizeTagsToggle.SetState(cb.conf.ResizeTitleTags)

	resizeTagsToggle.ConnectStateSet(func(state bool) bool {
		cb.configManagement.ResizeTagsChanged(state)
		return false
	})

	cb.resizeBox.Prepend(resizeLabel)
	cb.resizeBox.Append(resizeSpacer)
	cb.resizeBox.Append(resizeTagsToggle)
}

func (cb *ConfigBox) setupLinkColorOptions() {
	cb.linkColorBox = gtk.NewBox(gtk.OrientationVertical, 0)
	margin.SetMarginAll(cb.linkColorBox, 10)
	cb.linkColorBox.SetHExpand(true)
	lbl := gtk.NewLabel("Editor link color")
	lbl.SetHAlign(gtk.AlignStart)
	lbl.SetMarginBottom(10)
	lbl.SetName("config-subhead")
	cb.linkColorBox.Append(lbl)
	scroller := gtk.NewScrolledWindow()
	scroller.SetVExpand(true)
	cb.linkColorListBox = gtk.NewListBox()
	cb.linkColorListBox.SetSizeRequest(150, -1)
	scroller.SetChild(cb.linkColorListBox)
	cb.linkColorBox.Append(scroller)

	keys := make([]string, len(x11Colors))
	idx := 0
	for k := range x11Colors {
		keys[idx] = k
		idx++
	}
	slices.Sort(keys)
	for _, name := range keys {
		func(clr string) {
			glib.IdleAdd(func() {
				row := gtk.NewListBoxRow()
				colorLabel := gtk.NewLabel(clr)
				colorLabel.SetHAlign(gtk.AlignStart)
				margin.SetMarginAll(colorLabel, 12)
				row.SetChild(colorLabel)
				if clr == cb.conf.LinkColor {
					cb.linkColorListBox.SelectRow(row)
				}
				cb.linkColorListBox.Append(row)
			})
		}(name)
	}

	cb.linkColorListBox.ConnectRowSelected(func(row *gtk.ListBoxRow) {
		if row.Index() < 0 {
			return
		}
		selected := keys[row.Index()]
		cb.configManagement.LinkColorChanged(selected)
	})
}

func (cb *ConfigBox) setupHighlightCurrentLine() {
	cb.highlightBox = gtk.NewBox(gtk.OrientationHorizontal, 0)
	margin.SetMarginAll(cb.highlightBox, 10)
	lbl := gtk.NewLabel("Highlight current line")
	spacer := gtk.NewLabel("")
	spacer.SetHExpand(true)
	//spacer.SetHExpand(true)

	toggle := gtk.NewSwitch()
	//margin.SetMarginAll(toggle, 10)
	toggle.SetState(cb.conf.HighlightCurrentLine)

	toggle.ConnectStateSet(func(state bool) bool {
		cb.configManagement.HighlightRowChanged(state)
		return false
	})

	cb.highlightBox.Append(lbl)
	cb.highlightBox.Append(spacer)
	cb.highlightBox.Append(toggle)
}

func (cb *ConfigBox) setupLineNumbers() {
	cb.showLineNumberBox = gtk.NewBox(gtk.OrientationHorizontal, 0)
	margin.SetMarginAll(cb.showLineNumberBox, 10)
	lbl := gtk.NewLabel("Show line numbers")
	spacer := gtk.NewLabel("")
	spacer.SetHExpand(true)

	toggle := gtk.NewSwitch()
	toggle.SetState(cb.conf.ShowLineNumbers)

	toggle.ConnectStateSet(func(state bool) bool {
		cb.configManagement.ShowLineNumbersChanged(state)
		return false
	})

	cb.showLineNumberBox.Append(lbl)
	cb.showLineNumberBox.Append(spacer)
	cb.showLineNumberBox.Append(toggle)
}

func (cb *ConfigBox) setupAutoIndent() {
	cb.autoIndentBox = gtk.NewBox(gtk.OrientationHorizontal, 0)
	margin.SetMarginAll(cb.autoIndentBox, 10)
	lbl := gtk.NewLabel("Auto indent new line")
	spacer := gtk.NewLabel("")
	spacer.SetHExpand(true)
	toggle := gtk.NewSwitch()
	toggle.SetState(cb.conf.AutoIndent)

	toggle.ConnectStateSet(func(state bool) bool {
		cb.configManagement.AutoIndentChanged(state)
		return false
	})

	cb.autoIndentBox.Append(lbl)
	cb.autoIndentBox.Append(spacer)
	cb.autoIndentBox.Append(toggle)
}

func (cb *ConfigBox) setupTabSpace() {
	cb.tabSpaceBox = gtk.NewBox(gtk.OrientationVertical, 0)

	titleBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	lbl := gtk.NewLabel("Tab spaces")
	margin.SetMarginAll(lbl, 10)
	lbl.SetHAlign(gtk.AlignStart)
	lbl.SetHExpand(true)
	titleBox.Append(lbl)

	sizeLbl := gtk.NewLabel(fmt.Sprintf("%d", cb.conf.TabWidth))
	margin.SetMarginAll(sizeLbl, 10)
	titleBox.Append(sizeLbl)
	cb.tabSpaceBox.Append(titleBox)

	scale := gtk.NewScaleWithRange(gtk.OrientationHorizontal, 1, 10, 1)
	cb.tabSpaceBox.Append(scale)

	scale.ConnectChangeValue(func(st gtk.ScrollType, val float64) bool {
		sizeLbl.SetText(fmt.Sprintf("%d", int(val)))
		cb.configManagement.TabSpaceChanged(int(val))
		return false
	})

	scale.SetValue(float64(cb.conf.TabWidth))
}

func (cb *ConfigBox) setupEmojiSize() {
	cb.emojiSizeBox = gtk.NewBox(gtk.OrientationVertical, 0)
	titleBox := gtk.NewBox(gtk.OrientationHorizontal, 0)

	emojiSizeLbl := gtk.NewLabel("Emoji Size")
	margin.SetMarginAll(emojiSizeLbl, 10)
	emojiSizeLbl.SetHExpand(true)
	emojiSizeLbl.SetHAlign(gtk.AlignStart)
	sizeLbl := gtk.NewLabel(fmt.Sprintf("%d", cb.conf.EmojiSize))
	margin.SetMarginAll(sizeLbl, 10)

	titleBox.Append(emojiSizeLbl)
	titleBox.Append(sizeLbl)

	cb.emojiSizeBox.Append(titleBox)

	scale := gtk.NewScaleWithRange(gtk.OrientationHorizontal, 1, 30, 1)
	cb.emojiSizeBox.Append(scale)

	scale.ConnectChangeValue(func(st gtk.ScrollType, val float64) bool {
		cb.configManagement.EmojiSizeChanged(int(val))
		sizeLbl.SetText(fmt.Sprintf("%d", int(val)))
		return false
	})

	scale.SetValue(float64(cb.conf.EmojiSize))
}

func (cb *ConfigBox) setupDictionaryBox() {
	cb.dictionaryBox = gtk.NewBox(gtk.OrientationVertical, 0)

	dictionaryLbl := gtk.NewLabel("Dictionary list")
	dictionaryLbl.SetHExpand(true)
	dictionaryLbl.SetHAlign(gtk.AlignStart)
	margin.SetMarginAll(dictionaryLbl, 10)
	cb.dictionaryBox.Append(dictionaryLbl)

	combo := gtk.NewComboBox()
	margin.SetMarginAll(combo, 10)
	pathsListStore := gtk.NewListStore([]glib.Type{glib.TypeString})
	combo.SetModel(pathsListStore)
	pathsRender := gtk.NewCellRendererText()
	combo.PackStart(pathsRender, true)
	combo.AddAttribute(pathsRender, "text", 0)

	for _, dict := range cb.conf.AvailableDictionaries {
		pathsListStore.Set(pathsListStore.Append(), []int{0}, []glib.Value{*glib.NewValue(dict)})
	}

	cb.dictionaryBox.Append(combo)

	combo.ConnectChanged(func() {
		if iter, ok := combo.ActiveIter(); ok {
			path := pathsListStore.Value(iter, 0)
			cb.configManagement.DictionaryChanged(path.String())
		}
	})

	if cb.conf.DictionaryBasePath != "" && slices.Contains(cb.conf.AvailableDictionaries, cb.conf.DictionaryBasePath) {
		idx := slices.Index(cb.conf.AvailableDictionaries, cb.conf.DictionaryBasePath)
		combo.SetActive(idx)
	}
}
