package config

var chromaColors = []string{
	"arduino",
	"autumn",
	"base16-snazzy",
	"borland",
	"bw",
	"colorful",
	"doom-one",
	"doom-one2",
	"dracula",
	"monokai",
	"nord",
	"witchhazel",
}
