package search

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/chert/internal/core"
	"strings"
)

type SearchManagement interface {
	EditInternalLink(linkRelativePath string)
	OpenInternalLink(path string)
}

type SearchResults struct {
	query    string
	box      *gtk.Box
	queryLbl *gtk.Label

	scrolledWindow *gtk.ScrolledWindow
	listBox        *gtk.ListBox

	searchManagement SearchManagement
}

func NewSearchResults(query string, found []core.IndexPage, searchManagement SearchManagement) *SearchResults {
	box := gtk.NewBox(gtk.OrientationVertical, 0)
	queryLbl := gtk.NewLabel(query)
	queryLbl.SetName("search-result-query")
	queryLbl.SetHAlign(gtk.AlignStart)
	queryLbl.SetMarginTop(20)
	queryLbl.SetMarginStart(10)
	queryLbl.SetMarginBottom(20)
	box.Append(queryLbl)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	listBox := gtk.NewListBox()
	//listBox.SetActivateOnSingleClick(true)
	listBox.SetShowSeparators(true)
	scrolledWindow := gtk.NewScrolledWindow()
	scrolledWindow.SetChild(listBox)
	scrolledWindow.SetVExpand(true)
	box.Append(scrolledWindow)

	for _, indexPage := range found {
		rr := NewResultRow(indexPage.ID, indexPage.Fragments, searchManagement)
		lbr := gtk.NewListBoxRow()
		lbr.SetChild(rr.Box)
		listBox.Append(lbr)
	}

	//listBox.ConnectRowSelected(func(row *gtk.ListBoxRow) {
	//	log.Println("Row activated:", found[row.Index()].ID)
	//	searchManagement.EditInternalLink(found[row.Index()].ID)
	//})

	return &SearchResults{
		query:            query,
		box:              box,
		queryLbl:         queryLbl,
		scrolledWindow:   scrolledWindow,
		listBox:          listBox,
		searchManagement: searchManagement,
	}
}

func (sr *SearchResults) GetView() gtk.Widgetter {
	return sr.box
}

type ResultRow struct {
	Relativepath string
	Title        *gtk.Label
	Box          *gtk.Box
}

func NewResultRow(relativePath string, fragments []string, searchManagement SearchManagement) *ResultRow {
	title := gtk.NewLabel(relativePath)
	title.SetName("search-result-page")
	title.SetHAlign(gtk.AlignStart)
	title.SetVAlign(gtk.AlignCenter)
	title.SetMarginStart(10)
	title.SetMarginTop(10)

	titleBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	titleBox.Append(title)

	spacerLbl := gtk.NewLabel("")
	spacerLbl.SetHExpand(true)
	titleBox.Append(spacerLbl)

	openBtn := gtk.NewButtonWithLabel("Open in editor")
	openBtn.SetVAlign(gtk.AlignCenter)
	openBtn.ConnectClicked(func() {
		searchManagement.EditInternalLink(relativePath)
	})
	titleBox.Append(openBtn)

	browserBtn := gtk.NewButtonWithLabel("Open in browser")
	browserBtn.SetVAlign(gtk.AlignCenter)
	browserBtn.ConnectClicked(func() {
		searchManagement.OpenInternalLink(relativePath)
	})
	browserBtn.SetMarginStart(8)
	titleBox.Append(browserBtn)

	box := gtk.NewBox(gtk.OrientationVertical, 0)
	box.Append(titleBox)

	for _, fragment := range fragments {

		// https://docs.gtk.org/Pango/pango_markup.html
		fragment = strings.Replace(fragment, "<mark>", `<span foreground="black" background="yellow">`, -1)
		fragment = strings.Replace(fragment, "</mark>", "</span>", -1)

		fragLbl := gtk.NewLabel("")
		fragLbl.SetWrap(true)
		fragLbl.SetWrapMode(pango.WrapWord)
		fragLbl.SetHAlign(gtk.AlignStart)
		fragLbl.SetMarkup(fragment)
		fragLbl.SetMarginTop(20)
		fragLbl.SetMarginStart(10)
		box.Append(fragLbl)
	}

	box.SetMarginBottom(20)
	return &ResultRow{
		Relativepath: relativePath,
		Title:        title,
		Box:          box,
	}
}
