package links

import (
	"github.com/diamondburned/gotk4/pkg/core/glib"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
)

type LinksManagement interface {
	EditInternalLink(string)
}

type LinksTreeView struct {
	scrolledWindow *gtk.ScrolledWindow
	treeView       *gtk.TreeView
	listStore      *gtk.ListStore

	pageCellRenderer *gtk.CellRendererText
	pageColumn       *gtk.TreeViewColumn

	linksManagement LinksManagement
}

func NewLinksTreeView(linksManagement LinksManagement) *LinksTreeView {
	scrolledWindow := gtk.NewScrolledWindow()
	treeView := gtk.NewTreeView()
	treeView.SetMarginStart(5)
	treeView.SetMarginTop(45)
	treeView.SetMarginEnd(5)
	treeView.SetMarginBottom(5)

	scrolledWindow.SetChild(treeView)
	treeView.SetHExpand(true)
	treeView.SetVExpand(true)

	col := gtk.NewTreeViewColumn()
	col.SetTitle("Backlinks")

	cellRender := gtk.NewCellRendererText()

	col.PackEnd(cellRender, true)
	col.AddAttribute(cellRender, "text", 0)

	treeView.AppendColumn(col)

	listStore := gtk.NewListStore([]glib.Type{glib.TypeString})
	listStore.SetSortColumnID(0, gtk.SortAscending)

	treeView.SetModel(listStore)
	treeView.SetActivateOnSingleClick(true)

	ltv := &LinksTreeView{
		scrolledWindow:   scrolledWindow,
		treeView:         treeView,
		listStore:        listStore,
		pageCellRenderer: cellRender,
		pageColumn:       col,
		linksManagement:  linksManagement,
	}

	treeView.ConnectRowActivated(ltv.pageSelected)

	return ltv
}

func (ltv *LinksTreeView) pageSelected(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	iter, _ := ltv.listStore.Iter(path)
	valAtPath := ltv.listStore.Value(iter, 0)
	ltv.linksManagement.EditInternalLink(valAtPath.String())
}

func (ltv *LinksTreeView) AddLinked(relativePath string) {
	ltv.listStore.Set(ltv.listStore.Append(), []int{0}, []glib.Value{*glib.NewValue(relativePath)})
}

func (ltv *LinksTreeView) GetView() gtk.Widgetter {
	return ltv.scrolledWindow
}

func (ltv *LinksTreeView) Clear() {
	ltv.listStore.Clear()
}
