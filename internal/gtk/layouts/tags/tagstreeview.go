package tags

import (
	"github.com/diamondburned/gotk4/pkg/core/glib"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/chert/internal/core"
)

type TagManagement interface {
	TagSelected(tagName string, tagSource core.TagSource)
	RequestTagEmoji(tagName string, callback func(text string))
	RequestDeleteTag(tagName string) error
}

type TagsTreeView struct {
	scrolledWindow *gtk.ScrolledWindow
	*gtk.TreeView
	listStore *gtk.ListStore

	tagManagement TagManagement

	backingStore map[string]*core.Tag

	emojiRender *gtk.CellRendererText
}

func NewTagsTreeView(tagManagement TagManagement, conf *core.Config) *TagsTreeView {
	scrolledWindow := gtk.NewScrolledWindow()

	treeView := gtk.NewTreeView()
	scrolledWindow.SetChild(treeView)
	treeView.SetHExpand(true)
	treeView.SetVExpand(true)

	col := gtk.NewTreeViewColumn()
	col.SetTitle("Tags")

	textRender := gtk.NewCellRendererText()
	emojiRender := gtk.NewCellRendererText()
	emojiRender.SetObjectProperty("size", conf.EmojiSize*pango.SCALE)

	col.PackStart(emojiRender, false)
	col.AddAttribute(emojiRender, "text", 0)

	col.PackEnd(textRender, true)
	col.AddAttribute(textRender, "text", 1)

	treeView.AppendColumn(col)
	treeView.SetActivateOnSingleClick(true)

	// Add list
	listStore := gtk.NewListStore([]glib.Type{glib.TypeString, glib.TypeString})
	listStore.SetSortColumnID(1, gtk.SortAscending)

	treeView.SetModel(listStore)

	ttv := &TagsTreeView{
		scrolledWindow: scrolledWindow,
		TreeView:       treeView,
		listStore:      listStore,
		tagManagement:  tagManagement,
		backingStore:   make(map[string]*core.Tag),
		emojiRender:    emojiRender,
	}

	treeView.ConnectRowActivated(ttv.TagSelected)

	clickGesture := gtk.NewGestureClick()
	clickGesture.SetButton(gdk.BUTTON_SECONDARY)
	clickGesture.ConnectPressed(ttv.rightClick)
	treeView.AddController(clickGesture)

	return ttv
}

func (t *TagsTreeView) UnselectAll() {
	if sel := t.Selection(); sel != nil {
		sel.UnselectAll()
	}
}

func (t *TagsTreeView) TagSelected(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	tagName := t.getTagName(path, column)

	t.tagManagement.TagSelected(tagName, core.TagTree)
}

func (t *TagsTreeView) AddTag(tag *core.Tag) {
	t.backingStore[tag.Name] = tag
	iter := t.listStore.Append()
	t.listStore.Set(iter, []int{0, 1}, []glib.Value{*glib.NewValue(tag.Emoji), *glib.NewValue(tag.Name)})
}

func (t *TagsTreeView) Clear() {
	t.listStore.Clear()
}

func (t *TagsTreeView) GetView() gtk.Widgetter {
	return t.scrolledWindow
}

func (t *TagsTreeView) getTagName(path *gtk.TreePath, column *gtk.TreeViewColumn) string {
	iter, _ := t.listStore.Iter(path)
	valAtPath := t.listStore.Value(iter, 1)
	return valAtPath.String()
}

func (t *TagsTreeView) HandleEmojiSizeChanged(val int) {
	t.emojiRender.SetObjectProperty("size", val*pango.SCALE)
}

func (t *TagsTreeView) rightClick(nPress int, x, y float64) {
	path, col, _, _, _ := t.PathAtPos(t.ConvertWidgetToBinWindowCoords(int(x), int(y)))

	if path != nil {
		tagName := t.getTagName(path, col)

		rect := t.CellArea(path, col)
		popoverRect := gdk.NewRectangle(rect.X(), rect.Y()+35, rect.Width(), rect.Height())
		popover := gtk.NewPopover()
		popover.SetParent(t.TreeView)
		popover.SetPointingTo(&popoverRect)

		menuBox := gtk.NewBox(gtk.OrientationVertical, 10)

		requestEmojiBtn := gtk.NewButtonWithLabel("Choose Emjoi")
		requestEmojiBtn.ConnectClicked(func() {
			popover.Hide()
			t.tagManagement.RequestTagEmoji(tagName, func(emoji string) {
				if iter, ok := t.listStore.Iter(path); ok {
					t.listStore.Set(iter, []int{0}, []glib.Value{*glib.NewValue(emoji)})
				}
			})
		})
		menuBox.Append(requestEmojiBtn)

		sep := gtk.NewSeparator(gtk.OrientationHorizontal)
		menuBox.Append(sep)

		deleteBtn := gtk.NewButtonFromIconName("edit-delete")
		deleteBtn.ConnectClicked(func() {
			popover.Hide()
			t.tagManagement.RequestDeleteTag(tagName)
		})
		menuBox.Append(deleteBtn)

		popover.SetChild(menuBox)
		popover.Show()
	}
}
