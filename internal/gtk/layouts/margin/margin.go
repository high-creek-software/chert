package margin

type marginer interface {
	SetMarginStart(int)
	SetMarginTop(int)
	SetMarginEnd(int)
	SetMarginBottom(int)
}

func SetMarginAll(widget marginer, margin int) {
	widget.SetMarginStart(margin)
	widget.SetMarginTop(margin)
	widget.SetMarginEnd(margin)
	widget.SetMarginBottom(margin)
}
