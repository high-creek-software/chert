package editorcomponents

import (
	"bytes"
	"fmt"
	"github.com/client9/gospell"
	"github.com/diamondburned/gotk4-sourceview/pkg/gtksource/v5"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gdkpixbuf/v2"
	"github.com/diamondburned/gotk4/pkg/glib/v2"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"github.com/nfnt/resize"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/links"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/margin"
	"gitlab.com/high-creek-software/chert/internal/gtk/layouts/page"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"time"
	"unicode/utf8"
)

var linkLoadCoolDown = 2000 * time.Millisecond

type EditorManagement interface {
	ShowError(title string, err error)
	OpenExternalLink(link string)
	OpenInternalLink(relativePath string)
	EditInternalLink(relativePath string)
	TagSelected(tagName string, tagSource core.TagSource)
	ToggleMetaDataExpander(relativePath string, isOpen bool)
	ChooseImage(result func(path string))
	ExportPDF(page *core.Page)
}

type EditorComponents struct {
	conf           *core.Config
	registry       *core.Registry
	content        *gtksource.View
	buffer         *gtksource.Buffer
	scrolledWindow *gtk.ScrolledWindow

	errorTag *gtk.TextTag
	linkTag  *gtk.TextTag
	h1Tag    *gtk.TextTag
	h2Tag    *gtk.TextTag

	selectedPage *core.Page

	box           *gtk.Box
	action        *gtk.Box
	imageButton   *gtk.Button
	progressBar   *gtk.ProgressBar
	saveButton    *gtk.Button
	spacerLabel   *gtk.Label
	openInBrowser *gtk.Button
	exportPDF     *gtk.Button

	lastLinkInspection *time.Time
	linkLocations      []LinkLocation
	inspectionTimer    *time.Timer

	backlinks *links.LinksTreeView

	spelling *gospell.GoSpell

	metaBox      *gtk.Box
	metaExpander *gtk.Expander
	pageTags     *page.PageTags

	editorManagement EditorManagement

	bufferDirty bool
	DirtyLbl    *gtk.Label

	logger *core.Logger
}

func NewEditorComponents(registry *core.Registry, conf *core.Config, logger *core.Logger, editorManagement EditorManagement, language *gtksource.Language, scheme *gtksource.StyleScheme,
	selectedPage *core.Page, spelling *gospell.GoSpell, dirtyLabel *gtk.Label, fontDesc *pango.FontDescription) *EditorComponents {
	ec := &EditorComponents{
		conf:             conf,
		logger:           logger,
		registry:         registry,
		selectedPage:     selectedPage,
		spelling:         spelling,
		editorManagement: editorManagement,
		DirtyLbl:         dirtyLabel,
	}

	// https://en.wikipedia.org/wiki/X11_color_names#Clashes_between_web_and_X11_colors
	ec.errorTag = gtk.NewTextTag("error")
	ec.errorTag.SetObjectProperty("underline", pango.UnderlineError)

	ec.linkTag = gtk.NewTextTag("link")
	ec.linkTag.SetObjectProperty("underline", pango.UnderlineDouble)
	ec.linkTag.SetObjectProperty("foreground", conf.LinkColor)

	// TODO: These sizes will be out of sync of the selected font, I need to pass a font in here.
	ec.h1Tag = gtk.NewTextTag("h1")
	ec.h1Tag.SetObjectProperty("size", 15*pango.SCALE)

	ec.h2Tag = gtk.NewTextTag("h2")
	ec.h2Tag.SetObjectProperty("size", 14*pango.SCALE)

	if fontDesc != nil {
		ec.h1Tag.SetObjectProperty("size", fontDesc.Size()+(3*pango.SCALE))
		ec.h2Tag.SetObjectProperty("size", fontDesc.Size()+(2*pango.SCALE))
	}

	ec.buffer = gtksource.NewBufferWithLanguage(language)
	ec.buffer.SetStyleScheme(scheme)
	ec.content = gtksource.NewViewWithBuffer(ec.buffer)
	ec.content.SetMarginStart(10)
	ec.content.SetMarginEnd(10)
	ec.content.SetName("contentview")
	ec.content.SetShowLineNumbers(conf.ShowLineNumbers)
	ec.content.SetWrapMode(gtk.WrapWord)
	ec.content.SetTabWidth(uint(conf.TabWidth))
	ec.content.SetHighlightCurrentLine(conf.HighlightCurrentLine)
	//ec.content.SetAutoIndent(conf.AutoIndent)

	saveTrigger := gtk.NewShortcutTriggerParseString("<Control>s")
	saveAction := gtk.NewCallbackAction(func(widget gtk.Widgetter, args *glib.Variant) bool {
		ec.saveActivated()

		return true
	})
	saveShortcut := gtk.NewShortcut(saveTrigger, saveAction)
	saveController := gtk.NewShortcutController()
	saveController.AddShortcut(saveShortcut)
	ec.content.AddController(saveController)

	ec.buffer.ConnectPasteDone(func(clipboard *gdk.Clipboard) {
		ec.spellCheckFull()
		//txt := ec.buffer.Text(ec.buffer.StartIter(), ec.buffer.EndIter(), false)
		//ec.extractLinks(txt)
	})

	ec.buffer.TagTable().Add(ec.errorTag)
	ec.buffer.TagTable().Add(ec.linkTag)
	ec.buffer.TagTable().Add(ec.h2Tag)
	ec.buffer.TagTable().Add(ec.h1Tag)

	ec.errorTag.SetPriority(0)
	ec.linkTag.SetPriority(1)
	ec.h2Tag.SetPriority(2)
	ec.h1Tag.SetPriority(3)

	clickController := gtk.NewGestureClick()
	clickController.SetButton(gdk.BUTTON_PRIMARY)
	clickController.ConnectPressed(func(nPress int, x, y float64) {
		bufX, bufY := ec.content.WindowToBufferCoords(gtk.TextWindowText, int(x), int(y))
		iter, ok := ec.content.IterAtLocation(bufX, bufY)
		if !ok {
			return
		}

		for _, ll := range ec.linkLocations {
			if iter.Offset() >= ll.Start && iter.Offset() <= ll.End {
				switch ll.LinkType {
				case LinkExternal:
					ec.handleExternalLinkSelection(int(x), int(y), ll)
				case LinkInternal:
					ec.handleInternalLinkSelection(int(x), int(y), ll)
				case LinkImage:
					ec.handleImageSelection(int(x), int(y), ll)
				}

			}
		}

	})
	ec.content.AddController(clickController)

	ec.scrolledWindow = gtk.NewScrolledWindow()
	ec.scrolledWindow.SetChild(ec.content)
	ec.scrolledWindow.SetVExpand(true)

	ec.box = gtk.NewBox(gtk.OrientationVertical, 0)

	ec.action = gtk.NewBox(gtk.OrientationHorizontal, 0)
	ec.action.SetHExpand(true)

	ec.progressBar = gtk.NewProgressBar()
	ec.progressBar.SetVAlign(gtk.AlignCenter)
	ec.progressBar.Hide()
	ec.action.Prepend(ec.progressBar)

	ec.saveButton = gtk.NewButtonFromIconName("document-save")
	ec.saveButton.SetVAlign(gtk.AlignCenter)
	ec.saveButton.ConnectClicked(ec.saveActivated)
	margin.SetMarginAll(ec.saveButton, 10)
	ec.action.Prepend(ec.saveButton)

	ec.imageButton = gtk.NewButtonFromIconName("insert-image")
	ec.imageButton.SetVAlign(gtk.AlignCenter)
	ec.imageButton.ConnectClicked(ec.addImage)
	margin.SetMarginAll(ec.imageButton, 10)
	ec.action.Prepend(ec.imageButton)

	ec.spacerLabel = gtk.NewLabel("")
	ec.spacerLabel.SetHExpand(true)
	ec.action.Append(ec.spacerLabel)

	ec.box.Append(ec.action)
	ec.box.Append(ec.scrolledWindow)

	ec.openInBrowser = gtk.NewButtonWithLabel("Open in browser")
	ec.openInBrowser.SetVAlign(gtk.AlignCenter)
	ec.openInBrowser.SetHAlign(gtk.AlignEnd)
	ec.openInBrowser.SetMarginEnd(10)
	ec.openInBrowser.ConnectClicked(ec.doOpenInBrowser)
	ec.action.Append(ec.openInBrowser)

	ec.exportPDF = gtk.NewButtonWithLabel("Export as PDF")
	ec.exportPDF.SetVAlign(gtk.AlignCenter)
	ec.exportPDF.SetHAlign(gtk.AlignEnd)
	ec.exportPDF.SetMarginEnd(10)
	ec.exportPDF.ConnectClicked(func() {
		ec.editorManagement.ExportPDF(ec.selectedPage)
	})
	ec.action.Append(ec.exportPDF)

	ec.metaBox = gtk.NewBox(gtk.OrientationHorizontal, 0)
	ec.metaBox.SetVExpandSet(true)
	ec.metaBox.SetSizeRequest(-1, 250)
	ec.metaBox.SetMarginTop(10)

	ec.pageTags = page.NewPageTags(ec, ec.conf)
	ec.metaBox.Append(ec.pageTags.GetView())

	ec.backlinks = links.NewLinksTreeView(editorManagement)
	ec.metaBox.Append(ec.backlinks.GetView())

	ec.metaExpander = gtk.NewExpander("Page meta data")
	ec.metaExpander.SetChild(ec.metaBox)
	margin.SetMarginAll(ec.metaExpander, 10)

	ec.box.Append(ec.metaExpander)

	ec.metaExpander.ConnectActivate(func() {
		ec.editorManagement.ToggleMetaDataExpander(ec.selectedPage.RelativePath, !ec.metaExpander.Expanded())
	})
	ec.metaExpander.SetExpanded(!conf.HidePageManagement)

	term := fmt.Sprintf("[[%s]]", ec.selectedPage.RelativePath)
	indexedPages := ec.registry.FindBacklinks(term)
	for _, indexedPage := range indexedPages {
		ec.backlinks.AddLinked(indexedPage)
	}

	if f, err := ec.registry.ReadPage(ec.selectedPage); err == nil {
		ec.buffer.SetText(f)
		ec.spellCheckFull()
		ec.extractLinks(ec.buffer.StartIter())
		if conf.ResizeTitleTags {
			ec.extractHeadings(ec.buffer.StartIter())
		}
	} else {
		ec.editorManagement.ShowError("Could not open page", err)
	}

	if allTags := ec.registry.GetTags(); allTags != nil {
		for _, tag := range allTags {
			ec.pageTags.AddTag(tag)
		}
	}

	if tags, err := ec.registry.FindPageTags(ec.selectedPage); err == nil {
		for _, tag := range tags {
			ec.pageTags.AddPageTag(tag)
		}
	}

	ec.buffer.ConnectChanged(ec.bufferChanged)

	return ec
}

func (ec *EditorComponents) bufferChanged() {
	// There is some state management that I want to work with:
	// 1. Autosaving of the buffer -> Every N seconds after opening a new page
	//		- Restart with new page
	//		- Only save if buffer is dirty
	// 2. Inspecting the markdown document to parse out:
	//		- Content hierarchy
	//		- Any links internal/external
	//		- Spelling corrections
	if ec.selectedPage == nil {
		return
	}
	start := time.Now()
	defer func() {
		ec.logger.Debug("Total buffer callback duration:", time.Now().Sub(start))
	}()

	// Cancelling the inspection timer here, so that only the last change that is queued up will actually run.
	if ec.inspectionTimer != nil {
		ec.inspectionTimer.Stop()
	}

	insertMark := ec.buffer.GetInsert()
	if insertMark != nil {
		// Cursor position here?
		startIter := ec.buffer.IterAtMark(insertMark)
		startIter.BackwardLines(4)
		ec.inspectionTimer = time.NewTimer(500 * time.Millisecond)

		go func(iter *gtk.TextIter) {
			<-ec.inspectionTimer.C
			// Extracting wikilinks and URLS
			// We grab the text for the latest iteration, and pass it into the goroutine, but we won't actually work off of it until
			// there is a 500 ms delay in typing (this is guessing that the person typing has taken a break).
			glib.IdleAdd(func() {
				ec.buffer.RemoveTagByName("link", iter, ec.buffer.EndIter())
			})
			ec.extractLinks(iter)

			if ec.conf.ResizeTitleTags {
				glib.IdleAdd(func() {
					ec.RemoveTagTags(iter)
				})
				ec.extractHeadings(iter)
			}
		}(startIter)

		inspectionIter := ec.buffer.IterAtMark(insertMark)
		if inspectionIter.StartsLine() { /** Check previous line here, for autoindent **/
			if ec.conf.AutoIndent {
				ec.handleAutoIndent(insertMark)
			}
		} else { /*** Running at cursor spell check ***/
			inspectionIter.BackwardWordStart()
			inspectionIter.ForwardWordEnd()

			wordStartIter := ec.buffer.IterAtMark(insertMark)
			wordStartIter.BackwardWordStart()

			ec.buffer.RemoveTag(ec.errorTag, wordStartIter, inspectionIter)

			word := ec.buffer.Text(wordStartIter, inspectionIter, false)
			if ec.spelling != nil && !ec.spelling.Spell(word) {
				ec.buffer.ApplyTag(ec.errorTag, wordStartIter, inspectionIter)
			}
		}
	}

	ec.bufferDirty = true
	ec.DirtyLbl.SetText("🔴")
}

func (ec *EditorComponents) GetView() gtk.Widgetter {
	return ec.box
}

func (ec *EditorComponents) handleAutoIndent(insertMark *gtk.TextMark) {
	previousLineIter := ec.buffer.IterAtMark(insertMark)
	previousLineIter.BackwardLine()

	previousLineEndIter := ec.buffer.IterAtMark(insertMark)
	previousLineEndIter.BackwardLine()
	previousLineEndIter.ForwardToLineEnd()

	line := ec.buffer.Text(previousLineIter, previousLineEndIter, false)
	log.Println("Previous Line:", line)
	if core.TaskItemRegex.MatchString(line) {
		log.Println("Matched task item")
	} else if core.ListItemRegex.MatchString(line) {
		log.Println("Matched list item")
		matches := core.ListItemRegex.FindStringSubmatch(line)
		prefix := matches[core.ListItemRegex.SubexpIndex("prefix")]
		content := matches[core.ListItemRegex.SubexpIndex("content")]

		if content != "" {
			ec.buffer.Insert(ec.buffer.IterAtMark(insertMark), prefix+" ")
		} else {
			ec.buffer.Delete(previousLineIter, previousLineEndIter)
		}
	}
}

func (ec *EditorComponents) RemoveTagTags(startIter *gtk.TextIter) {
	ec.buffer.RemoveTagByName("h1", startIter, ec.buffer.EndIter())
	ec.buffer.RemoveTagByName("h2", startIter, ec.buffer.EndIter())
}

func (ec *EditorComponents) extractLinks(startIter *gtk.TextIter) {
	defer func() {
		if err := recover(); err != nil {
			ec.logger.Error("Error from the link inspection goroutine:", err)
		}
	}()
	ec.logger.Debug("Starting link extraction at:", startIter.Offset())
	lastLink := time.Now()
	ec.lastLinkInspection = &lastLink

	// Remove any links forward of start iter:
	ec.logger.Debug(ec.linkLocations)
	var removeLinks []int
	for idx, ll := range ec.linkLocations {
		if ll.Start >= startIter.Offset() {
			removeLinks = append(removeLinks, idx)
		}
	}
	for i := len(removeLinks) - 1; i >= 0; i-- {
		idx := removeLinks[i]
		ec.linkLocations = slices.Delete(ec.linkLocations, idx, idx+1)
	}
	ec.logger.Debug(ec.linkLocations)

	txt := ec.buffer.Text(startIter, ec.buffer.EndIter(), true)
	ec.logger.Debug("Extracting links from:", txt)

	wikilinks := core.WikilinkRegex.FindAllString(txt, -1)
	autoLinks := core.Link.FindAllString(txt, -1)
	wikilinks = append(wikilinks, autoLinks...)
	combinedLinks := core.CombinedLinks.FindAllStringSubmatch(txt, -1)

	for _, link := range combinedLinks {
		urlIdx := core.CombinedLinks.SubexpIndex("link")
		url := link[urlIdx]
		if url != "" {
			if strings.HasPrefix(link[0], "!") {
				wikilinks = append(wikilinks, "!:"+url)
			} else {
				wikilinks = append(wikilinks, url)
			}
		}
	}

	for _, url := range wikilinks {
		lt := LinkExternal
		if strings.Contains(url, "[[") {
			url = strings.TrimPrefix(url, "[[")
			url = strings.TrimSuffix(url, "]]")
			lt = LinkInternal
		}
		if strings.HasPrefix(url, "<") {
			url = strings.TrimPrefix(url, "<")
			url = strings.TrimSuffix(url, ">")
		}
		if strings.HasPrefix(url, "!:") {
			url = strings.TrimPrefix(url, "!:")
			lt = LinkImage
		}
		byteIndex := strings.Index(txt, url)
		idx := utf8.RuneCountInString(txt[:byteIndex])
		length := utf8.RuneCountInString(url)

		wordStartIter := ec.buffer.IterAtOffset(startIter.Offset() + idx)
		endIter := ec.buffer.IterAtOffset(startIter.Offset() + idx + length)
		// Running this on the main thread.
		func(si, ei *gtk.TextIter) {
			glib.IdleAdd(func() {
				ec.buffer.RemoveTagByName("error", si, ei)
				ec.buffer.ApplyTag(ec.linkTag, si, ei)
			})
		}(wordStartIter, endIter)
		ec.linkLocations = append(ec.linkLocations, LinkLocation{Start: wordStartIter.Offset(), End: endIter.Offset(), Link: url, LinkType: lt})
	}
	ec.logger.Debug(ec.linkLocations)
	ec.logger.Info("Link extraction;", time.Now().Sub(lastLink))
}

func (ec *EditorComponents) extractHeadings(startIter *gtk.TextIter) {
	start := time.Now()
	txt := ec.buffer.Text(startIter, ec.buffer.EndIter(), true)
	ec.logger.Debug("Extracting headings:", txt)
	h1Match := core.H1Regex.FindAllString(txt, -1)
	for _, match := range h1Match {
		byteIndex := strings.Index(txt, match)
		idx := utf8.RuneCountInString(txt[:byteIndex])
		length := utf8.RuneCountInString(match)

		wordStartIter := ec.buffer.IterAtOffset(startIter.Offset() + idx)
		endIter := ec.buffer.IterAtOffset(startIter.Offset() + idx + length)
		func(si, ei *gtk.TextIter) {
			glib.IdleAdd(func() {
				ec.buffer.ApplyTag(ec.h1Tag, si, ei)
			})
		}(wordStartIter, endIter)
	}

	h2Match := core.H2Regex.FindAllString(txt, -1)
	for _, match := range h2Match {
		byteIndex := strings.Index(txt, match)
		idx := utf8.RuneCountInString(txt[:byteIndex])
		length := utf8.RuneCountInString(match)

		wordStartIter := ec.buffer.IterAtOffset(startIter.Offset() + idx)
		endIter := ec.buffer.IterAtOffset(startIter.Offset() + idx + length)
		func(si, ei *gtk.TextIter) {
			glib.IdleAdd(func() {
				ec.buffer.ApplyTag(ec.h2Tag, si, ei)
			})
		}(wordStartIter, endIter)
	}

	ec.logger.Info("Tag Resize:", time.Now().Sub(start))
}

func (ec *EditorComponents) spellCheckFull() {
	start := time.Now()

	endIter := ec.buffer.StartIter()
	startIter := ec.buffer.StartIter()
	startIter.ForwardWordEnd()
	startIter.BackwardWordStart()

	ec.buffer.RemoveTag(ec.errorTag, ec.buffer.StartIter(), ec.buffer.EndIter())
	for {
		moved := endIter.ForwardWordEnd()
		if !moved {
			break
		}
		word := ec.buffer.Text(startIter, endIter, false)

		if ec.spelling != nil && !ec.spelling.Spell(word) {
			ec.buffer.ApplyTag(ec.errorTag, startIter, endIter)
		}

		startIter.ForwardWordEnd()
		startIter.ForwardWordEnd()
		startIter.BackwardWordStart()
	}

	ec.logger.Info("Spell check duration:", time.Now().Sub(start))
}

func (ec *EditorComponents) handleExternalLinkSelection(bufX, bufY int, ll LinkLocation) {
	popover := gtk.NewPopover()
	rect := gdk.NewRectangle(bufX, bufY+10, 1, 1)
	popover.SetParent(ec.content)
	popover.SetPointingTo(&rect)

	openBtn := gtk.NewButtonWithLabel("Open in browser")
	openBtn.ConnectClicked(func() {
		popover.Hide()
		ec.editorManagement.OpenExternalLink(ll.Link)
	})

	margin.SetMarginAll(openBtn, 10)

	popover.SetChild(openBtn)
	popover.Popup()
}

func (ec *EditorComponents) handleInternalLinkSelection(bufX, bufY int, ll LinkLocation) {
	popover := gtk.NewPopover()
	rect := gdk.NewRectangle(bufX, bufY+10, 1, 1)
	popover.SetParent(ec.content)
	popover.SetPointingTo(&rect)

	box := gtk.NewBox(gtk.OrientationVertical, 10)

	openEditor := gtk.NewButtonWithLabel("Open in editor")
	openEditor.ConnectClicked(func() {
		popover.Hide()
		ec.editorManagement.EditInternalLink(ll.Link)
	})
	margin.SetMarginAll(openEditor, 10)
	box.Append(openEditor)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	box.Append(sep)

	openBrowser := gtk.NewButtonWithLabel("Open in browser")
	openBrowser.ConnectClicked(func() {
		popover.Hide()
		ec.editorManagement.OpenInternalLink(ll.Link)
	})
	margin.SetMarginAll(openBrowser, 10)
	box.Append(openBrowser)

	popover.SetChild(box)
	popover.Popup()
}

func (ec *EditorComponents) handleImageSelection(bufX, bufY int, ll LinkLocation) {
	popover := gtk.NewPopover()
	rect := gdk.NewRectangle(bufX, bufY+10, 1, 1)
	popover.SetParent(ec.content)
	popover.SetPointingTo(&rect)

	path := gtk.NewLabel(ll.Link)

	box := gtk.NewBox(gtk.OrientationVertical, 0)
	box.Append(path)

	input, err := os.Open(strings.Replace(ec.selectedPage.Path, ec.selectedPage.Name, ll.Link, 1))
	if err != nil {
		ec.editorManagement.ShowError("Could not open file", err)
		return
	}
	defer input.Close()

	img, typ, imgErr := image.Decode(input)
	if imgErr != nil {
		ec.editorManagement.ShowError("Could not read image", imgErr)
		return
	}

	out := resize.Resize(1024, 0, img, resize.Lanczos3)

	width := out.Bounds().Dx()
	height := out.Bounds().Dy()

	var bs bytes.Buffer
	switch typ {
	case "jpeg":
		jpeg.Encode(&bs, out, nil)
	case "png":
		png.Encode(&bs, out)
	}

	loader := gdkpixbuf.NewPixbufLoader()
	loader.SetSize(width, height)
	loader.WriteBytes(glib.NewBytes(bs.Bytes()))
	loader.Close()

	display := gtk.NewImageFromPixbuf(loader.Pixbuf())
	display.SetSizeRequest(width, height)
	box.Append(display)

	popover.SetChild(box)
	popover.Popup()
}

func (ec *EditorComponents) SetStyleScheme(sel *gtksource.StyleScheme) {
	ec.buffer.SetStyleScheme(sel)
}

func (ec *EditorComponents) saveActivated() {
	if !ec.bufferDirty {
		return
	}
	ec.progressBar.Show()
	ec.progressBar.Pulse()

	go func() {
		defer glib.IdleAdd(func() {
			ec.progressBar.Hide()
			ec.progressBar.SetFraction(0)
		})
		update := time.NewTicker(20 * time.Millisecond)
		end := time.NewTimer(300 * time.Millisecond)
		defer update.Stop()
		defer end.Stop()

		for {
			select {
			case <-update.C:
				glib.IdleAdd(func() {
					ec.progressBar.Pulse()
				})
			case <-end.C:
				return
			}
		}
	}()

	ec.logger.Info("Starting save...")

	text := ec.buffer.Text(ec.buffer.StartIter(), ec.buffer.EndIter(), false)
	err := ec.registry.SavePage(ec.selectedPage, text)
	if err != nil {
		ec.editorManagement.ShowError("Error saving", err)
		return
	}
	ec.bufferDirty = false
	ec.DirtyLbl.SetText("")
	ec.logger.Info("Ending save...")
}

func (ec *EditorComponents) addImage() {
	ec.editorManagement.ChooseImage(func(path string) {
		base := filepath.Base(path)
		page := filepath.Base(ec.selectedPage.Path)

		destPath := strings.Replace(ec.selectedPage.Path, page, base, 1)

		in, err := os.Open(path)
		if err != nil {
			ec.editorManagement.ShowError("Could not open file", err)
			return
		}
		defer in.Close()

		out, oErr := os.Create(destPath)
		if oErr != nil {
			ec.editorManagement.ShowError("Could not create file", oErr)
			return
		}
		defer out.Close()

		_, copyErr := io.Copy(out, in)
		if copyErr != nil {
			ec.editorManagement.ShowError("Error copying image", copyErr)
			return
		}

		ec.buffer.InsertAtCursor(fmt.Sprintf("![](%s)", base))
	})
}

func (ec *EditorComponents) doOpenInBrowser() {
	ec.editorManagement.OpenInternalLink(ec.selectedPage.RelativePath)
}

func (ec *EditorComponents) AddTag(tag *core.Tag) {
	ec.pageTags.AddTag(tag)
}

func (ec *EditorComponents) UnselectAllTags() {
	ec.pageTags.UnselectAll()
}

func (ec *EditorComponents) SetTagFontSize(size int) {
	//ec.errorTag.SetObjectProperty("size", size)
	//ec.linkTag.SetObjectProperty("size", size)
	ec.h1Tag.SetObjectProperty("size", size+(3*pango.SCALE))
	ec.h2Tag.SetObjectProperty("size", size+(2*pango.SCALE))
}

func (ec *EditorComponents) AddTagToActivePage(tagName string) error {
	err := ec.registry.AddTagToPage(ec.selectedPage, tagName)
	if err != nil {
		ec.editorManagement.ShowError("Could not add tag to page", err)
	}
	return err
}

func (ec *EditorComponents) RemoveTagFromActivePage(tagName string) error {
	err := ec.registry.RemoveTagFromPage(ec.selectedPage, tagName)
	if err != nil {
		ec.editorManagement.ShowError("Could not remove tag", err)
	}
	return err
}

func (ec *EditorComponents) TagSelected(tagName string, tagSource core.TagSource) {
	ec.editorManagement.TagSelected(tagName, tagSource)
}

func (ec *EditorComponents) ToggleMeta(isOpen bool) {
	ec.metaExpander.SetExpanded(isOpen)
}

func (ec *EditorComponents) HandleResizeTagsChange(resize bool) {
	if resize {
		ec.extractHeadings(ec.buffer.StartIter())
	} else {
		ec.RemoveTagTags(ec.buffer.StartIter())
	}
}

func (ec *EditorComponents) HandleHighlightRow(highlight bool) {
	ec.content.SetHighlightCurrentLine(highlight)
}

func (ec *EditorComponents) HandleShowLineNumbersChange(show bool) {
	ec.content.SetShowLineNumbers(show)
}

func (ec *EditorComponents) HandleAutoIndentChanged(indent bool) {
	//ec.content.SetAutoIndent(indent)
}

func (ec *EditorComponents) HandleLinkColorChanged(color string) {
	ec.linkTag.SetObjectProperty("foreground", ec.conf.LinkColor)
}

func (ec *EditorComponents) HandleTabSpaceChanged(val int) {
	ec.content.SetTabWidth(uint(val))
}

func (ec *EditorComponents) HandleEmojiSizeChanged(val int) {
	ec.pageTags.HandleEmojiSizeChanged(val)
}

type LinkLocation struct {
	Start    int
	End      int
	Link     string
	LinkType LinkType
}

type LinkType int

const (
	LinkExternal LinkType = iota + 1
	LinkInternal
	LinkImage
)
