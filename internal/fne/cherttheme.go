package fne

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
	"image/color"
)

type ChertTheme struct {
}

func (c ChertTheme) Color(name fyne.ThemeColorName, variant fyne.ThemeVariant) color.Color {
	switch name {
	case theme.ColorNameBackground:
		//if variant == theme.VariantDark {
		//	return colornames.Gray
		//}
	}
	return theme.DefaultTheme().Color(name, variant)
}

func (c ChertTheme) Font(style fyne.TextStyle) fyne.Resource {
	return theme.DefaultTheme().Font(style)
}

func (c ChertTheme) Icon(name fyne.ThemeIconName) fyne.Resource {
	return theme.DefaultTheme().Icon(name)
}

func (c ChertTheme) Size(name fyne.ThemeSizeName) float32 {
	switch name {
	//case theme.SizeNameText:
	//	return 11
	//case theme.SizeNamePadding:
	//	return 3
	case theme.SizeNameScrollBar:
		return 2 * theme.DefaultTheme().Size(theme.SizeNameScrollBarSmall)
	case theme.SizeNameSeparatorThickness:
		return 1
	default:
		return theme.DefaultTheme().Size(name)
	}
}
