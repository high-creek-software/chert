package layouts

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"net/url"
)

var iconLinks = []string{"https://icons8.com/icons/collections/JOhC0FtZb8bC",
	"https://icons8.com/icons/collections/Qm2nuv4OPCbq",
	"https://icons8.com/icons/collections/e2VdJpGv57iZ",
	"https://icons8.com/icons/collections/vlYmrZVMBkqx",
	"https://icons8.com/icons/collections/cApEHOokrZgB",
	"https://icons8.com/icons/collections/GDEh2cEvySzG",
}

const (
	aboutText = `Chert is more than a note taking app. It is a knowledge graph building software.

Build up a set of notebooks that have common information. Then tag your pages that share common content.`
)

type AboutLayout struct {
	widget.BaseWidget

	nameTitle *widget.RichText
	iconImg   *canvas.Image
	aboutBody *widget.Label
}

func (a *AboutLayout) CreateRenderer() fyne.WidgetRenderer {
	a.nameTitle = widget.NewRichTextFromMarkdown("# Chert")
	a.iconImg = canvas.NewImageFromResource(fyne.NewStaticResource("icon", core.IconBytes))
	a.iconImg.SetMinSize(fyne.NewSize(256, 256))
	a.iconImg.FillMode = canvas.ImageFillContain
	a.aboutBody = widget.NewLabel(aboutText)
	a.aboutBody.Alignment = fyne.TextAlignCenter

	iconsBox := container.NewVBox()
	for _, link := range iconLinks {
		if l, err := url.Parse(link); err == nil {
			iconsBox.Add(widget.NewHyperlink(link, l))
		}
	}

	topBorder := container.NewBorder(container.NewGridWithColumns(3, layout.NewSpacer(), a.nameTitle, layout.NewSpacer()), nil, nil, nil, a.iconImg)
	aboutBorder := container.NewBorder(topBorder, nil, nil, nil, a.aboutBody)
	linksBorder := container.NewBorder(widget.NewRichTextFromMarkdown("## Icons by icons8"), nil, nil, nil, iconsBox)

	mainBorder := container.NewBorder(aboutBorder, nil, nil, nil, container.NewGridWithColumns(3, layout.NewSpacer(), linksBorder, layout.NewSpacer()))

	return widget.NewSimpleRenderer(container.NewScroll(container.NewPadded(mainBorder)))

}

func NewAboutLayout() *AboutLayout {
	al := &AboutLayout{}
	al.ExtendBaseWidget(al)

	return al
}
