package layouts

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/adapters"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
)

type SearchLayout struct {
	widget.BaseWidget

	interactor wids.IndexPageInteractor

	term    string
	termLbl *widget.RichText

	ips              []core.IndexPage
	indexPageAdapter *adapters.IndexPageAdapter
	indexPageList    *widget.List
}

func (s *SearchLayout) CreateRenderer() fyne.WidgetRenderer {

	s.termLbl = widget.NewRichTextFromMarkdown(fmt.Sprintf("# %s", s.term))
	s.indexPageAdapter = adapters.NewIndexPageAdapter(s.ips, s.interactor)
	s.indexPageList = widget.NewList(s.indexPageAdapter.Count, s.indexPageAdapter.CreateTemplate, s.indexPageAdapter.Bind)
	s.indexPageAdapter.SetList(s.indexPageList)

	return widget.NewSimpleRenderer(container.NewBorder(s.termLbl, nil, nil, nil, s.indexPageList))
}

func NewSearchLayout(interactor wids.IndexPageInteractor, term string, ips []core.IndexPage) *SearchLayout {
	sl := &SearchLayout{interactor: interactor, term: term, ips: ips}
	sl.ExtendBaseWidget(sl)

	return sl
}
