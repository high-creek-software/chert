package layouts

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/adapters"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type EventInteractor interface {
	OpenInBrowser(p *core.Page)
	ShowError(err error)
	PageTagAction(p *core.Page, t *core.Tag, editor *EditorLayout, e *fyne.PointEvent)
	UpdateTagPages(tag *core.Tag)
	OpenInternalLink(link string)
	ChooseImage(result func(path string))
}

type EditorLayout struct {
	widget.BaseWidget

	interactor EventInteractor

	registry     *core.Registry
	selectedPage *core.Page
	data         string

	content       *wids.ChertEntry
	imageBtn      *widget.Button
	saveBtn       *widget.Button
	openBtn       *widget.Button
	saveIndicator *widget.ProgressBarInfinite

	tagAdapter          *adapters.TagAdapter
	tagList             *widget.List
	tagNames            []string
	availableTags       *widget.Select
	internalLinkAdapter *adapters.StringAdapter
	internalList        *widget.List
}

func (e *EditorLayout) CreateRenderer() fyne.WidgetRenderer {
	e.content = wids.NewChertEntry(e.saveAction)
	e.content.MultiLine = true
	e.content.Wrapping = fyne.TextWrapWord
	e.content.TextStyle = fyne.TextStyle{Monospace: true}
	e.content.SetText(e.data)

	e.imageBtn = widget.NewButtonWithIcon("Add image", theme.FileImageIcon(), e.chooseImage)
	e.saveBtn = widget.NewButtonWithIcon("Save", theme.DocumentSaveIcon(), e.saveAction)
	e.openBtn = widget.NewButtonWithIcon("Open in browser", theme.FolderOpenIcon(), func() {
		e.interactor.OpenInBrowser(e.selectedPage)
	})
	e.saveIndicator = widget.NewProgressBarInfinite()
	e.saveIndicator.Hide()

	tagTitle := widget.NewRichTextFromMarkdown("## Page Tags")
	internalLinksTitle := widget.NewRichTextFromMarkdown("## Internal Links")
	//externalLinksTitle := widget.NewRichTextFromMarkdown("## External Links")

	tgs, _ := e.registry.FindPageTags(e.selectedPage)
	e.tagAdapter = adapters.NewTagAdapter(tgs, func(t *core.Tag, pe *fyne.PointEvent) {
		e.interactor.PageTagAction(e.selectedPage, t, e, pe)
	})
	e.tagList = widget.NewList(e.tagAdapter.Count, e.tagAdapter.CreateTemplate, e.tagAdapter.Bind)
	e.tagList.OnSelected = func(id widget.ListItemID) {
		tag := e.tagAdapter.GetTag(id)
		e.interactor.UpdateTagPages(tag)
	}
	allTags := e.registry.GetTags()
	for _, t := range allTags {
		e.tagNames = append(e.tagNames, t.Name)
	}
	e.availableTags = widget.NewSelect(e.tagNames, e.tagSelected)
	e.availableTags.PlaceHolder = "(Add tag)"

	e.internalLinkAdapter = adapters.NewStringAdapter(e.selectedPage.LinkedPages)
	e.internalList = widget.NewList(e.internalLinkAdapter.Count, e.internalLinkAdapter.CreateTemplate, e.internalLinkAdapter.Bind)
	e.internalList.OnSelected = func(id widget.ListItemID) {
		link := e.internalLinkAdapter.GetItem(id)
		e.interactor.OpenInternalLink(link)
	}

	sr := wids.NewSizer(250)

	bottomBox := container.NewGridWithColumns(2,
		container.NewPadded(container.NewBorder(container.NewBorder(tagTitle, nil, nil, nil, e.availableTags), nil, nil, nil, e.tagList)),
		container.NewPadded(container.NewBorder(internalLinksTitle, nil, nil, nil, container.NewMax(sr, e.internalList))),
	)
	accordion := widget.NewAccordion(widget.NewAccordionItem("Metadata", bottomBox))

	//grid := widget.NewTextGridFromString(e.data)
	//grid.ShowLineNumbers = true
	//grid.ShowWhitespace = true

	btnBar := container.NewHBox(e.imageBtn, e.saveBtn, e.openBtn)
	actionBorder := container.NewBorder(nil, nil, btnBar, nil, e.saveIndicator)
	return widget.NewSimpleRenderer(container.NewPadded(
		container.NewBorder(
			//container.NewBorder(btnBar, nil, nil, nil, e.saveIndicator),
			actionBorder,
			accordion,
			nil,
			nil,
			e.content,
			//container.NewGridWithRows(2, e.content, grid),
		),
	))
}

func (e *EditorLayout) UpdateTags() {
	tgs, _ := e.registry.FindPageTags(e.selectedPage)
	e.tagAdapter.UpdateTags(tgs)
	e.tagList.Refresh()
}

func (e *EditorLayout) saveAction() {
	e.saveIndicator.Show()
	hideTimer := time.NewTimer(500 * time.Millisecond)
	go func() {
		<-hideTimer.C
		e.saveIndicator.Hide()
	}()
	text := e.content.Text
	err := e.registry.SavePage(e.selectedPage, text)
	if err != nil {
		e.interactor.ShowError(err)
	}
}

func (e *EditorLayout) chooseImage() {
	e.interactor.ChooseImage(func(path string) {
		base := filepath.Base(path)
		page := filepath.Base(e.selectedPage.Path)

		destPath := strings.Replace(e.selectedPage.Path, page, base, 1)

		in, err := os.Open(path)
		if err != nil {
			e.interactor.ShowError(err)
			return
		}
		defer in.Close()

		out, oErr := os.Create(destPath)
		if oErr != nil {
			e.interactor.ShowError(oErr)
			return
		}
		defer out.Close()

		_, copyErr := io.Copy(out, in)
		if copyErr != nil {
			e.interactor.ShowError(copyErr)
			return
		}

		data := e.content.Text
		data += "\n"
		data += fmt.Sprintf("![](%s)", base)
		e.content.SetText(data)
	})
}

func (e *EditorLayout) tagSelected(sel string) {
	e.registry.AddTagToPage(e.selectedPage, sel)
	pgTgs, _ := e.registry.FindPageTags(e.selectedPage)
	e.tagAdapter.UpdateTags(pgTgs)
	e.tagList.Refresh()
}

func NewEditorLayout(interactor EventInteractor, registry *core.Registry, selectedPage *core.Page, data string) *EditorLayout {
	el := &EditorLayout{interactor: interactor, registry: registry, selectedPage: selectedPage, data: data}
	el.ExtendBaseWidget(el)

	return el
}
