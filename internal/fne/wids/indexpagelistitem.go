package wids

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
)

type IndexPageInteractor interface {
	OpenInEditor(id string)
	OpenInBrowserByID(id string)
}

type IndexPageListItem struct {
	widget.BaseWidget
	selectedIndex       core.IndexPage
	indexPageInteractor IndexPageInteractor

	pageNameLbl    *widget.RichText
	openEditorBtn  *widget.Button
	openBrowserBtn *widget.Button
	fieldLbl       *widget.RichText
}

func (i *IndexPageListItem) CreateRenderer() fyne.WidgetRenderer {
	i.pageNameLbl = widget.NewRichTextWithText("")
	i.openEditorBtn = widget.NewButton("Open in editor", func() {
		i.indexPageInteractor.OpenInEditor(i.selectedIndex.ID)
	})
	i.openBrowserBtn = widget.NewButton("Open in browser", func() {
		i.indexPageInteractor.OpenInBrowserByID(i.selectedIndex.ID)
	})
	i.fieldLbl = widget.NewRichText()
	i.fieldLbl.Wrapping = fyne.TextWrapWord

	actionCont := container.NewGridWithColumns(2, i.openEditorBtn, i.openBrowserBtn)
	topCont := container.NewBorder(nil, nil, nil, actionCont, i.pageNameLbl)

	return widget.NewSimpleRenderer(container.NewPadded(container.NewBorder(topCont, nil, nil, nil, i.fieldLbl)))
}

func (i *IndexPageListItem) UpdateIndexPage(ip core.IndexPage) {
	i.selectedIndex = ip
	i.pageNameLbl.ParseMarkdown(fmt.Sprintf("## %s", ip.ID))

	frag := ip.Fragments[0]
	i.fieldLbl.ParseMarkdown(frag)
}

func NewIndexPageListItem(ipi IndexPageInteractor) *IndexPageListItem {
	ipli := &IndexPageListItem{indexPageInteractor: ipi}
	ipli.ExtendBaseWidget(ipli)

	return ipli
}
