package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
)

type PosButton struct {
	widget.Button
	OnSelected func(e *fyne.PointEvent)
}

func (pb *PosButton) Cursor() desktop.Cursor {
	return desktop.PointerCursor
}

func NewPosButton(icon fyne.Resource) *PosButton {
	pb := &PosButton{}
	pb.SetIcon(icon)
	pb.Importance = widget.LowImportance

	pb.ExtendBaseWidget(pb)
	return pb
}

func (pb *PosButton) Tapped(e *fyne.PointEvent) {
	if pb.OnSelected != nil {
		pb.OnSelected(e)
	}
}
