package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"slices"
)

type IconSelection struct {
	widget.BaseWidget

	OnSelected func(name string)
	sets       map[string]map[string]fyne.Resource
	appTabs    *container.AppTabs
	cancelBtn  *widget.Button
}

func (i *IconSelection) CreateRenderer() fyne.WidgetRenderer {
	names := make([]string, len(i.sets))
	idx := 0
	for k := range i.sets {
		names[idx] = k
		idx++
	}
	slices.Sort(names)
	i.appTabs = container.NewAppTabs()
	i.appTabs.SetTabLocation(container.TabLocationLeading)
	for _, name := range names {
		ig := newIconGrid(name, i.sets[name], i.OnSelected)
		i.appTabs.Append(container.NewTabItem(name, ig))
	}

	return widget.NewSimpleRenderer(i.appTabs)
}

func NewIconSelection(sets map[string]map[string]fyne.Resource, onSelected func(name string)) *IconSelection {

	is := &IconSelection{sets: sets, OnSelected: onSelected}
	is.ExtendBaseWidget(is)

	return is
}
