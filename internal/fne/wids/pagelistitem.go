package wids

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/images"
)

type PageListItem struct {
	widget.BaseWidget
	selectedPage   *core.Page
	actionSelected func(p *core.Page, e *fyne.PointEvent)

	nameLbl   *widget.RichText
	actionBtn *PosButton
	emojiBox  *fyne.Container
	emojis    []*canvas.Image
}

func (p *PageListItem) CreateRenderer() fyne.WidgetRenderer {
	name := "template"
	if p.selectedPage != nil {
		name = p.selectedPage.Name
	}
	p.nameLbl = widget.NewRichTextFromMarkdown(fmt.Sprintf("### %s", name))
	p.actionBtn = NewPosButton(theme.MoreVerticalIcon())
	p.actionBtn.OnSelected = func(e *fyne.PointEvent) {
		p.actionSelected(p.selectedPage, e)
	}

	p.emojiBox = container.NewHBox()

	return widget.NewSimpleRenderer(container.NewPadded(container.NewBorder(nil, nil, nil, p.actionBtn, container.NewBorder(nil, nil, nil, p.emojiBox, p.nameLbl))))
}

func (p *PageListItem) UpdatePage(pg *core.Page) {
	p.selectedPage = pg
	if p.nameLbl != nil {
		p.nameLbl.ParseMarkdown(fmt.Sprintf("### %s", p.selectedPage.Name))
	}

	if p.emojiBox != nil {
		for _, e := range p.emojis {
			e.Hide()
		}

		for idx, t := range pg.Tags {
			var img *canvas.Image
			if idx > len(p.emojis)-1 {
				img = canvas.NewImageFromResource(nil)
				p.emojis = append(p.emojis, img)
				p.emojiBox.Add(img)
			} else {
				img = p.emojis[idx]
			}
			if ico := images.FindIcon(t.Emoji); ico != nil {
				img.Show()
				img.Resource = ico
				img.FillMode = canvas.ImageFillContain
				img.SetMinSize(fyne.NewSize(20, 20))
				img.Refresh()
			}
		}
	}
}

//func (p *PageListItem) TappedSecondary(pe *fyne.PointEvent) {
//	p.actionSelected(p.selectedPage, pe)
//}

func NewPageListItem(actionSelected func(p *core.Page, e *fyne.PointEvent)) *PageListItem {
	pli := &PageListItem{actionSelected: actionSelected}
	pli.ExtendBaseWidget(pli)

	return pli
}
