package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
	"log"
)

type ChertEntry struct {
	widget.Entry

	actionSave func()
}

func NewChertEntry(actionSave func()) *ChertEntry {
	ce := &ChertEntry{actionSave: actionSave}

	ce.ExtendBaseWidget(ce)
	return ce
}

func (ce *ChertEntry) TypedShortcut(s fyne.Shortcut) {
	if _, ok := s.(*desktop.CustomShortcut); !ok {
		ce.Entry.TypedShortcut(s)
		return
	}

	log.Println("Shortcut typed:", s.ShortcutName())
	short := s.(*desktop.CustomShortcut)
	if short.Key() == fyne.KeyS && (short.Mod() == fyne.KeyModifierShortcutDefault) {
		ce.actionSave()
	}
}
