package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"slices"
)

type iconGrid struct {
	widget.BaseWidget

	OnSelected func(name string)
	setName    string
	icons      map[string]fyne.Resource
}

func (i *iconGrid) CreateRenderer() fyne.WidgetRenderer {
	keys := make([]string, len(i.icons))
	idx := 0
	for k := range i.icons {
		keys[idx] = k
		idx++
	}
	slices.Sort(keys)

	grid := container.NewGridWithColumns(6)
	for _, k := range keys {
		func(key string) {
			ig := newClickableIcon(i.icons[key], key, i.OnSelected)
			grid.Add(ig)
		}(k)
	}

	scroll := container.NewVScroll(grid)
	spacer := NewSizer(250)

	return widget.NewSimpleRenderer(container.NewMax(scroll, spacer))
}

func newIconGrid(setName string, icons map[string]fyne.Resource, onSelected func(name string)) *iconGrid {
	ig := &iconGrid{setName: setName, icons: icons, OnSelected: onSelected}
	ig.ExtendBaseWidget(ig)

	return ig
}

type clickableIcon struct {
	widget.BaseWidget
	//widget.Icon

	image *canvas.Image
	res   fyne.Resource

	OnSelected func(name string)
	name       string
}

func (c *clickableIcon) CreateRenderer() fyne.WidgetRenderer {
	c.image = canvas.NewImageFromResource(c.res)
	//c.image.Resize(fyne.NewSize(128, 128))
	c.image.FillMode = canvas.ImageFillContain
	c.image.SetMinSize(fyne.NewSize(64, 64))

	return widget.NewSimpleRenderer(container.NewPadded(container.NewMax(c.image)))
}

func (c *clickableIcon) Tapped(event *fyne.PointEvent) {
	c.OnSelected(c.name)
}

func newClickableIcon(res fyne.Resource, name string, onSelected func(name string)) *clickableIcon {
	ci := &clickableIcon{name: name, OnSelected: onSelected, res: res}
	ci.ExtendBaseWidget(ci)

	return ci
}
