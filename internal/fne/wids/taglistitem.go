package wids

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/images"
)

type TagListItem struct {
	widget.BaseWidget
	selectedTag    *core.Tag
	actionSelected func(t *core.Tag, e *fyne.PointEvent)

	nameLbl   *widget.RichText
	emoji     *canvas.Image
	actionBtn *PosButton
}

func (t *TagListItem) CreateRenderer() fyne.WidgetRenderer {
	name := "template"
	if t.selectedTag != nil {
		name = t.selectedTag.Name
	}
	t.nameLbl = widget.NewRichTextFromMarkdown(fmt.Sprintf("### %s", name))
	t.actionBtn = NewPosButton(theme.MoreVerticalIcon())
	t.actionBtn.OnSelected = func(e *fyne.PointEvent) {
		t.actionSelected(t.selectedTag, e)
	}

	t.emoji = canvas.NewImageFromResource(nil)

	return widget.NewSimpleRenderer(container.NewPadded(container.NewBorder(nil, nil, t.emoji, t.actionBtn, t.nameLbl)))
}

func (t *TagListItem) UpdateTag(tg *core.Tag) {
	t.selectedTag = tg
	if t.nameLbl != nil {
		t.nameLbl.ParseMarkdown(fmt.Sprintf("### %s", t.selectedTag.Name))
	}

	if t.emoji != nil {
		if t.selectedTag.Emoji != "" {
			if ico := images.FindIcon(t.selectedTag.Emoji); ico != nil {
				t.emoji.Show()
				t.emoji.Resource = ico
				t.emoji.FillMode = canvas.ImageFillContain
				t.emoji.SetMinSize(fyne.NewSize(20, 20))
				t.Refresh()
			}
		} else {
			t.emoji.Hide()
		}
	}
}

//func (t *TagListItem) TappedSecondary(pe *fyne.PointEvent) {
//	t.actionSelected(t.selectedTag, pe)
//}

func NewTagListItem(actionSelected func(t *core.Tag, e *fyne.PointEvent)) *TagListItem {
	tli := &TagListItem{actionSelected: actionSelected}
	tli.ExtendBaseWidget(tli)

	return tli
}
