package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type Sizer struct {
	widget.BaseWidget

	minHeight float32
	minWidth  float32
}

func (s *Sizer) CreateRenderer() fyne.WidgetRenderer {
	return &sizerRenderer{s: s}
}

func NewSizer(minHeight float32) *Sizer {
	s := &Sizer{minHeight: minHeight}
	s.ExtendBaseWidget(s)

	return s
}

func (s *Sizer) SetWidth(width float32) {
	s.minWidth = width
}

type sizerRenderer struct {
	s *Sizer
}

func (s sizerRenderer) Destroy() {

}

func (s sizerRenderer) Layout(size fyne.Size) {

}

func (s sizerRenderer) MinSize() fyne.Size {
	return fyne.NewSize(s.s.minWidth, s.s.minHeight)
}

func (s sizerRenderer) Objects() []fyne.CanvasObject {
	return []fyne.CanvasObject{}
}

func (s sizerRenderer) Refresh() {

}
