package wids

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

type RecentListItem struct {
	widget.BaseWidget

	current      string
	lbl          *widget.Label
	action       *widget.Button
	removeAction func(current string)
}

func (r *RecentListItem) CreateRenderer() fyne.WidgetRenderer {
	r.lbl = widget.NewLabel("")
	r.action = widget.NewButtonWithIcon("", theme.DeleteIcon(), func() {
		r.removeAction(r.current)
	})

	return widget.NewSimpleRenderer(container.NewBorder(nil, nil, nil, r.action, r.lbl))
}

func (r *RecentListItem) UpdateCurrent(current string) {
	r.current = current
	r.lbl.SetText(current)
}

func NewRecentListItem(action func(current string)) *RecentListItem {
	rli := &RecentListItem{removeAction: action}
	rli.ExtendBaseWidget(rli)

	return rli
}
