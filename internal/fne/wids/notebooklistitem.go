package wids

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/images"
)

type NotebookListItem struct {
	widget.BaseWidget
	selectedNotebook *core.Notebook
	actionSelected   func(nb *core.Notebook, e *fyne.PointEvent)

	nameLbl   *widget.RichText
	emoji     *canvas.Image
	actionBtn *PosButton
}

func (n *NotebookListItem) CreateRenderer() fyne.WidgetRenderer {
	name := "template"
	if n.selectedNotebook != nil {
		name = n.selectedNotebook.Name
	}
	n.nameLbl = widget.NewRichTextFromMarkdown(fmt.Sprintf("### %s", name))
	n.actionBtn = NewPosButton(theme.MoreVerticalIcon())
	n.actionBtn.OnSelected = func(e *fyne.PointEvent) {
		n.actionSelected(n.selectedNotebook, e)
	}

	n.emoji = canvas.NewImageFromResource(nil)

	return widget.NewSimpleRenderer(container.NewPadded(container.NewBorder(nil, nil, n.emoji, n.actionBtn, n.nameLbl)))
}

func (n *NotebookListItem) UpdateNotebook(nb *core.Notebook) {
	n.selectedNotebook = nb
	if n.nameLbl != nil {
		n.nameLbl.ParseMarkdown(fmt.Sprintf("### %s", n.selectedNotebook.Name))
	}

	if n.selectedNotebook.Emoji != "" && n.emoji != nil {
		if ico := images.FindIcon(n.selectedNotebook.Emoji); ico != nil {
			n.emoji.Show()
			//n.emoji.SetResource(ico)

			n.emoji.Resource = ico
			n.emoji.FillMode = canvas.ImageFillContain
			n.emoji.SetMinSize(fyne.NewSize(20, 20))
			n.emoji.Refresh()
		}
	} else {
		if n.emoji != nil && n.emoji.Resource != nil {
			n.emoji.Hide()
		}
	}
}

//func (n *NotebookListItem) TappedSecondary(pe *fyne.PointEvent) {
//	n.actionSelected(n.selectedNotebook, pe)
//}

func NewNotebookListItem(actionSelected func(nb *core.Notebook, e *fyne.PointEvent)) *NotebookListItem {
	nli := &NotebookListItem{actionSelected: actionSelected}
	nli.ExtendBaseWidget(nli)

	return nli
}
