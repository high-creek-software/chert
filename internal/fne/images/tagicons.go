package images

import (
	"fyne.io/fyne/v2"
	"gitlab.com/high-creek-software/chert/internal/core"
	"io"
	"io/fs"
	"log"
	"path/filepath"
)

// TODO: Display link -> https://icons8.com/icons/collections/vlYmrZVMBkqx
var allSets map[string]map[string]fyne.Resource

func LoadTagIcons() {
	allSets = make(map[string]map[string]fyne.Resource)

	if iconsSub, err := fs.Sub(core.IconDir, "icons"); err == nil {
		fs.WalkDir(iconsSub, ".", func(path string, d fs.DirEntry, err error) error {
			if path == "." {
				return nil
			}

			if d.IsDir() {
				allSets[path] = make(map[string]fyne.Resource)
				return nil
			}

			base := d.Name()
			parent := filepath.Dir(path)

			if in, err := iconsSub.Open(path); err == nil {
				if bs, err := io.ReadAll(in); err == nil {
					allSets[parent][base] = fyne.NewStaticResource(base, bs)
				} else {
					log.Println("	error reading", path)
				}
			} else {
				log.Println("	error opening", path)
			}
			return nil
		})
	}
}

func AllSets() map[string]map[string]fyne.Resource {
	return allSets
}

func FindIcon(name string) fyne.Resource {
	for _, set := range allSets {
		if ico, ok := set[name]; ok {
			return ico
		}
	}

	return nil
}
