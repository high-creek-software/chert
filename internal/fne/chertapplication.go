package fne

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	wikilink "github.com/abhinav/goldmark-wikilink"
	fmthtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/gammazero/deque"
	"github.com/high-creek-software/bento"
	"github.com/high-creek-software/tabman"
	"github.com/yuin/goldmark"
	highlighting "github.com/yuin/goldmark-highlighting"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/adapters"
	"gitlab.com/high-creek-software/chert/internal/fne/images"
	"gitlab.com/high-creek-software/chert/internal/fne/layouts"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"golang.org/x/image/colornames"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	pref_last = "last"
	recents   = "recents"
)

var OpenIcon = fyne.NewStaticResource("OpenIcon", core.OpenBytes)
var RecentIcon = fyne.NewStaticResource("RecentIcon", core.RecentBytes)
var SearchIcon = fyne.NewStaticResource("SearchIcon", core.SearchBytes)
var SettingsIcon = fyne.NewStaticResource("SettingsIcon", core.SettingsBytes)

type ChertApplication struct {
	app             fyne.App
	window          fyne.Window
	notebookTree    *widget.Tree
	notebookAdapter *adapters.NotebookAdapter
	pageList        *widget.List
	pageAdapter     *adapters.PageAdapter
	pageTitle       *widget.RichText
	tagsList        *widget.List
	tagsAdapter     *adapters.TagAdapter

	editorTabs       *container.DocTabs
	editorTabManager *tabman.Manager[string]
	editorLayoutMap  map[string]*layouts.EditorLayout

	registry *core.Registry
	box      *bento.Box

	markdown goldmark.Markdown
	port     int

	selectedNotebook *core.Notebook
	pendingCut       *core.PendingCut

	recents *deque.Deque[string]
}

func NewChertApplication() *ChertApplication {
	images.LoadTagIcons()

	fApp := app.NewWithID("io.highcreeksoftware.chert")
	chertIcon := fyne.NewStaticResource("icon", core.IconBytes)
	fApp.SetIcon(chertIcon)
	win := fApp.NewWindow("Chert")
	win.Resize(fyne.NewSize(1800, 1000))
	fApp.Settings().SetTheme(ChertTheme{})
	cApp := &ChertApplication{app: fApp, window: win, port: 9988, recents: deque.New[string]()}
	cApp.app.Lifecycle().SetOnStarted(cApp.onStart)
	cApp.init()

	if desk, ok := fApp.(desktop.App); ok {
		m := fyne.NewMenu("Chert", fyne.NewMenuItem("Show", func() {
			win.Show()
		}), fyne.NewMenuItem("Open in browser", func() {
			if url, err := url.Parse(fmt.Sprintf("http://localhost:%d", cApp.port)); err == nil {
				fApp.OpenURL(url)
			}
		}))
		desk.SetSystemTrayMenu(m)
		desk.SetSystemTrayIcon(fyne.NewStaticResource("systray", core.SystrayBytes))
	}
	win.SetCloseIntercept(func() {
		win.Hide()
	})
	return cApp
}

func (ca *ChertApplication) onStart() {

}

func (ca *ChertApplication) updateRecents(r string) {
	ca.removePathFromRecents(r)
	ca.recents.PushFront(r)
	ca.serializeRecents()
}

func (ca *ChertApplication) removePathFromRecents(path string) {
	idx := ca.recents.Index(func(input string) bool {
		return input == path
	})
	if idx != -1 {
		ca.recents.Remove(idx)
	}
}

func (ca *ChertApplication) serializeRecents() {
	parts := make([]string, ca.recents.Len())
	for idx := 0; idx < ca.recents.Len(); idx++ {
		parts[idx] = ca.recents.At(idx)
	}
	ca.app.Preferences().SetString(recents, strings.Join(parts, "|"))
}

func (ca *ChertApplication) init() {
	ca.markdown = goldmark.New(
		goldmark.WithRendererOptions(
			html.WithUnsafe(),
		),
		goldmark.WithExtensions(
			extension.GFM,
			extension.NewFootnote(),
			&wikilink.Extender{Resolver: core.WikiResolver{}},
			highlighting.NewHighlighting(
				highlighting.WithStyle("monokai"),
				highlighting.WithGuessLanguage(true),
				highlighting.WithFormatOptions(
					fmthtml.WithLineNumbers(true),
				),
			),
		),
	)

	ca.notebookAdapter = adapters.NewNotebookAdapter(nil, ca.notebookAction)
	ca.notebookTree = widget.NewTree(ca.notebookAdapter.ChildUIDs, ca.notebookAdapter.IsBranch, ca.notebookAdapter.Create, ca.notebookAdapter.Update)
	ca.notebookTree.OnSelected = ca.NotebookSelected

	ca.pageAdapter = adapters.NewPageAdapter(nil, ca.pageAction)
	ca.pageList = widget.NewList(ca.pageAdapter.Count, ca.pageAdapter.CreateTemplate, ca.pageAdapter.Bind)
	ca.pageList.OnSelected = ca.PageSelected
	ca.pageTitle = widget.NewRichTextFromMarkdown("")

	newTagButton := widget.NewButtonWithIcon("New Tag", theme.FolderNewIcon(), ca.addTagAction)
	ca.tagsAdapter = adapters.NewTagAdapter(nil, ca.tagAction)
	ca.tagsList = widget.NewList(ca.tagsAdapter.Count, ca.tagsAdapter.CreateTemplate, ca.tagsAdapter.Bind)
	ca.tagsList.OnSelected = ca.tagSelected

	ca.editorTabs = container.NewDocTabs()
	ca.editorTabManager = tabman.NewManager[string]()
	ca.editorLayoutMap = make(map[string]*layouts.EditorLayout)
	ca.editorTabs.OnClosed = func(ti *container.TabItem) {
		ca.editorTabManager.RemoveTab(ti)
		// TODO: Remove the editorlayout here too!
	}

	insideSplit := container.NewHSplit(container.NewBorder(container.NewBorder(nil, widget.NewSeparator(), nil, nil, ca.pageTitle), nil, nil, nil, ca.pageList), ca.editorTabs)
	insideSplit.Offset = 0.2
	notebooksTagsSplit := container.NewVSplit(ca.notebookTree, container.NewBorder(container.NewPadded(newTagButton), nil, nil, nil, ca.tagsList))
	notebooksTagsSplit.Offset = 0.75
	outsideSplit := container.NewHSplit(notebooksTagsSplit, insideSplit)
	outsideSplit.Offset = 0.2

	openAction := widget.NewToolbarAction(OpenIcon, ca.handleOpenAction)
	recentAction := widget.NewToolbarAction(RecentIcon, ca.handleRecents)
	searchAction := widget.NewToolbarAction(SearchIcon, ca.showSearch)
	aboutAction := widget.NewToolbarAction(SettingsIcon, ca.showSettings)
	toolbar := widget.NewToolbar(openAction, recentAction, widget.NewToolbarSeparator(), searchAction, widget.NewToolbarSpacer(), aboutAction)

	ca.box = bento.NewBox()

	ca.window.SetContent(container.NewMax(container.NewBorder(toolbar, nil, nil, nil, outsideSplit), ca.box))
}

func (ca *ChertApplication) handleRecents() {
	var pup *widget.PopUp
	var list *widget.List
	var recentsAdapter *adapters.RecentsAdapter
	recentsAdapter = adapters.NewRecentsAdapter(ca.recents, func(current string) {
		ca.removePathFromRecents(current)
		ca.serializeRecents()
		recentsAdapter.Update(ca.recents)
		list.Refresh()
	})
	list = widget.NewList(recentsAdapter.Count, recentsAdapter.CreateTemplate, recentsAdapter.Bind)
	list.OnSelected = func(id widget.ListItemID) {
		path := recentsAdapter.Get(id)
		ca.OpenDirectory(path)
		pup.Hide()
	}
	sizer := wids.NewSizer(200)
	sizer.SetWidth(400)

	pup = widget.NewPopUp(container.NewMax(list, sizer), ca.window.Canvas())
	pup.ShowAtPosition(fyne.NewPos(100, 25))
}

func (ca *ChertApplication) showSearch() {
	termEntry := widget.NewEntry()
	termForm := widget.NewFormItem("Query term", termEntry)
	dlg := dialog.NewForm("Search...", "OK", "Cancel", []*widget.FormItem{termForm}, func(ok bool) {
		if !ok {
			return
		}

		ca.doSearch(termEntry.Text)
	}, ca.window)
	dlg.Resize(fyne.NewSize(600, 300))
	dlg.Show()
}

func (ca *ChertApplication) showSettings() {
	settingsWin := ca.app.NewWindow("Settings")
	settingsWin.Resize(fyne.NewSize(1000, 750))

	aboutTab := container.NewTabItem("About", layouts.NewAboutLayout())
	appTabs := container.NewAppTabs(aboutTab)

	settingsWin.SetContent(appTabs)
	settingsWin.Show()
}

func (ca *ChertApplication) doSearch(term string) {
	res := ca.registry.Search(term)
	sl := layouts.NewSearchLayout(ca, term, res)
	tab := container.NewTabItem(fmt.Sprintf("Query: %s", term), sl)
	ca.editorTabs.Append(tab)
	ca.editorTabs.Select(tab)
}

func (ca *ChertApplication) OpenInEditor(id string) {
	ca.OpenInternalLink(id)
}

func (ca *ChertApplication) OpenInBrowserByID(id string) {
	pg := ca.registry.GetPageByPath(id)
	if pg != nil {
		ca.OpenInBrowser(pg)
	}
}

func (ca *ChertApplication) addTagAction() {
	tagNameEntry := widget.NewEntry()
	tagNameForm := widget.NewFormItem("Tag Name", tagNameEntry)
	dlg := dialog.NewForm("New Tag", "Add", "Cancel", []*widget.FormItem{tagNameForm}, func(ok bool) {
		if !ok {
			return
		}

		if tagNameEntry.Text == "" {
			ca.ShowMessage("Tag name is required")
			return
		}

		_, err := ca.registry.NewTag(tagNameEntry.Text)
		if err != nil {
			ca.ShowError(err)
			return
		}
		ca.updateTags()
	}, ca.window)
	dlg.Resize(fyne.NewSize(600, 300))
	dlg.Show()
}

func (ca *ChertApplication) tagSelected(id widget.ListItemID) {
	ca.notebookTree.UnselectAll()
	tag := ca.tagsAdapter.GetTag(id)
	ca.UpdateTagPages(tag)
	ca.selectedNotebook = nil
}

func (ca *ChertApplication) UpdateTagPages(tag *core.Tag) {
	ca.pageTitle.ParseMarkdown(fmt.Sprintf("## Tag: %s", tag.Name))
	pages, err := ca.registry.PagesForTag(tag.Name)
	if err != nil {
		ca.ShowError(err)
		return
	}

	for _, pg := range pages {
		if ts, err := ca.registry.FindPageTags(pg); err == nil {
			pg.Tags = ts
		} else {
			log.Println("error loading tags for page", pg.Name)
		}
	}

	ca.pageAdapter.UpdatePages(pages)
	ca.pageList.Refresh()
}

func (ca *ChertApplication) NotebookSelected(uid widget.TreeNodeID) {
	ca.tagsList.UnselectAll()
	notebook := ca.notebookAdapter.GetNotebook(uid)
	ca.loadPages(notebook)
}

func (ca *ChertApplication) loadPages(nb *core.Notebook) {
	ca.selectedNotebook = nb
	ca.pageTitle.ParseMarkdown(fmt.Sprintf("## Notebook: %s", nb.Name))
	for _, pg := range nb.Pages {
		if ts, err := ca.registry.FindPageTags(pg); err == nil {
			pg.Tags = ts
		} else {
			log.Println("error loading tags for page", pg.Name)
		}
	}
	ca.pageAdapter.UpdatePages(nb.Pages)
	ca.pageList.Refresh()
	ca.pageList.UnselectAll()
}

func (ca *ChertApplication) PageSelected(id widget.ListItemID) {
	page := ca.pageAdapter.GetPage(id)
	ca.displayPage(page)
}

func (ca *ChertApplication) handleOpenAction() {
	dlg := dialog.NewFolderOpen(func(l fyne.ListableURI, err error) {
		if err != nil {
			ca.ShowError(err)
			return
		}
		if l == nil {
			return
		}

		ca.OpenDirectory(l.Path())
	}, ca.window)
	dlg.Resize(fyne.NewSize(1200, 500))
	dlg.Show()
}

func (ca *ChertApplication) ShowError(err error) {
	itm := bento.NewItemWithMessage(err.Error(), bento.LengthLong)
	itm.SetBackgroundColor(colornames.Red)
	ca.box.AddItem(itm)
}

func (ca *ChertApplication) ShowMessage(message string) {
	itm := bento.NewItemWithMessage(message, bento.LengthLong)
	itm.SetBackgroundColor(colornames.Purple)
	ca.box.AddItem(itm)
}

func (ca *ChertApplication) OpenDirectory(dir string) {
	ca.registry = core.NewRegistry(dir, core.WikilinkRegex, ca.markdown)
	ca.notebookAdapter.UpdateRoot(ca.registry.Root)
	ca.notebookTree.Refresh()
	ca.window.SetTitle(dir)
	ca.updateTags()
	// TODO: Ensure that there is data in the tree first
	ca.notebookTree.OpenBranch("/")
	ca.loadPages(ca.registry.Root)

	ca.app.Preferences().SetString(pref_last, dir)
	ca.updateRecents(dir)
}

func (ca *ChertApplication) updateTags() {
	ca.tagsList.UnselectAll()
	ca.tagsAdapter.UpdateTags(ca.registry.GetTags())
	ca.tagsList.Refresh()
}

func (ca *ChertApplication) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if ca.registry != nil {
		ca.registry.ServeHTTP(w, r)
	}
}

func (ca *ChertApplication) OpenInBrowser(p *core.Page) {
	path := fmt.Sprintf("http://localhost:%d/%s", ca.port, p.RelativePath)
	if u, err := url.Parse(path); err == nil {
		ca.app.OpenURL(u)
	} else {
		ca.ShowError(err)
	}
}

func (ca *ChertApplication) OpenInternalLink(link string) {
	link = strings.TrimPrefix(link, "[[")
	link = strings.TrimSuffix(link, "]]")

	page := ca.registry.GetPageByPath(link)
	ca.displayPage(page)
}

func (ca *ChertApplication) ChooseImage(result func(path string)) {
	dlg := dialog.NewFileOpen(func(u fyne.URIReadCloser, err error) {
		if err != nil {
			ca.ShowError(err)
			return
		}
		if u == nil {
			return
		}
		result(u.URI().Path())
	}, ca.window)
	dlg.Resize(fyne.NewSize(1200, 700))
	dlg.Show()
}

func (ca *ChertApplication) displayPage(page *core.Page) {
	if page == nil {
		return
	}

	if tab, ok := ca.editorTabManager.GetTabItem(page.RelativePath); ok {
		ca.editorTabs.Select(tab)
		return
	}

	data, err := ca.registry.ReadPage(page)
	if err != nil {
		dialog.ShowError(err, ca.window)
		return
	}

	entry := layouts.NewEditorLayout(ca, ca.registry, page, data)
	tab := container.NewTabItem(page.Name, entry)
	ca.editorTabs.Append(tab)
	ca.editorTabs.Select(tab)
	ca.editorTabManager.AddTabItem(page.RelativePath, tab)
	ca.pageList.UnselectAll()
}

func (ca *ChertApplication) Start() {

	if rs := ca.app.Preferences().StringWithFallback(recents, ""); rs != "" {
		parts := strings.Split(rs, "|")
		for _, part := range parts {
			ca.recents.PushBack(part)
		}
	}

	if last := ca.app.Preferences().String(pref_last); last != "" {
		ca.OpenDirectory(last)
	}
	go func() {
		for {
			err := http.ListenAndServe(fmt.Sprintf(":%d", ca.port), ca)
			if strings.Contains(err.Error(), "bind: address already in use") {
				ca.port++
				time.Sleep(500 * time.Millisecond)
			} else {
				ca.ShowError(err)
				break
			}
		}
	}()
	ca.window.ShowAndRun()
}
