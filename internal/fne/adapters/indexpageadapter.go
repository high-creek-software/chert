package adapters

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
)

type IndexPageAdapter struct {
	ips                 []core.IndexPage
	indexPageInteractor wids.IndexPageInteractor

	list *widget.List
}

func NewIndexPageAdapter(ips []core.IndexPage, ipi wids.IndexPageInteractor) *IndexPageAdapter {
	return &IndexPageAdapter{ips: ips, indexPageInteractor: ipi}
}

func (ipa *IndexPageAdapter) SetList(list *widget.List) {
	ipa.list = list
}

func (ipa *IndexPageAdapter) Count() int {
	return len(ipa.ips)
}

func (ipa *IndexPageAdapter) CreateTemplate() fyne.CanvasObject {
	return wids.NewIndexPageListItem(ipa.indexPageInteractor)
}

func (ipa *IndexPageAdapter) Bind(id widget.ListItemID, co fyne.CanvasObject) {
	ip := ipa.GetIndexPage(id)
	ipli := co.(*wids.IndexPageListItem)
	ipli.UpdateIndexPage(ip)

	if ipa.list != nil {
		ipa.list.SetItemHeight(id, ipli.MinSize().Height)
	}
}

func (ipa *IndexPageAdapter) GetIndexPage(id widget.ListItemID) core.IndexPage {
	return ipa.ips[id]
}
