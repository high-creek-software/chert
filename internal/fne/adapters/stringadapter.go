package adapters

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type StringAdapter struct {
	data []string
}

func (sa *StringAdapter) UpdateData(data []string) {
	sa.data = data
}

func (sa *StringAdapter) Count() int {
	return len(sa.data)
}

func (sa *StringAdapter) CreateTemplate() fyne.CanvasObject {
	return widget.NewLabel("tempalte")
}

func (sa *StringAdapter) Bind(id widget.ListItemID, co fyne.CanvasObject) {
	itm := sa.GetItem(id)
	co.(*widget.Label).SetText(itm)
}

func (sa *StringAdapter) GetItem(id widget.ListItemID) string {
	return sa.data[id]
}

func NewStringAdapter(data []string) *StringAdapter {
	return &StringAdapter{data: data}
}
