package adapters

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"github.com/gammazero/deque"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
)

type RecentsAdapter struct {
	paths        *deque.Deque[string]
	removeAction func(current string)
}

func NewRecentsAdapter(paths *deque.Deque[string], removeAction func(current string)) *RecentsAdapter {
	return &RecentsAdapter{paths: paths, removeAction: removeAction}
}

func (r *RecentsAdapter) Update(paths *deque.Deque[string]) {
	r.paths = paths
}

func (r *RecentsAdapter) Count() int {
	return r.paths.Len()
}

func (r *RecentsAdapter) CreateTemplate() fyne.CanvasObject {
	return wids.NewRecentListItem(r.removeAction)
}

func (r *RecentsAdapter) Bind(id widget.ListItemID, co fyne.CanvasObject) {
	path := r.Get(id)
	rli := co.(*wids.RecentListItem)

	rli.UpdateCurrent(path)
}

func (r *RecentsAdapter) Get(id widget.ListItemID) string {
	return r.paths.At(id)
}
