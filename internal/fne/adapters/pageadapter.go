package adapters

import (
	"cmp"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"slices"
	"strings"
)

type PageAdapter struct {
	pages []*core.Page

	actionSelected func(p *core.Page, e *fyne.PointEvent)
}

func NewPageAdapter(pages []*core.Page, actionSelected func(p *core.Page, e *fyne.PointEvent)) *PageAdapter {
	return &PageAdapter{pages: pages, actionSelected: actionSelected}
}

func (pa *PageAdapter) UpdatePages(pages []*core.Page) {
	slices.SortFunc(pages, func(a, b *core.Page) int {
		return cmp.Compare(strings.ToLower(a.Name), strings.ToLower(b.Name))
	})
	pa.pages = pages
}

func (pa *PageAdapter) RemovePage(p *core.Page) {
	if idx := slices.Index(pa.pages, p); idx != -1 {
		pa.pages = slices.Delete(pa.pages, idx, idx+1)
	}
}

func (pa *PageAdapter) Count() int {
	return len(pa.pages)
}

func (pa *PageAdapter) CreateTemplate() fyne.CanvasObject {
	return wids.NewPageListItem(pa.actionSelected)
}

func (pa *PageAdapter) Bind(id widget.ListItemID, co fyne.CanvasObject) {
	page := pa.pages[id]
	co.(*wids.PageListItem).UpdatePage(page)
}

func (pa *PageAdapter) GetPage(id widget.ListItemID) *core.Page {
	return pa.pages[id]
}
