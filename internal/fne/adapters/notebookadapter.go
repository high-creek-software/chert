package adapters

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"slices"
	"strings"
)

type NotebookAdapter struct {
	root *core.Notebook

	actionSelected func(nb *core.Notebook, e *fyne.PointEvent)
}

func NewNotebookAdapter(root *core.Notebook, actionSelected func(nb *core.Notebook, e *fyne.PointEvent)) *NotebookAdapter {
	return &NotebookAdapter{root: root, actionSelected: actionSelected}
}

func (ta *NotebookAdapter) UpdateRoot(root *core.Notebook) {
	ta.root = root
}

func (ta *NotebookAdapter) ChildUIDs(uid widget.TreeNodeID) []widget.TreeNodeID {
	var kids []widget.TreeNodeID
	if ta.root == nil {
		return kids
	}
	if uid == "" {
		kids = append(kids, ta.root.RelativePath)
		return kids
	}
	notebook := ta.GetNotebook(uid)
	if notebook != nil && notebook.Children != nil {
		for _, kid := range notebook.Children {
			kids = append(kids, kid.RelativePath)
		}
	}
	slices.Sort(kids)
	return kids
}

func (ta *NotebookAdapter) IsBranch(node widget.TreeNodeID) bool {
	notebook := ta.GetNotebook(node)
	if notebook == nil {
		return false
	}
	if node == "" { // Putting this check here, this seems to allow me to show the top level directory as the root of the tree.
		return true
	}
	return len(notebook.Children) > 0
}

func (ta *NotebookAdapter) Create(isBranch bool) fyne.CanvasObject {

	return wids.NewNotebookListItem(ta.actionSelected)
}

func (ta *NotebookAdapter) Update(node widget.TreeNodeID, branch bool, co fyne.CanvasObject) {
	nb := ta.GetNotebook(node)
	//co.(*widget.Label).SetText(nb.Name)
	li := co.(*wids.NotebookListItem)
	li.UpdateNotebook(nb)
}

func (ta *NotebookAdapter) GetNotebook(uid widget.TreeNodeID) *core.Notebook {
	notebook := ta.root
	if uid != "" && uid != "/" {
		parts := strings.Split(uid, "/")
		for _, part := range parts {
			notebook = notebook.Children[part]
		}
	}
	return notebook
}
