package adapters

import (
	"cmp"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"slices"
	"strings"
)

type TagAdapter struct {
	tags           []*core.Tag
	actionSelected func(t *core.Tag, e *fyne.PointEvent)
}

func NewTagAdapter(tags []*core.Tag, actionSelected func(t *core.Tag, e *fyne.PointEvent)) *TagAdapter {
	return &TagAdapter{tags: tags, actionSelected: actionSelected}
}

func (ta *TagAdapter) UpdateTags(tags []*core.Tag) {
	ta.tags = tags
	slices.SortFunc(ta.tags, func(f, s *core.Tag) int {
		return cmp.Compare(strings.ToLower(f.Name), strings.ToLower(s.Name))
	})
}

func (ta *TagAdapter) Count() int {
	return len(ta.tags)
}

func (ta *TagAdapter) CreateTemplate() fyne.CanvasObject {
	return wids.NewTagListItem(ta.actionSelected)
}

func (ta *TagAdapter) Bind(id widget.ListItemID, co fyne.CanvasObject) {
	tag := ta.GetTag(id)
	co.(*wids.TagListItem).UpdateTag(tag)
}

func (ta *TagAdapter) GetTag(id widget.ListItemID) *core.Tag {
	return ta.tags[id]
}
