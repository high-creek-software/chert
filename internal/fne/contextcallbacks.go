package fne

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/chert/internal/core"
	"gitlab.com/high-creek-software/chert/internal/fne/images"
	"gitlab.com/high-creek-software/chert/internal/fne/layouts"
	"gitlab.com/high-creek-software/chert/internal/fne/wids"
	"net/url"
)

func (ca *ChertApplication) notebookAction(nb *core.Notebook, e *fyne.PointEvent) {

	var items []*fyne.MenuItem

	if ca.pendingCut != nil {
		pasteItem := fyne.NewMenuItem("Paste", func() {
			if err := ca.registry.CutPastePage(ca.pendingCut.Page, ca.pendingCut.ParentNotebook, nb); err != nil {
				ca.ShowError(err)
			} else {
				ca.loadPages(nb)
			}
		})
		pasteItem.Icon = theme.ContentPasteIcon()
		items = append(items, pasteItem)
		items = append(items, fyne.NewMenuItemSeparator())
	}

	addPage := fyne.NewMenuItem("Add Page", func() {
		pageEntry := widget.NewEntry()
		pageName := widget.NewFormItem("Page name", pageEntry)
		dlg := dialog.NewForm(fmt.Sprintf("Add page to %s", nb.Name), "OK", "Cancel", []*widget.FormItem{pageName}, func(ok bool) {
			if !ok {
				return
			}
			if pageEntry.Text == "" {
				ca.ShowMessage("Page name required")
				return
			}

			ca.registry.AddPage(nb, pageEntry.Text)
			ca.loadPages(nb)
		}, ca.window)
		dlg.Resize(fyne.NewSize(600, 300))
		dlg.Show()

	})
	addPage.Icon = theme.ContentAddIcon()
	items = append(items, addPage)
	items = append(items, fyne.NewMenuItemSeparator())

	addIcon := fyne.NewMenuItem("Add icon", func() {
		var pup *widget.PopUp
		iconSelect := wids.NewIconSelection(images.AllSets(), func(name string) {
			pup.Hide()
			ca.registry.SetEmoji(nb.RelativePath, name)
			nb.Emoji = name
			ca.notebookTree.Refresh()
		})
		pup = widget.NewPopUp(iconSelect, ca.window.Canvas())
		pup.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
	})
	addIcon.Icon = theme.InfoIcon()
	items = append(items, addIcon)

	open := fyne.NewMenuItem("Open in browser", func() {
		if url, err := url.Parse(fmt.Sprintf("http://localhost:%d/%s", ca.port, nb.RelativePath)); err == nil {
			ca.app.OpenURL(url)
		} else {
			ca.ShowError(err)
		}
	})
	open.Icon = theme.ViewFullScreenIcon()
	items = append(items, open)

	addChild := fyne.NewMenuItem("Add Child Notebook", func() {
		nbEntry := widget.NewEntry()
		nbForm := widget.NewFormItem("Child notebook", nbEntry)
		dlg := dialog.NewForm("Add Child Notebook", "OK", "Cancel", []*widget.FormItem{nbForm}, func(ok bool) {
			if !ok {
				return
			}
			if nbEntry.Text == "" {
				ca.ShowMessage("Notebook name required")
				return
			}

			ca.registry.AddChild(nb, nbEntry.Text)
			ca.notebookAdapter.UpdateRoot(ca.registry.Root)
			ca.notebookTree.Refresh()
		}, ca.window)
		dlg.Resize(fyne.NewSize(600, 300))
		dlg.Show()
	})
	addChild.Icon = theme.FolderNewIcon()
	items = append(items, addChild)

	rename := fyne.NewMenuItem("Rename", func() {
		nameEntry := widget.NewEntry()
		nameItem := widget.NewFormItem("Rename notebook", nameEntry)
		dlg := dialog.NewForm(fmt.Sprintf("Rename %s", nb.Name), "OK", "Cancel", []*widget.FormItem{nameItem}, func(ok bool) {
			if !ok {
				return
			}
			if nameEntry.Text == "" {
				ca.ShowMessage("Name requierd")
				return
			}

			err := ca.registry.RenameNotebook(nb, nameEntry.Text)
			if err != nil {
				ca.ShowError(err)
				return
			}
			nb.Name = nameEntry.Text
			ca.notebookTree.Refresh()
		}, ca.window)
		dlg.Resize(fyne.NewSize(600, 300))
		dlg.Show()
	})
	rename.Icon = theme.SearchReplaceIcon()
	items = append(items, rename)

	menu := fyne.NewMenu(nb.Name, items...)
	pm := widget.NewPopUpMenu(menu, ca.window.Canvas())
	pm.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
}

func (ca *ChertApplication) pageAction(p *core.Page, e *fyne.PointEvent) {
	var items []*fyne.MenuItem

	if ca.selectedNotebook != nil {
		cut := fyne.NewMenuItem("Cut", func() {
			ca.pendingCut = &core.PendingCut{Page: p, ParentNotebook: ca.selectedNotebook}
		})
		cut.Icon = theme.ContentCutIcon()
		items = append(items, cut)
		items = append(items, fyne.NewMenuItemSeparator())
	}
	copyLink := fyne.NewMenuItem("Copy internal link", func() {
		link := fmt.Sprintf("[[%s]]", p.RelativePath)
		ca.window.Clipboard().SetContent(link)
		ca.ShowMessage(fmt.Sprintf("%s copied to clipboard", link))
	})
	copyLink.Icon = theme.ContentCopyIcon()
	items = append(items, copyLink)

	rename := fyne.NewMenuItem("Rename", func() {
		nameEntry := widget.NewEntry()
		nameItem := widget.NewFormItem("Rename notebook", nameEntry)
		dlg := dialog.NewForm(fmt.Sprintf("Rename %s", p.Name), "OK", "Cancel", []*widget.FormItem{nameItem}, func(ok bool) {
			if !ok {
				return
			}
			if nameEntry.Text == "" {
				ca.ShowMessage("Name requierd")
				return
			}

			err := ca.registry.RenamePage(p, nameEntry.Text)
			if err != nil {
				ca.ShowError(err)
				return
			}
			p.Name = nameEntry.Text
			ca.pageList.Refresh()
		}, ca.window)
		dlg.Resize(fyne.NewSize(600, 300))
		dlg.Show()
	})
	rename.Icon = theme.SearchReplaceIcon()
	items = append(items, rename)
	items = append(items, fyne.NewMenuItemSeparator())

	deleteItm := fyne.NewMenuItem("Delete", func() {
		err := ca.registry.DeletePage(p)
		if err != nil {
			ca.ShowError(err)
			return
		}
		// TODO: Update the page list
		if ca.selectedNotebook != nil {
			ca.loadPages(ca.selectedNotebook)
		} else {
			ca.pageAdapter.RemovePage(p)
			ca.pageList.Refresh()
		}
	})
	deleteItm.Icon = theme.DeleteIcon()
	items = append(items, deleteItm)

	menu := fyne.NewMenu(p.Name, items...)
	pm := widget.NewPopUpMenu(menu, ca.window.Canvas())
	pm.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
}

func (ca *ChertApplication) tagAction(t *core.Tag, e *fyne.PointEvent) {
	addIcon := fyne.NewMenuItem("Add icon", func() {
		var pup *widget.PopUp
		iconSelect := wids.NewIconSelection(images.AllSets(), func(name string) {
			pup.Hide()
			ca.registry.SetTagEmoji(t.Name, name)
			t.Emoji = name
			ca.tagsList.Refresh()
		})
		pup = widget.NewPopUp(iconSelect, ca.window.Canvas())
		pup.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
	})
	addIcon.Icon = theme.FileImageIcon()

	del := fyne.NewMenuItem("Delete", func() {

	})
	del.Icon = theme.DeleteIcon()
	menu := fyne.NewMenu(t.Name, addIcon, del)
	pm := widget.NewPopUpMenu(menu, ca.window.Canvas())
	pm.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
}

func (ca *ChertApplication) PageTagAction(p *core.Page, t *core.Tag, editor *layouts.EditorLayout, e *fyne.PointEvent) {
	remove := fyne.NewMenuItem("Remove tag", func() {
		if err := ca.registry.RemoveTagFromPage(p, t.Name); err == nil {
			editor.UpdateTags()
		} else {
			ca.ShowError(err)
		}
	})
	remove.Icon = theme.DeleteIcon()

	menu := fyne.NewMenu(p.Name, remove)
	pm := widget.NewPopUpMenu(menu, ca.window.Canvas())
	pm.ShowAtPosition(e.AbsolutePosition.AddXY(15, 15))
}
