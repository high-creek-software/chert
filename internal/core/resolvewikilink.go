package core

import wikilink "github.com/abhinav/goldmark-wikilink"

var sep = []byte("/")
var hash = []byte("#")

type WikiResolver struct{}

func (wr WikiResolver) ResolveWikilink(node *wikilink.Node) (destination []byte, err error) {

	res := make([]byte, len(sep)+len(node.Target)+len(hash)+len(node.Fragment))

	var i int
	if len(node.Target) > 0 {
		i += copy(res, sep)
		i += copy(res[i:], node.Target)
	}
	if len(node.Fragment) > 0 {
		i += copy(res[i:], hash)
		i += copy(res[i:], node.Fragment)
	}

	return res[:i], nil
}
