package core

type PendingCut struct {
	Page           *Page
	ParentNotebook *Notebook
}
