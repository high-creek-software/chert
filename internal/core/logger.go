package core

import (
	"log"
	"os"
)

type Logger struct {
	showDebug bool
	info      *log.Logger
	debug     *log.Logger
	err       *log.Logger
}

func NewLogger(showDebug bool) *Logger {
	return &Logger{
		showDebug: showDebug,
		info:      log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
		debug:     log.New(os.Stdout, "DEBUG\t", log.Ldate|log.Ltime|log.Lshortfile),
		err:       log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile),
	}
}

func (l *Logger) Error(v ...any) {
	l.err.Println(v)
}

func (l *Logger) Info(v ...any) {
	l.info.Println(v)
}

func (l *Logger) Fatal(v ...any) {
	l.err.Fatalln(v)
}

func (l *Logger) Debug(v ...any) {
	if !l.showDebug {
		return
	}

	l.debug.Println(v)
}
