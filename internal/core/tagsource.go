package core

type TagSource int

const (
	TagTree TagSource = iota
	TagPage
)
