package core

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/blevesearch/bleve/v2"
	"github.com/yuin/goldmark"
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"
	"time"
)

const (
	serializationFile = "chert.data"
)

//go:embed page.gohtml
var pageTemplate string

//go:embed notebook.gohtml
var notebookTemplate string

var ErrorTagExists = errors.New("tag exists")
var ErrorTagNotFound = errors.New("tag not found")

func NewRegistry(selection string, wikilinkRegex *regexp.Regexp, markdown goldmark.Markdown) *Registry {

	chertData := ChertData{EmojiMap: make(map[string]string), Tags: make(map[string]*Tag)}
	if chertDataInput, err := os.Open(filepath.Join(selection, serializationFile)); err == nil {
		defer chertDataInput.Close()
		if err := json.NewDecoder(chertDataInput).Decode(&chertData); err != nil {
			log.Println("error decoding chert data", err)
		}
	}

	rootEmoji := chertData.EmojiMap[string(os.PathSeparator)]
	registry := &Registry{RootPath: selection,
		Root:          NewNotebook(selection, string(os.PathSeparator), filepath.Base(selection), rootEmoji),
		ChertData:     chertData,
		WikilinkRegex: wikilinkRegex,
		markdown:      markdown,
	}

	filepath.WalkDir(selection, func(path string, d fs.DirEntry, err error) error {
		if path == selection {
			return nil
		}
		relativePath := strings.Replace(path, selection+string(os.PathSeparator), "", 1)

		emoji := registry.ChertData.EmojiMap[relativePath]

		fi, fiErr := d.Info()

		if fiErr != nil {
			log.Println("encountered error:", fiErr)
			return fiErr
		}

		if d.IsDir() {
			dirs := strings.Split(relativePath, string(os.PathSeparator))
			if len(dirs) == 1 {
				registry.Root.Children[dirs[0]] = NewNotebook(path, relativePath, fi.Name(), emoji)
			} else {
				parent := registry.Root.Children[dirs[0]]
				for _, dir := range dirs[1:] {
					if nb, ok := parent.Children[dir]; !ok {
						parent.Children[dir] = NewNotebook(path, relativePath, fi.Name(), emoji)
					} else {
						parent = nb
					}
				}

			}
		} else {

			ext := filepath.Ext(path)
			if ext != ".md" && ext != ".txt" {
				return nil
			}

			if strings.Contains(relativePath, string(os.PathSeparator)) {
				paths := strings.Replace(relativePath, string(os.PathSeparator)+fi.Name(), "", 1)
				parts := strings.Split(paths, string(os.PathSeparator))
				if len(parts) == 1 {
					registry.Root.Children[parts[0]].Pages = append(registry.Root.Children[parts[0]].Pages, NewPage(path, relativePath, fi.Name(), emoji, fi.ModTime()))
				} else {
					parent := registry.Root.Children[parts[0]]
					for _, p := range parts[1:] {
						parent = parent.Children[p]
					}
					parent.Pages = append(parent.Pages, NewPage(path, relativePath, fi.Name(), emoji, fi.ModTime()))
				}
			} else {
				registry.Root.Pages = append(registry.Root.Pages, NewPage(path, relativePath, fi.Name(), emoji, fi.ModTime()))
			}
		}

		return nil
	})

	mapping := bleve.NewIndexMapping()
	pageDocMapping := bleve.NewDocumentMapping()
	nameMapping := bleve.NewTextFieldMapping()
	nameMapping.Analyzer = "en"
	pageDocMapping.AddFieldMappingsAt("name", nameMapping)
	contentMapping := bleve.NewTextFieldMapping()
	contentMapping.Analyzer = "en"
	pageDocMapping.AddFieldMappingsAt("content", contentMapping)
	mapping.AddDocumentMapping("page", pageDocMapping)

	index, err := bleve.NewMemOnly(mapping)
	if err != nil {
		log.Fatalln(err)
	}

	registry.ChertIndex = index
	go registry.IndexNotebook(registry.Root)

	return registry
}

type ChertData struct {
	EmojiMap map[string]string `json:"emojiMap"`
	Tags     map[string]*Tag   `json:"tags"`
}

type Tag struct {
	Name    string    `json:"name"`
	Emoji   string    `json:"emoji"`
	Created time.Time `json:"created"`
	Pages   []string  `json:"pages"` // This will be the page relative paths
}

type Registry struct {
	RootPath      string
	Root          *Notebook
	ChertData     ChertData
	ChertIndex    bleve.Index
	WikilinkRegex *regexp.Regexp
	markdown      goldmark.Markdown
}

type IndexPage struct {
	ID        string
	Name      string
	Content   string
	Fragments []string
}

func (ip IndexPage) Type() string {
	return "page"
}

func (r *Registry) IndexNotebook(notebook *Notebook) {
	for _, page := range notebook.Pages {
		r.addPageToIndex(page)
	}

	for _, child := range notebook.Children {
		r.IndexNotebook(child)
	}
}

func (r *Registry) addPageToIndex(page *Page) {
	if content, err := r.ReadPage(page); err == nil {

		wikilinks := r.WikilinkRegex.FindAllString(content, -1)
		page.LinkedPages = wikilinks
		ip := IndexPage{
			ID:      page.RelativePath,
			Name:    page.Name,
			Content: content,
		}

		r.ChertIndex.Index(ip.ID, ip)
	}
}

func (r *Registry) Search(term string) []IndexPage {

	query := bleve.NewMatchQuery(term)
	search := bleve.NewSearchRequest(query)
	search.Highlight = bleve.NewHighlight()
	results, err := r.ChertIndex.Search(search)
	if err != nil {
		log.Println("error performing search", err)
		return nil
	}

	var res []IndexPage
	for _, hit := range results.Hits {
		ip := IndexPage{ID: hit.ID}

		for _, arr := range hit.Fragments {
			for _, item := range arr {
				ip.Fragments = append(ip.Fragments, item)
			}
		}

		res = append(res, ip)
	}

	return res
}

func (r *Registry) FindBacklinks(wikilink string) []string {
	return r.findBacklinksInNotebook(r.Root, wikilink)
}

func (r *Registry) findBacklinksInNotebook(notebook *Notebook, wikilink string) []string {
	var res []string
	for _, page := range notebook.Pages {
		if page.PageContainsLink(wikilink) {
			res = append(res, page.RelativePath)
		}
	}

	for _, nb := range notebook.Children {
		childLinks := r.findBacklinksInNotebook(nb, wikilink)
		if len(childLinks) > 0 {
			res = append(res, childLinks...)
		}
	}

	return res
}

func (r *Registry) String() string {
	bs, err := json.MarshalIndent(r, "", "  ")
	if err != nil {
		return err.Error()
	}
	return string(bs)
}

func (r *Registry) AddPage(notebook *Notebook, pageName string) (*Page, error) {

	ext := filepath.Ext(pageName)
	if ext == "" {
		pageName = fmt.Sprintf("%s.md", pageName)
	}

	fullPath := filepath.Join(notebook.Path, pageName)
	relativePath := filepath.Join(notebook.RelativePath, pageName)
	relativePath = strings.TrimPrefix(relativePath, string(os.PathSeparator))
	f, err := os.Create(fullPath)

	if err != nil {
		return nil, err
	}
	defer f.Close()

	pg := &Page{
		Path:         fullPath,
		RelativePath: relativePath,
		Name:         pageName,
		ModTime:      time.Now(),
	}

	notebook.Pages = append(notebook.Pages, pg)

	return pg, nil
}

func (r *Registry) AddChild(notebook *Notebook, childName string) (*Notebook, error) {
	fullPath := filepath.Join(notebook.Path, childName)
	relativePath := filepath.Join(notebook.RelativePath, childName)

	// The root notebook starts with an os.Pathseperator, but loading the notebook doesn't add this.
	// So we're just removing it from here so that the new child notebook has the same relative path as those loaded
	relativePath = strings.TrimPrefix(relativePath, string(os.PathSeparator))

	err := os.Mkdir(fullPath, os.ModePerm)
	if err != nil {
		return nil, err
	}

	nb := &Notebook{
		Path:         fullPath,
		RelativePath: relativePath,
		Name:         childName,
		Children:     make(map[string]*Notebook),
	}

	notebook.Children[childName] = nb

	return nb, nil
}

func (r *Registry) RenameNotebook(notebook *Notebook, rename string) error {
	parent := r.findParent(notebook)
	base := filepath.Base(notebook.Path)
	newPath := strings.Replace(notebook.Path, base, rename, -1)
	err := os.Rename(notebook.Path, newPath)

	if err != nil {
		return err
	}

	newRelative := strings.Replace(notebook.RelativePath, base, rename, -1)

	// Now I need to traverse all the children notebooks and pages to update the path
	r.updatePaths(notebook, notebook.Path, newPath, notebook.RelativePath, newRelative)
	if parent != nil {
		parent.Rekey(notebook, base, rename)
	}
	notebook.Name = rename
	r.serializeChertData()

	return nil
}

func (r *Registry) DeleteNotebook(notebook *Notebook) error {

	var err error
	for _, child := range notebook.Children {
		if err != nil {
			return err
		}
		err = r.DeleteNotebook(child)
	}

	for _, page := range notebook.Pages {
		if err != nil {
			return err
		}
		err = r.DeletePage(page)
	}

	// Remove any emojis for this notebook
	delete(r.ChertData.EmojiMap, notebook.RelativePath)

	// Delete this directory from the underlying filesystem
	err = os.Remove(notebook.Path)

	return err
}

func (r *Registry) RenamePage(page *Page, rename string) error {

	ext := filepath.Ext(rename)
	if ext == "" {
		rename = fmt.Sprintf("%s.md", rename)
	}

	newPath := strings.Replace(page.Path, page.Name, rename, 1)
	newRelativePath := strings.Replace(page.RelativePath, page.Name, rename, 1)
	err := os.Rename(page.Path, newPath)

	if err != nil {
		return err
	}

	r.updateTags(page.RelativePath, newRelativePath)
	page.Path = newPath
	page.RelativePath = newRelativePath
	return nil
}

func (r *Registry) DeletePage(page *Page) error {
	err := os.Remove(page.Path)
	if err != nil {
		return err
	}

	// Remove from notebook
	notebook := r.findNotebookForPage(page.RelativePath)
	idx := slices.Index(notebook.Pages, page)
	notebook.Pages = slices.Delete(notebook.Pages, idx, idx+1)

	// Remove from any tags
	for _, tag := range r.ChertData.Tags {
		if slices.Contains(tag.Pages, page.RelativePath) {
			r.RemoveTagFromPage(page, tag.Name)
		}
	}

	return nil
}

func (r *Registry) CutPastePage(page *Page, parentNotebook *Notebook, newNotebook *Notebook) error {

	newPath := filepath.Join(newNotebook.Path, page.Name)
	oldRelativePath := page.RelativePath
	newRelativePath := filepath.Join(newNotebook.RelativePath, page.Name)
	err := os.Rename(page.Path, newPath)

	if err != nil {
		return err
	}

	r.updateTags(oldRelativePath, newRelativePath)

	page.Path = newPath
	page.RelativePath = newRelativePath

	idx := slices.Index(parentNotebook.Pages, page)
	parentNotebook.Pages = slices.Delete(parentNotebook.Pages, idx, idx+1)

	newNotebook.Pages = append(newNotebook.Pages, page)

	return nil
}

func (r *Registry) updateTags(oldRelativePath, newRelativePath string) {
	for _, tag := range r.ChertData.Tags {
		if slices.Contains(tag.Pages, oldRelativePath) {
			idx := slices.Index(tag.Pages, oldRelativePath)
			tag.Pages[idx] = newRelativePath
		}
	}

	r.serializeChertData()
}

func (r *Registry) SetEmoji(relativePath, emoji string) {
	r.ChertData.EmojiMap[relativePath] = emoji
	r.serializeChertData()
}

func (r *Registry) NewTag(name string) (*Tag, error) {
	if _, ok := r.ChertData.Tags[name]; ok {
		return nil, ErrorTagExists
	}

	tag := &Tag{
		Name:    name,
		Created: time.Now(),
	}

	r.ChertData.Tags[name] = tag
	r.serializeChertData()

	return tag, nil
}

func (r *Registry) SetTagEmoji(tagName, emoji string) {
	r.ChertData.Tags[tagName].Emoji = emoji
	r.serializeChertData()
}

func (r *Registry) GetTags() []*Tag {
	if r.ChertData.Tags == nil {
		return nil
	}

	tags := make([]*Tag, len(r.ChertData.Tags))
	idx := 0
	for _, v := range r.ChertData.Tags {
		tags[idx] = v
		idx++
	}
	return tags
}

func (r *Registry) AddTagToPage(page *Page, tagName string) error {

	if tag, ok := r.ChertData.Tags[tagName]; ok {

		tag.Pages = append(tag.Pages, page.RelativePath)

		r.serializeChertData()

		return nil
	}

	return ErrorTagNotFound
}

func (r *Registry) RemoveTagFromPage(page *Page, tagName string) error {
	if tag, ok := r.ChertData.Tags[tagName]; ok {
		idx := slices.Index(tag.Pages, page.RelativePath)
		tag.Pages = slices.Delete(tag.Pages, idx, idx+1)
		r.serializeChertData()
	}

	return nil
}

func (r *Registry) FindPageTags(page *Page) ([]*Tag, error) {
	var tags []*Tag
	for _, tag := range r.ChertData.Tags {
		if slices.Contains(tag.Pages, page.RelativePath) {
			tags = append(tags, tag)
		}
	}

	return tags, nil
}

func (r *Registry) PagesForTag(tagName string) ([]*Page, error) {
	var pages []*Page

	var tag *Tag
	var ok bool

	tag, ok = r.ChertData.Tags[tagName]

	if !ok {
		return nil, ErrorTagNotFound
	}

	for _, pagePath := range tag.Pages {
		if page := r.GetPageByPath(pagePath); page != nil {
			pages = append(pages, page)
		}
	}

	return pages, nil
}

func (r *Registry) ReadPage(page *Page) (string, error) {
	f, err := ioutil.ReadFile(page.Path)
	if err != nil {
		return "", err
	}

	return string(f), nil
}

func (r *Registry) SavePage(page *Page, txt string) error {
	o, err := os.OpenFile(page.Path, os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}
	defer o.Close()

	_, err = o.Write([]byte(txt))
	// TODO: Index the text that we already have above.  Don't hit the disk again.
	go r.addPageToIndex(page)
	return err
}

func (r *Registry) updatePaths(nb *Notebook, oldPath, newPath, oldRelative, newRelative string) {
	nb.Path = strings.Replace(nb.Path, oldPath, newPath, 1)
	nbNewRelative := strings.Replace(nb.RelativePath, oldRelative, newRelative, 1)

	if emoji, ok := r.ChertData.EmojiMap[nb.RelativePath]; ok {
		r.ChertData.EmojiMap[nbNewRelative] = emoji
		delete(r.ChertData.EmojiMap, nb.RelativePath)
	}

	nb.RelativePath = newRelative
	for _, page := range nb.Pages {
		previousPageRelative := page.RelativePath
		page.Path = strings.Replace(page.Path, oldPath, newPath, 1)
		page.RelativePath = strings.Replace(page.RelativePath, oldRelative, newRelative, 1)
		r.updateTags(previousPageRelative, page.RelativePath)
	}

	for _, child := range nb.Children {
		r.updatePaths(child, oldPath, newPath, oldRelative, newRelative)
	}
}

func (r *Registry) findParent(nb *Notebook) *Notebook {
	// This handles any children of the root node.
	if !strings.Contains(nb.RelativePath, string(os.PathSeparator)) {
		return r.Root
	}

	// I need to find the parent, this is for any interior or leaf nodes
	exclusiveRelativePath := strings.Replace(nb.RelativePath, string(os.PathSeparator)+nb.Name, "", -1)

	keys := strings.Split(exclusiveRelativePath, string(os.PathSeparator))

	parent := r.Root.Children[keys[0]]
	for _, dir := range keys[1:] {
		parent = parent.Children[dir]
	}

	return parent
}

func (r *Registry) GetPageByPath(relativePath string) *Page {
	base := filepath.Base(relativePath)
	innerNodes := strings.Replace(relativePath, base, "", 1)
	innerNodes = strings.Trim(innerNodes, string(os.PathSeparator))

	if innerNodes == "" {
		return r.findPageInPages(base, r.Root.Pages)
	}
	notebooks := strings.Split(innerNodes, string(os.PathSeparator))

	// Walk the notebook/directory hierarchy
	parent := r.Root.Children[notebooks[0]]
	for _, dir := range notebooks[1:] {
		if parent == nil {
			return nil
		}
		parent = parent.Children[dir]
	}
	if parent == nil {
		return nil
	}

	return r.findPageInPages(base, parent.Pages)
}

func (r *Registry) FindNotebook(relativePath string) *Notebook {
	relativePath = strings.Trim(relativePath, "/")
	if relativePath == "" {
		return r.Root
	}

	nodes := strings.Split(relativePath, "/")
	parent := r.Root.Children[nodes[0]]
	for _, node := range nodes[1:] {
		if parent == nil {
			return nil
		}
		parent = parent.Children[node]
	}

	return parent
}

func (r *Registry) findNotebookForPage(relativePath string) *Notebook {
	base := filepath.Base(relativePath)
	innerNodes := strings.Replace(relativePath, base, "", 1)
	innerNodes = strings.Trim(innerNodes, string(os.PathSeparator))

	if innerNodes == "" {
		return r.Root
	}

	notebooks := strings.Split(innerNodes, string(os.PathSeparator))
	parent := r.Root.Children[notebooks[0]]
	for _, dir := range notebooks[1:] {
		parent = parent.Children[dir]
	}

	return parent
}

func (r *Registry) findPageInPages(name string, pages []*Page) *Page {
	for _, page := range pages {
		if page.Name == name {
			return page
		}
	}
	return nil
}

func (r *Registry) serializeChertData() {
	o, err := os.Create(filepath.Join(r.RootPath, serializationFile))
	if err != nil {
		log.Println("could not open serialization file", err)
		return
	}

	enc := json.NewEncoder(o)
	enc.SetIndent("", "  ")
	err = enc.Encode(r.ChertData)
	if err != nil {
		log.Println("error encoding chert data", err)
	}
}

func (reg *Registry) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// TODO: Handle favicon
	// TODO: Handle tags
	// TODO: Handle images

	path := r.URL.Path
	ext := filepath.Ext(path)
	if ext == ".md" || ext == ".txt" {
		reg.handlePage(w, r)
	} else if ext != "" {
		reg.handleFile(w, r)
	} else {
		reg.handleNotebook(w, r)
	}
}

func (reg *Registry) handlePage(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	pg := reg.GetPageByPath(path)
	if pg == nil {
		http.Error(w, "could not find page", http.StatusNotFound)
		return
	}

	in, err := ioutil.ReadFile(pg.Path)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	tpl, err := template.New("page").Funcs(map[string]any{"md": reg.MarkdownRender}).Parse(pageTemplate)

	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	pgData := struct {
		Title   string
		Content string
	}{
		Title:   pg.Name,
		Content: string(in),
	}
	tpl.Execute(w, pgData)
}

func (reg *Registry) handleNotebook(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	nb := reg.FindNotebook(path)
	if nb == nil {
		http.Error(w, "could not find notebook", http.StatusNotFound)
		return
	}

	tpl, err := template.New("notebook").Funcs(map[string]any{"md": reg.MarkdownRender}).Parse(notebookTemplate)

	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	nbData := struct {
		Title    string
		Notebook *Notebook
	}{
		Title:    path,
		Notebook: nb,
	}
	tpl.Execute(w, nbData)
}

func (reg *Registry) handleFile(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path

	in, err := os.Open(filepath.Join(reg.RootPath, path))

	if err != nil {
		http.Error(w, "could not find file", http.StatusNotFound)
		return
	}
	defer in.Close()

	io.Copy(w, in)
}

func (reg *Registry) MarkdownRender(in string) template.HTML {
	var buf bytes.Buffer
	err := reg.markdown.Convert([]byte(in), &buf)
	if err != nil {
		return template.HTML("Error converting markdown:" + err.Error())
	}
	return template.HTML(string(buf.Bytes()))
}
