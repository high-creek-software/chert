package core

import (
	"mvdan.cc/xurls/v2"
	"regexp"
)

const (
	patternListItem = `^(?P<prefix>\s*[-*+])\s?(?P<content>.+)?`
	//patternEmptyListItem  = `^\s*-$`
	patternStartsWithTask = `^\s*[-*+]\s\[[\sx]\]\s?(?P<content>.+)?`
	//patternEmtpyTask      = `^\s*[-*+]\s\[[\sx]\]$`

	patternImageLink    = `(?m)!\[.*\]\((?P<link>.*) ?(?P<caption>".*")?\)`
	patternMDLink       = `(?m)[^!]\[.*\]\((?P<link>.*)\)`
	patternCombinedLink = `(?mU)!?\[(?P<alt>.*)?\]\((?P<link>.*) ?(?P<msg>".*")?\)`
	patternLink         = `(?m)<https?://.*>`
)

var CombinedLinks = regexp.MustCompile(patternCombinedLink)
var Link = regexp.MustCompile(patternLink)
var WikilinkRegex = regexp.MustCompile(`(?m)\[\[.*\]\]`)
var H1Regex = regexp.MustCompile(`(?m)^# .*`)
var H2Regex = regexp.MustCompile(`(?m)^## .*`)
var ListItemRegex = regexp.MustCompile(patternListItem)
var TaskItemRegex = regexp.MustCompile(patternStartsWithTask)
var StrictUrls = xurls.Strict()
