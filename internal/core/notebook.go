package core

import (
	"slices"
	"time"
)

type Notebook struct {
	Path         string
	RelativePath string
	Name         string
	Pages        []*Page
	Children     map[string]*Notebook
	Emoji        string
}

func NewNotebook(path, relativePath, name, emoji string) *Notebook {
	return &Notebook{Path: path, RelativePath: relativePath, Name: name, Children: make(map[string]*Notebook), Emoji: emoji}
}

func (nb *Notebook) Rekey(child *Notebook, oldKey, newKey string) {
	nb.Children[newKey] = child
	delete(nb.Children, oldKey)
}

type Page struct {
	Path         string
	RelativePath string
	Name         string
	ModTime      time.Time
	Emoji        string
	LinkedPages  []string
	Tags         []*Tag
}

func NewPage(path, relativePath, name, emoji string, modTime time.Time) *Page {
	return &Page{Path: path, RelativePath: relativePath, Name: name, ModTime: modTime, Emoji: emoji}
}

func (p *Page) PageContainsLink(wikilink string) bool {
	return slices.Contains(p.LinkedPages, wikilink)
}
