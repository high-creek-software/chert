package core

import (
	"bufio"
	"github.com/BurntSushi/toml"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"slices"
	"strings"
)

const (
	configDir  = "chert"
	configFile = "chert.toml"
)

type Config struct {
	Scheme                string   `toml:"scheme"`
	Font                  string   `toml:"font"`
	Recent                []string `toml:"recent"`
	HidePageManagement    bool     `toml:"hidePageManagement"`
	Port                  int      `toml:"port"`
	TabWidth              int      `toml:"tabWidth"`
	HighlightCurrentLine  bool     `toml:"highlightCurrentLine"`
	ShowLineNumbers       bool     `toml:"showLineNumbers"`
	AutoIndent            bool     `toml:"autoIndent"`
	HighlightStyle        string   `toml:"highlightStyle"`
	DictionaryBasePath    string   `toml:"dictionaryBase"`
	AvailableDictionaries []string `toml:"-"`
	LinkColor             string   `toml:"linkColor"`
	ResizeTitleTags       bool     `toml:"resizeTitleTags"`
	EmojiSize             int      `toml:"emojiSize"`
}

func (c *Config) PrependRecent(recent string) {

	if slices.Contains(c.Recent, recent) {
		idx := slices.Index(c.Recent, recent)
		c.Recent = slices.Delete(c.Recent, idx, idx+1)
	}

	c.Recent = slices.Insert(c.Recent, 0, recent)

	if len(c.Recent) > 10 {
		c.Recent = c.Recent[:10]
	}
}

type ConfigManager interface {
	LoadConfig() (*Config, error)
	SaveConfig(conf *Config) error
}

type ConfigManagerImpl struct {
	configDir string
}

func (c ConfigManagerImpl) LoadConfig() (*Config, error) {
	config := &Config{Port: 3200,
		TabWidth:             4,
		HighlightCurrentLine: true,
		ShowLineNumbers:      true,
		AutoIndent:           true,
		HighlightStyle:       "monokai",
		DictionaryBasePath:   "/usr/share/hunspell/en_US",
		LinkColor:            "Light Sky Blue",
		EmojiSize:            12,
	}
	_, err := toml.DecodeFile(filepath.Join(c.configDir, configFile), config)

	config.AvailableDictionaries = c.ParseAvailableDictionaries()

	return config, err
}

func (c ConfigManagerImpl) SaveConfig(conf *Config) error {
	o, err := os.Create(filepath.Join(c.configDir, configFile))
	if err != nil {
		return err
	}
	defer o.Close()
	enc := toml.NewEncoder(o)
	return enc.Encode(conf)
}

func (c ConfigManagerImpl) ParseAvailableDictionaries() []string {
	var res []string

	cmd := exec.Command("hunspell", "-D")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(err)
		return res
	}

	hunspellOutput := string(out)
	idx := strings.Index(hunspellOutput, "AVAILABLE DICTIONARIES")
	if idx < 0 {
		return res
	}

	dictSegment := hunspellOutput[idx:]
	sc := bufio.NewScanner(strings.NewReader(dictSegment))
	for sc.Scan() {
		res = append(res, sc.Text())
	}
	return res[1:]
}

func NewConfigManager() ConfigManager {
	configPath, err := os.UserConfigDir()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("config path", configPath)

	chertConfigDir := filepath.Join(configPath, configDir)
	err = os.MkdirAll(chertConfigDir, 0755)
	if err != nil {
		log.Fatalln("error creating config directory", err)
	}

	return &ConfigManagerImpl{configDir: chertConfigDir}
}
