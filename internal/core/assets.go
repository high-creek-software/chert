package core

import (
	"embed"
)

//go:embed chertv1-256.png
var IconBytes []byte

//go:embed systray.png
var SystrayBytes []byte

//go:embed icons
var IconDir embed.FS

//go:embed open.png
var OpenBytes []byte

//go:embed recent.png
var RecentBytes []byte

//go:embed search.png
var SearchBytes []byte

//go:embed settings.png
var SettingsBytes []byte
